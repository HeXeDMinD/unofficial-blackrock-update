package data.scripts.util;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.combat.*;
import org.lazywizard.lazylib.*;
import org.lazywizard.lazylib.combat.entities.SimpleEntity;
import org.lwjgl.util.vector.Vector2f;

import java.awt.*;
import java.util.List;

public class brdy_Utils {
    //////////////////////////////////////////
    //               Misc Utils             //
    //////////////////////////////////////////
    public static void drawText(ShipAPI ship, String string, float freq, float dur) {
        Global.getCombatEngine().addFloatingText(ship.getLocation(), string,15f, Color.white, ship,
                freq, dur); // Yes i am too lazy to type that all out everytime
    }


    @SuppressWarnings("unused")
    public static String getPersonality(ShipAPI ship) {
        String name = null;
        PersonAPI captain = ship.getCaptain();

        if (captain != null) {
            switch (captain.getPersonalityAPI().getId()) {
                case "cautious":
                    name = "cautious";
                    break;
                case "timid":
                    name = "timid";
                    break;
                case "steady":
                    name = "steady";
                    break;
                case "aggressive":
                    name = "aggressive";
                    break;
                case "reckless":
                    name = "reckless";
                    break;
            }
        }
        return name;
    }

    //////////////////////////////////////////
    //              Module Check            //
    //////////////////////////////////////////
    public static ShipAPI getRoot(ShipAPI ship) {
        if (isMultiShip(ship)) {
            ShipAPI root = ship;
            while (root.getParentStation() != null) {
                root = root.getParentStation();
            }
            return root;
        } else {
            return ship;
        }
    }

    public static boolean isMultiShip(ShipAPI ship) {
        return ship.getParentStation() != null || ship.isShipWithModules();
    }

    public static boolean isRoot(ShipAPI ship) {
        return getRoot(ship) != ship;
    }

    //////////////////////////////////////////
    //           Anamorphic Flare           //
    //////////////////////////////////////////
    public static void createFlare(ShipAPI origin, Vector2f point, CombatEngineAPI engine, float brightness, float thickness, float angle, float spread, float fringeGain, Color fringeColor, Color coreColor) {
        int magnitude = (int) (1f / thickness);
        int alpha = Math.min((int) (255f * brightness * thickness), 255);
        int alphaf = Math.min((int) (255f * brightness * thickness * fringeGain), 255);

        for (int i = 0; i < magnitude; i++) {
            float angleP = angle + (float) Math.random() * 2f * spread - spread;
            if (angleP >= 360f) {
                angleP -= 360f;
            } else if (angleP < 0f) {
                angleP += 360f;
            }

            point.x += FastTrig.cos(angleP * Math.PI / 180f);
            point.y += FastTrig.sin(angleP * Math.PI / 180f);

            Color fringeColorP = new Color(fringeColor.getRed(), fringeColor.getGreen(), fringeColor.getBlue(), alphaf);
            Color coreColorP = new Color(coreColor.getRed(), coreColor.getGreen(), coreColor.getBlue(), alpha);

            engine.spawnEmpArc(origin, point, null, new SimpleEntity(point), DamageType.ENERGY,
                    0.0f, 0.0f, 100000f, null, 25f,
                    fringeColorP, coreColorP);
        }
    }

    //////////////////////////////////////////
    //                 VENT AI              //
    //////////////////////////////////////////
    public static float getArmorLevel(ShipAPI ship) {
        if (ship == null || !Global.getCombatEngine().isEntityInPlay(ship)) {
            return 0f;
        }
        float current = 0f;
        float total = 0f;
        float worst = 1f;
        ArmorGridAPI armorGrid = ship.getArmorGrid();
        for (int x = 0; x < armorGrid.getGrid().length; x++) {
            for (int y = 0; y < armorGrid.getGrid()[x].length; y++) {
                float fraction = armorGrid.getArmorFraction(x, y);
                current += fraction;
                total += 1f;
                if (fraction < worst) {
                    worst = fraction;
                }
            }
        }
        return (current / total) * (float) Math.sqrt(worst * 0.75f + 0.25f);
    }

    public static float getTimeToAim(WeaponAPI weapon, Vector2f aimAt) {
        float turnSpeed;
        float time;
        if (Math.abs(weapon.distanceFromArc(aimAt)) >= 10f) {
            turnSpeed = weapon.getShip().getMutableStats().getMaxTurnRate().getModifiedValue();
        } else {
            turnSpeed = Math.max(weapon.getTurnRate(),
                    weapon.getShip().getMutableStats().getMaxTurnRate().getModifiedValue());
        }
        time = Math.abs(MathUtils.getShortestRotation(weapon.getCurrAngle(), VectorUtils.getAngle(weapon.getLocation(), aimAt))) / turnSpeed;

        // Divide by zero - can't turn, only a threat if already aimed
        if (Float.isNaN(time) || turnSpeed <= 0f) {
            if (weapon.distanceFromArc(aimAt) == 0) {
                return 0f;
            } else {
                return Float.MAX_VALUE;
            }
        }

        return time;
    }

    public static boolean shipBurst(ShipAPI ship) {
        List<WeaponAPI> weapons = ship.getAllWeapons();

        for (WeaponAPI weapon : weapons) {
            if (weapon.getType() == WeaponAPI.WeaponType.DECORATIVE || weapon.getType() == WeaponAPI.WeaponType.LAUNCH_BAY
                    || weapon.getType() == WeaponAPI.WeaponType.SYSTEM) {
                continue;
            }
            if (((!weapon.isBeam() && weapon.getDerivedStats().getBurstFireDuration() > 0f
                    && weapon.getDerivedStats().getBurstFireDuration() <= 15f)
                    || weapon.isBurstBeam()) && (weapon.getChargeLevel() >= 0.75f || weapon.isFiring())
                    && weapon.getCooldownRemaining() <= 0.25f) {
                if (weapon.getSize() == WeaponAPI.WeaponSize.SMALL && (ship.getHullSize() == ShipAPI.HullSize.FRIGATE
                        || ship.getHullSize() == ShipAPI.HullSize.DESTROYER)) {
                    return true;
                }
                if (weapon.getSize() == WeaponAPI.WeaponSize.MEDIUM && (ship.getHullSize() == ShipAPI.HullSize.FRIGATE
                        || ship.getHullSize() == ShipAPI.HullSize.DESTROYER
                        || ship.getHullSize() == ShipAPI.HullSize.CRUISER)) {
                    return true;
                }
                if (weapon.getSize() == WeaponAPI.WeaponSize.LARGE) {
                    return true;
                }
            }
        }
        return false;
    }

    public static final class FilterMisses implements CollectionUtils.CollectionFilter<DamagingProjectileAPI> {

        private final ShipAPI ship;

        public FilterMisses(ShipAPI ship) {
            this.ship = ship;
        }

        @Override
        public boolean accept(DamagingProjectileAPI proj) {
            if (proj.getOwner() == ship.getOwner()) {
                return false;
            }

            if (proj instanceof MissileAPI) {
                MissileAPI missile = (MissileAPI) proj;
                if (missile.isFlare()) {
                    return false;
                }
            }

            return (CollisionUtils.getCollides(proj.getLocation(), Vector2f.add(proj.getLocation(),
                            (Vector2f) new Vector2f(proj.getVelocity()).scale(ship.getFluxTracker().getTimeToVent() + 1f), null),
                    ship.getLocation(), ship.getCollisionRadius() + 50f) &&
                    Math.abs(MathUtils.getShortestRotation(proj.getFacing(),
                            VectorUtils.getAngle(proj.getLocation(), ship.getLocation()))) <= 90f
            );
        }
    }

    //////////////////////////////////////////
    //          Damage Estimation           //
    //////////////////////////////////////////
    public static float estimateAllIncomingDamage(ShipAPI ship, float damage_window, boolean skip_blocked) {
        float estimated_damage = estimateIncomingProjectileDamage(ship, damage_window, skip_blocked);
        estimated_damage += estimateIncomingBeamDamage(ship, damage_window);
        estimated_damage += estimateIncomingMissileDamage(ship);

        return  estimated_damage;
    }

    public static float estimateIncomingProjectileDamage(ShipAPI ship, float damage_window, boolean skip_blocked) {
        float estimated_damage = 0f;

        for (DamagingProjectileAPI proj : Global.getCombatEngine().getProjectiles()) {
            if (proj.getOwner() == ship.getOwner()) continue; // Skip allied

            Vector2f endPoint = new Vector2f(proj.getVelocity());
            endPoint.scale(damage_window);
            Vector2f.add(endPoint, proj.getLocation(), endPoint);

            if (skip_blocked) {
                if (ship.getShield() != null && ship.getShield().isWithinArc(proj.getLocation()))
                    continue; // Skip if blocked by shield
            }
            if (!CollisionUtils.getCollides(proj.getLocation(), endPoint,
                    new Vector2f(ship.getLocation()), ship.getCollisionRadius()))
                continue; // Skip missing projectiles

            estimated_damage += proj.getDamageAmount() + (proj.getEmpAmount()*0.66f);
        }
        return estimated_damage;
    }

    public static float estimateIncomingBeamDamage(ShipAPI ship, float damage_window) {
        float estimated_damage = 0f;

        for (BeamAPI beam : Global.getCombatEngine().getBeams()) {
            if (beam.getDamageTarget() != ship) continue; //Skip if we aren't the target

            float dps = beam.getWeapon().getDerivedStats().getDamageOver30Sec() / 30;
            float emp = beam.getWeapon().getDerivedStats().getEmpPerSecond();

            DamageType damage_type = beam.getWeapon().getDamageType();
            float damage_multiplier = 1f;

            switch (damage_type) {
                case HIGH_EXPLOSIVE:
                case ENERGY:
                    break;
                case FRAGMENTATION:
                    damage_multiplier = 0.75f;
                    break;
                case KINETIC:
                    damage_multiplier = 0.25f;
                    break;
            }

            estimated_damage += ((dps*damage_multiplier) + (emp*0.66f)) * damage_window;
        }
        return estimated_damage;
    }

    public static float estimateIncomingMissileDamage(ShipAPI ship) {
        float estimated_damage = 0f;

        for (MissileAPI missile : Global.getCombatEngine().getMissiles()) {
            if (missile.getOwner() == ship.getOwner()) continue; // Skip allied

            float safeDistance = 600f + ship.getCollisionRadius();
            if (MathUtils.getDistance(ship.getLocation(), missile.getLocation()) > safeDistance/2)
                continue; // Skip if missile isn't close to us

            if (ship.getShield() != null && ship.getShield().isWithinArc(missile.getLocation()))
                continue; // Skip if blocked by shield

            float threat = missile.getDamageAmount() + missile.getEmpAmount();
            estimated_damage += threat;
        }
        return estimated_damage;
    }
}
