package data.scripts.weapons;

import org.lazywizard.lazylib.combat.CombatUtils;
import data.scripts.util.brdy_Utils;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.listeners.ApplyDamageResultAPI;
import org.lwjgl.util.vector.Vector2f;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.OnHitEffectPlugin;

public class brdy_Hammerclaw_OnHitEffect implements OnHitEffectPlugin
{
    public void onHit(DamagingProjectileAPI projectile, CombatEntityAPI target, Vector2f point, boolean shieldHit, ApplyDamageResultAPI damageResult, CombatEngineAPI engine) {
        if (target instanceof ShipAPI) {
            target = brdy_Utils.getRoot((ShipAPI)target);
            CombatUtils.applyForce(target, projectile.getVelocity(), 900.0f);
        }
    }
}
