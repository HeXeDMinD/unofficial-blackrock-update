package data.scripts.weapons;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.OnHitEffectPlugin;
import com.fs.starfarer.api.combat.listeners.ApplyDamageResultAPI;
import org.lazywizard.lazylib.MathUtils;
import org.lwjgl.util.vector.Vector2f;

import java.awt.*;

public class brdy_VajraCannon_OnHitEffect implements OnHitEffectPlugin
{
    private static final Color EXPLOSION_COLOR = new Color(255, 45, 245, 145);
    private static final Color PARTICLE_COLOR = new Color(195, 250, 70, 255);
    private static final String SOUND_ID = "brdy_weapon_VajraCannon_Impact";

    public void onHit(DamagingProjectileAPI projectile, CombatEntityAPI target, Vector2f point, boolean shieldHit, ApplyDamageResultAPI damageResult, CombatEngineAPI engine) {
        //engine.applyDamage(target, point, MathUtils.getRandomNumberInRange(DAMAGE_MIN, DAMAGE_MAX),DamageType.ENERGY, 0.0f, false, true, projectile.getSource());
        //deals 1-2 damage extra, disabled but kept visuals.

        final Vector2f vel = new Vector2f(target.getVelocity());
        vel.scale(0.5f);
        engine.spawnExplosion(point, vel, brdy_VajraCannon_OnHitEffect.EXPLOSION_COLOR, 255.0f, 0.2f);

        final float speed = projectile.getVelocity().length();
        final float facing = projectile.getFacing();
        for (int x = 0; x < 14; ++x) {
            engine.addHitParticle(point, MathUtils.getPointOnCircumference(null,
                    MathUtils.getRandomNumberInRange(speed * 0.01f, speed * 0.17f),
                    MathUtils.getRandomNumberInRange(facing - 180.0f, facing + 180.0f)),
                    5.0f, 1.0f, 1.6f, PARTICLE_COLOR);
        }

        Global.getSoundPlayer().playSound(SOUND_ID, 1.0f, 1.0f, target.getLocation(), target.getVelocity());
    }
}
