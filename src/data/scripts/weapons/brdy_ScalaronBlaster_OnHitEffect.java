package data.scripts.weapons;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.combat.listeners.ApplyDamageResultAPI;
import data.scripts.brdy_ModPlugin;
import data.scripts.hullmods.TEM_LatticeShield;
import org.lwjgl.util.vector.Vector2f;

import java.awt.*;

public class brdy_ScalaronBlaster_OnHitEffect implements OnHitEffectPlugin
{
    private static final Color EXPLOSION1_COLOR = new Color(49, 242, 214, 70);
    private static final Color EXPLOSION2_COLOR = new Color(135, 255, 180, 50);
    public static final String SOUND_ID = "brdy_weapon_Scalaron_Impact";
    private static final Vector2f ZERO = new Vector2f();

    public void onHit(DamagingProjectileAPI projectile, CombatEntityAPI target, Vector2f point, boolean shieldHit, ApplyDamageResultAPI damageResult, CombatEngineAPI engine) {
        if (!shieldHit && target instanceof ShipAPI) {
            ShipAPI ship = (ShipAPI) target;
            if (brdy_ModPlugin.templarsExists && ship.getVariant().getHullMods().contains("tem_latticeshield") &&
                    TEM_LatticeShield.shieldLevel(ship) > 0f) {
                shieldHit = true;
            }
        }
        if (target instanceof ShipAPI && !shieldHit) {
            engine.spawnExplosion(point, ZERO, EXPLOSION1_COLOR, 155.0f, 1.3f);
            engine.spawnExplosion(point, ZERO, EXPLOSION2_COLOR, 305.0f, 0.6f);

            final float emp = projectile.getEmpAmount() * 0.1f;
            final float damage = projectile.getDamageAmount() * 0.25f;
            for (int x = 0; x < 4f; ++x) {
                engine.spawnEmpArc(projectile.getSource(), point, target, target, DamageType.ENERGY, damage, emp,
                        100000.0f, null, 20.0f,
                        EXPLOSION1_COLOR, EXPLOSION2_COLOR);
            }

            Global.getSoundPlayer().playSound(SOUND_ID, 1.0f, 1.0f, point, ZERO);
        }
    }
}
