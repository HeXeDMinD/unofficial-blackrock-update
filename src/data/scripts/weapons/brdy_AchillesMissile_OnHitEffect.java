package data.scripts.weapons;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.combat.listeners.ApplyDamageResultAPI;
import data.scripts.brdy_ModPlugin;
import data.scripts.hullmods.TEM_LatticeShield;
import org.lwjgl.util.vector.Vector2f;

import java.awt.*;

public class brdy_AchillesMissile_OnHitEffect implements OnHitEffectPlugin {
    public static final Color PARTICLE_COLOUR = new Color(130, 205, 247, 255);
    public static final String SOUND_ID = "brdy_missile_Achilles_Impact";

    public void onHit(DamagingProjectileAPI projectile, CombatEntityAPI target, Vector2f point, boolean shieldHit, ApplyDamageResultAPI damageResult, CombatEngineAPI engine) {
        if (!shieldHit && target instanceof ShipAPI) {
            ShipAPI ship = (ShipAPI) target;
            if (brdy_ModPlugin.templarsExists && ship.getVariant().getHullMods().contains("tem_latticeshield") &&
                    TEM_LatticeShield.shieldLevel(ship) > 0f) {
                shieldHit = true;
            }
        }
        if (target instanceof ShipAPI && !shieldHit) {
            engine.applyDamage(target, point, 75f, DamageType.ENERGY, 500.0f,
                    false, true, projectile.getSource());

            final Vector2f particleVelocity1 = projectile.getVelocity();
            final Vector2f particleVelocity2 = projectile.getVelocity();
            particleVelocity1.scale(0.02f);
            particleVelocity2.scale(0.06f);
            engine.addHitParticle(point, particleVelocity1, 3f, 0.98f, 2f, PARTICLE_COLOUR);
            engine.spawnExplosion(point, particleVelocity1, PARTICLE_COLOUR, 179f, 0.37f);
            engine.spawnExplosion(point, particleVelocity2, PARTICLE_COLOUR, 61f, 1.3f);

            Global.getSoundPlayer().playSound(SOUND_ID, 1.0f, 0.55f, point, projectile.getVelocity());
        }
    }
}
