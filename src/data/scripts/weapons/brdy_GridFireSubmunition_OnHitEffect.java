package data.scripts.weapons;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.OnHitEffectPlugin;
import com.fs.starfarer.api.combat.listeners.ApplyDamageResultAPI;
import org.lazywizard.lazylib.MathUtils;
import org.lwjgl.util.vector.Vector2f;

import java.awt.*;

public class brdy_GridFireSubmunition_OnHitEffect implements OnHitEffectPlugin
{
    private static final Color EXPLOSION_COLOR = new Color(195, 250, 70, 210);
    private static final Color PARTICLE_COLOR = new Color(195, 250, 70, 255);
    private static final String SOUND_ID = "brdy_missile_Gridfire_Explode";
    private static final Vector2f ZERO = new Vector2f();

    public static void pop(final Vector2f point, final CombatEngineAPI engine) {
        engine.spawnExplosion(point, ZERO, EXPLOSION_COLOR, 65.0f, 0.3f);
        Global.getSoundPlayer().playSound(SOUND_ID, 1.0f, 0.5f, point, ZERO);
    }

    public void onHit(DamagingProjectileAPI projectile, CombatEntityAPI target, Vector2f point, boolean shieldHit, ApplyDamageResultAPI damageResult, CombatEngineAPI engine) {
        final Vector2f vel = new Vector2f(target.getVelocity());
        vel.scale(0.5f);
        engine.spawnExplosion(point, vel, EXPLOSION_COLOR, 100.0f, 0.7f);

        final float speed = projectile.getVelocity().length();
        final float facing = projectile.getFacing();
        for (int x = 0; x < 5; ++x) {
            engine.addHitParticle(point, MathUtils.getPointOnCircumference(null,
                    MathUtils.getRandomNumberInRange(speed * 0.01f, speed * 0.2f),
                    MathUtils.getRandomNumberInRange(facing - 180.0f, facing + 180.0f)),
                    5.0f, 1.0f, 1.6f, PARTICLE_COLOR);
        }

        Global.getSoundPlayer().playSound(SOUND_ID, 1.0f, 1.0f, target.getLocation(), target.getVelocity());
    }
}
