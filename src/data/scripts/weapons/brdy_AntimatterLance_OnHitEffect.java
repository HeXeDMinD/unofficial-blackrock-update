package data.scripts.weapons;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.combat.listeners.ApplyDamageResultAPI;
import data.scripts.brdy_ModPlugin;
import data.scripts.hullmods.TEM_LatticeShield;
import data.scripts.util.brdy_Utils;
import org.lazywizard.lazylib.FastTrig;
import org.lazywizard.lazylib.MathUtils;
import org.lwjgl.util.vector.Vector2f;

import java.awt.*;

public class brdy_AntimatterLance_OnHitEffect implements OnHitEffectPlugin
{
    private static final Color EXPLOSION_COLOR = new Color(153, 225, 239, 226);
    private static final Color PARTICLE_COLOR = new Color(130, 235, 187, 247);
    private static final String SOUND_ID = "brdy_weapon_AntimatterLance_Impact";

    public void onHit(DamagingProjectileAPI projectile, CombatEntityAPI target, Vector2f point, boolean shieldHit, ApplyDamageResultAPI damageResult, CombatEngineAPI engine) {
        if (!shieldHit && target instanceof ShipAPI) {
            ShipAPI ship = (ShipAPI) target;
            if (brdy_ModPlugin.templarsExists && ship.getVariant().getHullMods().contains("tem_latticeshield") &&
                    TEM_LatticeShield.shieldLevel(ship) > 0f) {
                shieldHit = true;
            }
        }
        if (target instanceof ShipAPI && !shieldHit) {
            engine.applyDamage(target, point, MathUtils.getRandomNumberInRange(100.0f, 200.0f), DamageType.ENERGY,
                    0.0f, false, true, projectile.getSource());

            final Vector2f vector2f;
            final Vector2f loc = vector2f = new Vector2f(point);
            vector2f.x -= (float)FastTrig.cos(target.getFacing() * 3.141592653589793 / 180.0);
            loc.y -= (float)FastTrig.sin(target.getFacing() * 3.141592653589793 / 180.0);
            final ShipAPI ship = (ShipAPI)target;
            brdy_Utils.createFlare(ship, new Vector2f(loc), engine, 1.0f, 0.05f,
                    0.0f, 10.0f, 1.0f, PARTICLE_COLOR, EXPLOSION_COLOR);

            final Vector2f vel = new Vector2f(target.getVelocity());
            vel.scale(0.45f);
            engine.spawnExplosion(point, vel, EXPLOSION_COLOR, 295.0f, 3.5f);
            final float speed = projectile.getVelocity().length();
            final float facing = projectile.getFacing();
            for (int x = 0; x < 90; ++x) {
                engine.addHitParticle(point, MathUtils.getPointOnCircumference(null,
                        MathUtils.getRandomNumberInRange(speed * 0.004f, speed * 0.044f),
                        MathUtils.getRandomNumberInRange(facing - 180.0f, facing + 180.0f)),
                        7.0f, 1.0f, 4.4f, PARTICLE_COLOR);
            }
            Global.getSoundPlayer().playSound(SOUND_ID, 1.0f, 1.0f, target.getLocation(), target.getVelocity());
        }
    }
}
