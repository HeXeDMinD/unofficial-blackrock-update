package data.scripts.weapons;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.combat.listeners.ApplyDamageResultAPI;
import data.scripts.brdy_ModPlugin;
import data.scripts.hullmods.TEM_LatticeShield;
import org.lwjgl.util.vector.Vector2f;

import java.awt.*;
//Used by Particle Rifle, Fighter Particle Rifle, ESPADA
public class brdy_ParticleWeapon_OnHitEffect implements OnHitEffectPlugin
{
    private static final Color EXPLOSION_COLOR = new Color(246, 237, 190, 205);
    private static final String SOUND_ID = "brdy_weapon_ParticleGuns_Impact";

    public void onHit(DamagingProjectileAPI projectile, CombatEntityAPI target, Vector2f point, boolean shieldHit, ApplyDamageResultAPI damageResult, CombatEngineAPI engine) {
        if (!shieldHit && target instanceof ShipAPI) {
            ShipAPI ship = (ShipAPI) target;
            if (brdy_ModPlugin.templarsExists && ship.getVariant().getHullMods().contains("tem_latticeshield") &&
                    TEM_LatticeShield.shieldLevel(ship) > 0f) {
                shieldHit = true;
            }
        }
        if (target instanceof ShipAPI && !shieldHit) {
            engine.applyDamage(target, point, 30.0f, DamageType.ENERGY, 0.0f,
                    false, true, projectile.getSource());

            final Vector2f vel = new Vector2f(target.getVelocity());
            vel.scale(0.45f);
            engine.spawnExplosion(point, vel, EXPLOSION_COLOR, 35.0f, 0.25f);

            Global.getSoundPlayer().playSound(SOUND_ID, 1.0f, 1.0f, target.getLocation(), target.getVelocity());
        }
    }
}
