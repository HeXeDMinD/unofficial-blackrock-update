package data.scripts.weapons;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.combat.listeners.ApplyDamageResultAPI;
import org.lazywizard.lazylib.MathUtils;
import org.lwjgl.util.vector.Vector2f;

import java.awt.*;

public class brdy_CometFusor_OnHitEffect implements OnHitEffectPlugin
{
    private static final Color EXPLOSION_COLOR = new Color(147, 117, 224, 179);
    private static final Color PARTICLE_COLOR = new Color(248, 238, 255, 255);
    private static final String SOUND_ID = "brdy_weapon_CometFusor_Impact";

    public void onHit(DamagingProjectileAPI projectile, CombatEntityAPI target, Vector2f point, boolean shieldHit, ApplyDamageResultAPI damageResult, CombatEngineAPI engine) {
        engine.applyDamage(target, point, MathUtils.getRandomNumberInRange(50.0f, 100.0f),
                DamageType.FRAGMENTATION, 0.0f, false, true, projectile.getSource());

        final Vector2f vel = new Vector2f(target.getVelocity());
        vel.scale(0.6f);
        engine.spawnExplosion(point, vel, brdy_CometFusor_OnHitEffect.EXPLOSION_COLOR, 100, 0.2f);

        final float speed = projectile.getVelocity().length();
        final float facing = projectile.getFacing();
        for (int x = 0; x < 12; ++x) {
            engine.addHitParticle(point, MathUtils.getPointOnCircumference(null,
                    MathUtils.getRandomNumberInRange(speed * 0.01f, speed * 0.17f),
                    MathUtils.getRandomNumberInRange(facing - 180.0f, facing + 180.0f)),
                    5.0f, 1.0f, 1.6f, PARTICLE_COLOR);
        }

        Global.getSoundPlayer().playSound(SOUND_ID, 1.0f, 1.0f, target.getLocation(), target.getVelocity());
    }
}
