package data.scripts.weapons;

import com.fs.starfarer.api.combat.*;
import org.lazywizard.lazylib.MathUtils;
import org.lwjgl.util.vector.Vector2f;

import java.util.HashMap;
import java.util.Map;

public class brdy_ShotgunEffect implements EveryFrameWeaponEffectPlugin, OnFireEffectPlugin{
    private static final Map<String, Integer> PROJ_COUNT = new HashMap<>();
    static {
        PROJ_COUNT.put("brdy_volleygun_shot", 20);
        PROJ_COUNT.put("brdy_ironweaveradc_shot", 20);
    }

    @Override
    public void advance (float amount, CombatEngineAPI engine, WeaponAPI weapon) {
    }

    @Override
    public void onFire(DamagingProjectileAPI projectile, WeaponAPI weapon, CombatEngineAPI engine) {
        float minSpread = projectile.getWeapon().getSpec().getMinSpread();
        float maxSpread = projectile.getWeapon().getSpec().getMaxSpread();
        Vector2f loc = projectile.getLocation();
        Vector2f vel = projectile.getVelocity();
        int shotCount = PROJ_COUNT.get(projectile.getProjectileSpecId());

        for (int i = 0; i < shotCount; i++) {
            Vector2f randomVel = MathUtils.getRandomPointOnCircumference(
                    null,MathUtils.getRandomNumberInRange(minSpread*10, maxSpread*30));

            randomVel.x += vel.x;
            randomVel.y += vel.y;
            DamagingProjectileAPI proj = (DamagingProjectileAPI) engine.spawnProjectile(weapon.getShip(), weapon,
                    weapon.getId(), loc, projectile.getFacing(), randomVel);
            proj.setDamageAmount(projectile.getBaseDamageAmount()/shotCount);
        }
        engine.removeEntity(projectile);
    }
}
