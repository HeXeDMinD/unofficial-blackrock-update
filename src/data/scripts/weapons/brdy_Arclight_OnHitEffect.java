package data.scripts.weapons;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.combat.listeners.ApplyDamageResultAPI;
import data.scripts.brdy_ModPlugin;
import data.scripts.hullmods.TEM_LatticeShield;
import org.lwjgl.util.vector.Vector2f;

import java.awt.*;

public class brdy_Arclight_OnHitEffect implements OnHitEffectPlugin {
    public static final Color PARTICLE_COLOUR = new Color(49, 242, 222, 55);
    public static final Color PARTICLE_COLOUR2 = new Color(49, 255, 214, 50);
    public static final String SOUND_ID = "brdy_missile_Arclight_Impact";
    private static final Vector2f ZERO  = new Vector2f();

    public static void boom(final Vector2f point, final CombatEngineAPI engine) {
        engine.spawnExplosion(point, ZERO, PARTICLE_COLOUR, 100f, 1.0f);
        engine.spawnExplosion(point, ZERO, PARTICLE_COLOUR2, 200f, 0.55f);
        Global.getSoundPlayer().playSound(SOUND_ID, 1.0f, 0.55f, point, ZERO);
    }

    public void onHit(DamagingProjectileAPI projectile, CombatEntityAPI target, Vector2f point, boolean shieldHit, ApplyDamageResultAPI damageResult, CombatEngineAPI engine) {
        if (!shieldHit && target instanceof ShipAPI) {
            ShipAPI ship = (ShipAPI) target;
            if (brdy_ModPlugin.templarsExists && ship.getVariant().getHullMods().contains("tem_latticeshield") &&
                    TEM_LatticeShield.shieldLevel(ship) > 0f) {
                shieldHit = true;
            }
        }
        if (target instanceof ShipAPI && !shieldHit) {
            engine.spawnExplosion(point, ZERO, PARTICLE_COLOUR, 135.0f, 1.0f);
            engine.spawnExplosion(point, ZERO, PARTICLE_COLOUR2, 250.0f, 0.5f);

            final float emp = projectile.getEmpAmount() * 0.25f;
            final float dam = projectile.getDamageAmount() * 0.25f;
            for (int x = 0; x < 2; ++x) {
                engine.spawnEmpArc(projectile.getSource(), point, target, target, DamageType.ENERGY,
                        dam, emp, 100000.0f, null, 20.0f,
                        PARTICLE_COLOUR, PARTICLE_COLOUR2);
            }

            Global.getSoundPlayer().playSound(SOUND_ID, 1.0f, 0.9f, point, ZERO);
        }
    }
}
