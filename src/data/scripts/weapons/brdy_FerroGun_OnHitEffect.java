package data.scripts.weapons;

import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.combat.listeners.ApplyDamageResultAPI;
import org.lwjgl.util.vector.Vector2f;

import java.awt.*;

public class brdy_FerroGun_OnHitEffect implements OnHitEffectPlugin
{
    private static final Color EXPLOSION_COLOR1 = new Color(244, 241, 240, 130);
    private static final Color EXPLOSION_COLOR2 = new Color(125, 120, 160, 120);

    public void onHit(DamagingProjectileAPI projectile, CombatEntityAPI target, Vector2f point, boolean shieldHit, ApplyDamageResultAPI damageResult, CombatEngineAPI engine) {
        final Vector2f vel = new Vector2f(target.getVelocity());
        vel.scale(0.5f);
        engine.spawnExplosion(point, vel, EXPLOSION_COLOR1, 35.0f, 0.2f);
        engine.spawnExplosion(point, vel, EXPLOSION_COLOR2, 75.0f, 0.6f);
        engine.spawnExplosion(point, vel, EXPLOSION_COLOR2, 100.0f, 1.0f);
    }
}
