package data.scripts.weapons;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.OnHitEffectPlugin;
import com.fs.starfarer.api.combat.listeners.ApplyDamageResultAPI;
import org.lazywizard.lazylib.MathUtils;
import org.lwjgl.util.vector.Vector2f;

import java.awt.*;

public class brdy_FuryTorpedo_OnHitEffect implements OnHitEffectPlugin
{
    private static final Color EXPLOSION_COLOR = new Color(195, 250, 70, 210);
    private static final Color PARTICLE_COLOR = new Color(195, 250, 70, 255);
    private static final String SOUND_ID = "brdy_missile_Fury_Impact";

    public void onHit(DamagingProjectileAPI projectile, CombatEntityAPI target, Vector2f point, boolean shieldHit, ApplyDamageResultAPI damageResult, CombatEngineAPI engine) {
        final Vector2f vel = new Vector2f(target.getVelocity());
        vel.scale(0.5f);
        engine.spawnExplosion(point, vel, EXPLOSION_COLOR, 655.0f, 0.15f);

        final float speed = projectile.getVelocity().length();
        final float facing = projectile.getFacing();
        for (int x = 0; x < 9; ++x) {
            engine.addHitParticle(point, MathUtils.getPointOnCircumference(null,
                    MathUtils.getRandomNumberInRange(speed * 0.09f, speed * 0.57f),
                    MathUtils.getRandomNumberInRange(facing - 180.0f, facing + 180.0f)),
                    5.0f, 1.0f, 1.6f, PARTICLE_COLOR);
        }
        Global.getSoundPlayer().playSound(SOUND_ID, 0.7f, 1.0f, target.getLocation(), target.getVelocity());
    }
}
