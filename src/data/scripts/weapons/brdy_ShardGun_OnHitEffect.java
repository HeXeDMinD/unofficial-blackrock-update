package data.scripts.weapons;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.combat.listeners.ApplyDamageResultAPI;
import data.scripts.brdy_ModPlugin;
import data.scripts.hullmods.TEM_LatticeShield;
import org.lazywizard.lazylib.MathUtils;
import org.lwjgl.util.vector.Vector2f;

import java.awt.*;

public class brdy_ShardGun_OnHitEffect implements OnHitEffectPlugin
{
    private static final Color EXPLOSION_COLOR = new Color(130, 235, 217, 255);
    private static final Color PARTICLE_COLOR = new Color(130, 235, 217, 255);
    private static final String SOUND_ID = "brdy_weapon_Shard_Explode";

    public void onHit(DamagingProjectileAPI projectile, CombatEntityAPI target, Vector2f point, boolean shieldHit, ApplyDamageResultAPI damageResult, CombatEngineAPI engine) {
        if (!shieldHit && target instanceof ShipAPI) {
            ShipAPI ship = (ShipAPI) target;
            if (brdy_ModPlugin.templarsExists && ship.getVariant().getHullMods().contains("tem_latticeshield") &&
                    TEM_LatticeShield.shieldLevel(ship) > 0f) {
                shieldHit = true;
            }
        }
        if (target instanceof ShipAPI && !shieldHit && Math.random() <= 0.2f) {
            engine.applyDamage(target, point, MathUtils.getRandomNumberInRange(100.0f, 150.0f),
                    DamageType.ENERGY, 0.0f, false, true,
                    projectile.getSource());

            final Vector2f vel = new Vector2f(target.getVelocity());
            vel.scale(0.48f);
            engine.spawnExplosion(point, vel, EXPLOSION_COLOR, 39.0f, 1.0f);

            final float speed = projectile.getVelocity().length();
            final float facing = projectile.getFacing();
            for (int x = 0; x < 10; ++x) {
                engine.addHitParticle(point, MathUtils.getPointOnCircumference(null,
                        MathUtils.getRandomNumberInRange(speed * 0.007f, speed * 0.17f),
                        MathUtils.getRandomNumberInRange(facing - 180.0f, facing + 180.0f)),
                        5.0f, 1.0f, 1.6f, PARTICLE_COLOR);
            }

            Global.getSoundPlayer().playSound(SOUND_ID, 1.1f, 0.5f, target.getLocation(), target.getVelocity());
        }
    }
}
