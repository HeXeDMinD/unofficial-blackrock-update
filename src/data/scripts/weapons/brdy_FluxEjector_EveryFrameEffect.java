package data.scripts.weapons;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.EveryFrameWeaponEffectPlugin;
import com.fs.starfarer.api.combat.WeaponAPI;

public class brdy_FluxEjector_EveryFrameEffect implements EveryFrameWeaponEffectPlugin {
    private int last_weapon_ammo = 0;

    @Override
    public void advance(float amount, CombatEngineAPI engine, WeaponAPI weapon) {
        if (engine.isPaused()) {
            return;
        }
        int weapon_ammo = weapon.getAmmo();
        if (weapon_ammo < last_weapon_ammo) {
            weapon.getShip().getFluxTracker().decreaseFlux(1000f);
        }
        last_weapon_ammo = weapon_ammo;
    }
}
