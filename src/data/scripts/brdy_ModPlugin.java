package data.scripts;

import com.fs.starfarer.api.BaseModPlugin;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.PluginPick;
import com.fs.starfarer.api.campaign.CampaignPlugin;
import com.fs.starfarer.api.combat.MissileAIPlugin;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.impl.campaign.procgen.ProcgenUsedNames;
import data.scripts.ai.*;
import data.scripts.world.blackrock.brdy_GenerateSector;
import exerelin.campaign.SectorManager;
import org.dark.shaders.light.LightData;
import org.dark.shaders.util.ShaderLib;
import org.dark.shaders.util.TextureData;

public class brdy_ModPlugin extends BaseModPlugin {
    private static final String BRDY_DART_PROJ = "brdy_dartmissile_shot";
    private static final String BRDY_SPL_LIGHT_PROJ = "brdy_lightspl_shot";
    private static final String BRDY_SPL_HEAVY_PROJ = "brdy_heavyspl_shot";
    private static final String BRDY_SCALARONREPULSORP_PROJ = "brdy_scalaronrepulsor_shot";
    private static final String BRDY_ARCLIGHT_PROJ = "brdy_arclightlfo_shot";
    public static final String BRDY_VOIDSPEAR_PROJ = "brdy_voidspear_shot";
    private static final String BRDY_GRIDFIRE_PROJ = "brdy_gridfire_stage1_shot";
    private static final String BRDY_GRIDFIRE_SUBMUNITION_PROJ = "brdy_gridfire_stage2_shot";

    public static boolean templarsExists = false;

    @Override
    public PluginPick<MissileAIPlugin> pickMissileAI(MissileAPI missile, ShipAPI launchingShip) {
        switch (missile.getProjectileSpecId()) {
            case BRDY_DART_PROJ: {
                return new PluginPick<MissileAIPlugin>(new brdy_MissileAI_Dart(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SET);
            }
            case BRDY_SPL_LIGHT_PROJ:
            case BRDY_SPL_HEAVY_PROJ:
            case BRDY_SCALARONREPULSORP_PROJ: {
                return new PluginPick<MissileAIPlugin>(new brdy_MissileAI_EnergyTorpedo(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SET);
            }
            case BRDY_ARCLIGHT_PROJ: {
                return new PluginPick<MissileAIPlugin>(new brdy_MissileAI_Arclight(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SET);
            }
            case BRDY_VOIDSPEAR_PROJ: {
                return new PluginPick<MissileAIPlugin>(new brdy_MissileAI_VoidSpear(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SET);
            }
            case BRDY_GRIDFIRE_PROJ: {
                return new PluginPick<MissileAIPlugin>(new brdy_MissileAI_Gridfire(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SET);
            }
            case BRDY_GRIDFIRE_SUBMUNITION_PROJ: {
                return new PluginPick<MissileAIPlugin>(new brdy_MissileAI_GridfireSubmunition(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SET);
            }
            default:
                return null;
        }
    }

    @Override
    public void onApplicationLoad() {
        templarsExists = Global.getSettings().getModManager().isModEnabled("Templars");
        try {
            Global.getSettings().getScriptClassLoader().loadClass("org.dark.shaders.util.ShaderLib");
            ShaderLib.init();
            LightData.readLightDataCSV("data/lights/brdy_light_data.csv");
            TextureData.readTextureDataCSV("data/lights/brdy_texture_data.csv");
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onNewGame() {
        boolean haveNexerelin = Global.getSettings().getModManager().isModEnabled("nexerelin");
        if (haveNexerelin && !SectorManager.getManager().isCorvusMode()) {
            return;
        }

        ProcgenUsedNames.notifyUsed("Gneiss");
        ProcgenUsedNames.notifyUsed("Blackrock");
        ProcgenUsedNames.notifyUsed("Lodestone");
        ProcgenUsedNames.notifyUsed("Augustmoon");
        ProcgenUsedNames.notifyUsed("Creir");
        ProcgenUsedNames.notifyUsed("Lydia");
        ProcgenUsedNames.notifyUsed("Verge");
        ProcgenUsedNames.notifyUsed("Vigil");
        ProcgenUsedNames.notifyUsed("Preclusion");

        ProcgenUsedNames.notifyUsed("Rama");
        ProcgenUsedNames.notifyUsed("Sita");
        ProcgenUsedNames.notifyUsed("Bharata");
        ProcgenUsedNames.notifyUsed("Staalo");
        ProcgenUsedNames.notifyUsed("Senroamin");
        ProcgenUsedNames.notifyUsed("Sarayu");
        ProcgenUsedNames.notifyUsed("Niji");
        ProcgenUsedNames.notifyUsed("Vena");
        ProcgenUsedNames.notifyUsed("Limbo");
        ProcgenUsedNames.notifyUsed("Thalm");

        new brdy_GenerateSector().generate(Global.getSector());
    }
}