package data.scripts.world.blackrock.systems;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.JumpPointAPI;
import com.fs.starfarer.api.campaign.OrbitAPI;
import com.fs.starfarer.api.campaign.PlanetAPI;
import com.fs.starfarer.api.campaign.SectorAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.impl.campaign.ids.Conditions;
import com.fs.starfarer.api.impl.campaign.ids.Industries;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.Submarkets;
import com.fs.starfarer.api.impl.campaign.ids.Terrain;
import com.fs.starfarer.api.impl.campaign.procgen.NebulaEditor;
import com.fs.starfarer.api.impl.campaign.procgen.StarAge;
import com.fs.starfarer.api.impl.campaign.procgen.StarSystemGenerator;
import com.fs.starfarer.api.impl.campaign.terrain.BaseRingTerrain.RingParams;
import com.fs.starfarer.api.impl.campaign.terrain.HyperspaceTerrainPlugin;
import com.fs.starfarer.api.impl.campaign.terrain.MagneticFieldTerrainPlugin.MagneticFieldParams;
import com.fs.starfarer.api.util.Misc;
import data.campaign.brdy_Conditions;
import data.campaign.ids.brdy_Factions;
import data.campaign.ids.brdy_Industries;
import data.scripts.world.blackrock.brdy_addMarketplace;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;

@SuppressWarnings("unused")
public class brdy_Rama {
    private static final String FACTION_BLACKROCK = brdy_Factions.BRDY;

    public void generate(SectorAPI sector) {
        StarSystemAPI system = sector.createStarSystem("Rama");
        system.getLocation().set(11500, -16000);
        system.setBackgroundTextureFilename("graphics/BR/backgrounds/brdy_rama_background.jpg");

        PlanetAPI star_Rama = system.initStar("Rama", "star_red_giant", 590f, 690f);
        system.setLightColor(new Color(255, 220, 200)); // light color in entire system, affects all entities

        SectorEntityToken nebula_Rama = Misc.addNebulaFromPNG("data/campaign/terrain/brdy_rama_nebula.png",
                                                              0, 0, // center of nebula
                                                              system, // location to add to
                                                              "terrain", "nebula_amber", // "nebula_blue", // texture to use, uses xxx_map for map
                                                              4, 4, StarAge.OLD); // number of cells in texture

        PlanetAPI star_Sita = system.addPlanet("star_Sita", star_Rama, "Sita", "star_white", 35,
                90, 20500, 1150);
        system.addCorona(star_Sita, 150, 3f, 0.05f, 1f);
        //silencestar.setCustomDescriptionId("brdy_star_sita");
        system.addAsteroidBelt(star_Sita, 50, 2000, 256, 200, 200);

        system.addAsteroidBelt(star_Rama, 50, 1200, 255, 65, 45); // 0.32 AU

        PlanetAPI planet_Bharata = system.addPlanet("planet_Bharata", star_Rama, "Bharata", "arid",
                300, 75, 7600, 130);
        planet_Bharata.getSpec().setGlowTexture(Global.getSettings().getSpriteName("hab_glows", "asharu"));
        planet_Bharata.getSpec().setGlowColor(new Color(255, 245, 235, 255));
        planet_Bharata.getSpec().setUseReverseLightForGlow(true);
        planet_Bharata.applySpecChanges();
        planet_Bharata.setCustomDescriptionId("brdy_planet_bharata_description");
        planet_Bharata.setInteractionImage("illustrations", "cargo_loading");

        PlanetAPI planet_SitaI = system.addPlanet("planet_SitaI", star_Sita, "Sita I",
                "barren-bombarded", 10, 35, 650, -40);
		Misc.initConditionMarket(planet_SitaI);
        planet_SitaI.getMarket().addCondition(Conditions.NO_ATMOSPHERE);

        PlanetAPI planet_Staalo = system.addPlanet("planet_Staalo", star_Sita, "Staalo",
                "brdy_planet_staalo", 30, 70, 2400, -115);
        planet_Staalo.getSpec().setGlowTexture(Global.getSettings().getSpriteName("hab_glows", "sindria"));
        planet_Staalo.getSpec().setGlowColor(new Color(255, 245, 235, 255));
        planet_Staalo.getSpec().setUseReverseLightForGlow(true);
        planet_Staalo.applySpecChanges();
        planet_Staalo.setCustomDescriptionId("brdy_planet_staalo_description");
        planet_Staalo.setInteractionImage("illustrations", "urban03");

        PlanetAPI planet_Senroamin = system.addPlanet("planet_Senroamin", star_Rama, "Senroamin",
                "brdy_planet_senroamin", 45, 320, 12150, 280);
        planet_Senroamin.setCustomDescriptionId("brdy_planet_senroamin_description");
        planet_Senroamin.getSpec().setGlowTexture(Global.getSettings().getSpriteName("hab_glows", "banded"));
        planet_Senroamin.getSpec().setGlowColor(new Color(177, 144, 162, 85));
        planet_Senroamin.getSpec().setUseReverseLightForGlow(true);
        planet_Senroamin.applySpecChanges();

        SectorEntityToken magField_Senroamin = system.addTerrain(Terrain.MAGNETIC_FIELD,
                new MagneticFieldParams(
                        200f, // terrain effect band width
                        400, // terrain effect middle radius
                        planet_Senroamin, // entity that it's around
                        300f, // visual band start
                        500f, // visual band end
                        new Color(50, 30, 100, 30), // base color
                        1f, // probability to spawn aurora sequence, checked once/day when no aurora in progress
                        new Color(50, 20, 110, 130),
                        new Color(150, 30, 120, 150),
                        new Color(200, 50, 130, 190),
                        new Color(250, 70, 150, 240),
                        new Color(200, 80, 130, 255),
                        new Color(75, 0, 160),
                        new Color(127, 0, 255)
                )
        );
        magField_Senroamin.setCircularOrbit(planet_Senroamin, 0, 0, 100);

        system.addRingBand(planet_Senroamin, "misc", "rings_dust0", 256f, 2,
                new Color(143, 118, 107, 190), 256f, 570, 16.95f);

        system.addRingBand(star_Rama, "misc", "rings_dust0", 256f, 2, Color.white,
                256f, 1200, 30f);
        system.addRingBand(star_Rama, "misc", "rings_dust0", 256f, 3, Color.white,
                256f, 1400, 40f);
        system.addRingBand(star_Rama, "misc", "rings_dust0", 256f, 2, Color.white,
                256f, 1600, 50f);
        system.addRingBand(star_Rama, "misc", "rings_dust0", 256f, 3, Color.white,
                256f, 1800, 56f);
        system.addRingBand(star_Rama, "misc", "brdy_dusty_ring", 1024f, 0,
                new Color(148, 128, 97, 245), 2048f, 3000, 59);

        system.addRingBand(star_Rama, "misc", "rings_dust0", 256f, 2, Color.white,
                256f, 1000, 80f);
        system.addRingBand(star_Rama, "misc", "rings_dust0", 256f, 3, Color.white,
                256f, 1100, 120f);
        system.addRingBand(star_Rama, "misc", "rings_dust0", 256f, 2, Color.white,
                256f, 1200, 160f);

        SectorEntityToken ring = system.addTerrain(Terrain.RING, new RingParams(800 + 256,
                1400, null, "Great Rings of Sarayu"));
        ring.setCircularOrbit(star_Rama, 0, 0, 100);

        SectorEntityToken relay = system.addCustomEntity("rama_relay", "Rama Relay",
                "comm_relay", FACTION_BLACKROCK);
        relay.setCircularOrbit(star_Rama, 240, 3650, 240);

        system.addAsteroidBelt(star_Rama, 70, 5600, 128, 440, 120);
        system.addAsteroidBelt(star_Rama, 80, 5900, 128, 410, 150);
        system.addAsteroidBelt(star_Rama, 80, 15900, 128, 400, 250);
        system.addAsteroidBelt(star_Rama, 65, 15100, 128, 420, 290);
        system.addAsteroidBelt(star_Rama, 80, 15500, 128, 415, 340);
        system.addAsteroidBelt(star_Rama, 70, 15000, 128, 415, 250);
        system.addAsteroidBelt(star_Rama, 75, 15500, 128, 425, 300);
        
		SectorEntityToken stableloc = system.addCustomEntity(null,null, "stable_location",Factions.NEUTRAL);
        stableloc.setCircularOrbitPointingDown(star_Rama, 0, 9500, 450f);

		SectorEntityToken stableloc2 = system.addCustomEntity(null,null, "stable_location",Factions.NEUTRAL);
        stableloc2.setCircularOrbitPointingDown(star_Rama, 180, 9500, 450f);

        system.addRingBand(star_Rama, "misc", "rings_dust0", 256f, 2, Color.white,
                256f, 15200, 500f);
        system.addRingBand(star_Rama, "misc", "rings_dust0", 256f, 3,
                new Color(164, 195, 225, 200), 512f, 15250, 530f);
        system.addRingBand(star_Rama, "misc", "rings_dust0", 256f, 2, Color.white,
                256f, 15300, 540f);
        system.addRingBand(star_Rama, "misc", "brdy_dusty_ring", 1024f, 0,
                new Color(164, 195, 225, 220), 2048f, 16000, 550);

        system.addRingBand(star_Rama, "misc", "rings_dust0", 256f, 2,
                Color.lightGray, 256f, 17200, 625f);
        system.addRingBand(star_Rama, "misc", "rings_dust0", 256f, 3,
                Color.darkGray, 512f, 17070, 615f);
        system.addRingBand(star_Rama, "misc", "rings_dust0", 256f, 3,
                Color.white, 512f, 17050, 600f);
        system.addRingBand(star_Rama, "misc", "rings_dust0", 256f, 3,
                Color.lightGray, 256f, 17100, 620f);

        ring = system.addTerrain(Terrain.RING, new RingParams(1850 + 256, 16200,
                null, "The Shattered Bands"));
        ring.setCircularOrbit(star_Rama, 0, 0, 100);

        PlanetAPI planet_Niji = system.addPlanet("planet_Niji", star_Rama, "Niji",
                "ice_giant", 30, 160, 16036, 375);
        planet_Niji.getSpec().setPlanetColor(new Color(50, 100, 255, 255));
        planet_Niji.getSpec().setAtmosphereColor(new Color(120, 130, 130, 100));
        planet_Niji.getSpec().setCloudColor(new Color(195, 230, 255, 200));
        planet_Niji.getSpec().setIconColor(new Color(110, 130, 140, 255));
        planet_Niji.getSpec().setGlowTexture(Global.getSettings().getSpriteName("hab_glows", "aurorae"));
        planet_Niji.getSpec().setGlowColor(new Color(135, 208, 235, 105));
        planet_Niji.getSpec().setUseReverseLightForGlow(true);
        planet_Niji.getSpec().setAtmosphereThickness(0.5f);
        planet_Niji.applySpecChanges();

        PlanetAPI planet_Vena = system.addPlanet("planet_Vena", star_Rama, "Vena",
                "brdy_planet_vena", 30, 80, 3836, 215);
        planet_Vena.getSpec().setAtmosphereColor(new Color(184, 226, 145, 102));
        planet_Vena.getSpec().setCloudColor(new Color(195, 230, 255, 200));
        planet_Vena.setCustomDescriptionId("brdy_planet_vena_description");
        planet_Vena.applySpecChanges();
        
		// Add fixed conditions to Vena
		Misc.initConditionMarket(planet_Vena);
		planet_Vena.getMarket().addCondition(brdy_Conditions.VENA_CON);
		planet_Vena.getMarket().addCondition(Conditions.TECTONIC_ACTIVITY);
		planet_Vena.getMarket().addCondition(Conditions.METEOR_IMPACTS);

        SectorEntityToken magField_Vena = system.addTerrain(Terrain.MAGNETIC_FIELD,
                new MagneticFieldParams(
                        140f, // terrain effect band width
                        140f, // terrain effect middle radius
                        planet_Vena, // entity that it's around
                        60f, // visual band start
                        140f, // visual band end
                        new Color(19, 100, 94), // base color
                        0.90f, // probability to spawn aurora sequence, checked once/day when no aurora in progress
                        new Color(69, 220, 164),
                        new Color(129, 160, 174),
                        new Color(109, 180, 154),
                        new Color(49, 200, 134),
                        new Color(220, 230, 114),
                        new Color(129, 230, 154),
                        new Color(89, 240, 164)
                )
        );
        magField_Vena.setCircularOrbit(planet_Vena, 0, 0, 100);

        PlanetAPI planet_Limbo = system.addPlanet("planet_Limbo", planet_Niji, "Limbo",
                "rocky_ice", 30, 35, 520, 25);
        planet_Limbo.getSpec().setIconColor(new Color(255, 0, 0, 255));
        planet_Limbo.applySpecChanges();
        planet_Limbo.setCustomDescriptionId("brdy_planet_limbo_description");

        PlanetAPI planet_Thalm = system.addPlanet("planet_Thalm", planet_Senroamin, "Thalm",
                "rocky_unstable", 100, 30, 1100, 26);
        Misc.initConditionMarket(planet_Thalm);
        planet_Vena.getMarket().addCondition(Conditions.NO_ATMOSPHERE);
        planet_Vena.getMarket().addCondition(Conditions.TECTONIC_ACTIVITY);
        

        SectorEntityToken listenpost_Thalm = system.addCustomEntity("listenpost_Thalm", "Thalm Listening Post",
                "station_side04", "tritachyon");
        listenpost_Thalm.setCircularOrbitPointingDown(system.getEntityById("planet_Thalm"),
                90, 200, 7);
        listenpost_Thalm.setCustomDescriptionId("brdy_listeningoutpost_description");

        JumpPointAPI jumpPoint_Bharata = Global.getFactory().createJumpPoint("jumpPoint_Bharata", "Jump Point Bharata");
        OrbitAPI orbit = Global.getFactory().createCircularOrbit(planet_Bharata, 90, 900, 55);
        jumpPoint_Bharata.setOrbit(orbit);
        jumpPoint_Bharata.setRelatedPlanet(planet_Bharata);
        jumpPoint_Bharata.setStandardWormholeToHyperspaceVisual();
        system.addEntity(jumpPoint_Bharata);

        JumpPointAPI jumpPoint_Staalo = Global.getFactory().createJumpPoint("jumpPoint_Staalo", "Jump Point Staalo");
        OrbitAPI orbit2 = Global.getFactory().createCircularOrbit(planet_Staalo, 45, 1200, -53);
        jumpPoint_Staalo.setRelatedPlanet(planet_Staalo);
        jumpPoint_Staalo.setOrbit(orbit2);
        jumpPoint_Staalo.setStandardWormholeToHyperspaceVisual();
        system.addEntity(jumpPoint_Staalo);

        JumpPointAPI jumpPoint_Rama_Gate = Global.getFactory().createJumpPoint("jumpPoint_Rama_Gate", "Rama Gate");
        OrbitAPI orbit3 = Global.getFactory().createCircularOrbit(star_Rama, 220, 12000, 500);
        jumpPoint_Rama_Gate.setOrbit(orbit3);
        jumpPoint_Rama_Gate.setStandardWormholeToHyperspaceVisual();
        system.addEntity(jumpPoint_Rama_Gate);

        brdy_addMarketplace.addMarketplace(FACTION_BLACKROCK, planet_Bharata,
                                      null,
                                      "Bharata",
                                      5,
                                      new ArrayList<>(Arrays.asList(
                                              Conditions.ARID,
                                              Conditions.HABITABLE,
                                              Conditions.HOT,
                                              Conditions.FARMLAND_POOR,
                                              Conditions.ORGANICS_COMMON,
                                              Conditions.ORE_SPARSE,
                                              Conditions.POPULATION_5
                                      )),
                                      new ArrayList<>(Arrays.asList(
                                              Industries.POPULATION,
                                              Industries.MEGAPORT,
                                              brdy_Industries.BRDYDEFHQ,
                                              Industries.MILITARYBASE,
                                              //Industries.HEAVYINDUSTRY,
                                              Industries.HEAVYBATTERIES,
                                              //Industries.PATROLHQ,
                                              Industries.BATTLESTATION_MID,
                                              Industries.MINING,
                                              Industries.FARMING
                                      )),
                                      new ArrayList<>(Arrays.asList(
                                              Submarkets.SUBMARKET_STORAGE,
                                              Submarkets.GENERIC_MILITARY,
                                              Submarkets.SUBMARKET_BLACK,
                                              Submarkets.SUBMARKET_OPEN
                                      )),
                                      0.3f
        );

        brdy_addMarketplace.addMarketplace(Factions.HEGEMONY, planet_Staalo,
                                      null,
                                      "Staalo",
                                      5,
                                      new ArrayList<>(Arrays.asList(
                                              Conditions.NO_ATMOSPHERE,
                                              Conditions.DISSIDENT,
                                              Conditions.RARE_ORE_SPARSE,
                                              Conditions.ORE_MODERATE,
                                              Conditions.POPULATION_5
                                      )),
                                      new ArrayList<>(Arrays.asList(
                                              Industries.POPULATION,
                                              Industries.MILITARYBASE,
                                              Industries.HEAVYBATTERIES,
                                              Industries.SPACEPORT,
                                              //Industries.PATROLHQ,
                                              Industries.BATTLESTATION,
                                              Industries.MINING,
                                              Industries.REFINING
                                      )),
                                      new ArrayList<>(Arrays.asList(
                                              Submarkets.SUBMARKET_STORAGE,
                                              Submarkets.GENERIC_MILITARY,
                                              Submarkets.SUBMARKET_BLACK,
                                              Submarkets.SUBMARKET_OPEN
                                      )),
                                      0.3f
        );

        brdy_addMarketplace.addMarketplace(Factions.TRITACHYON, listenpost_Thalm,
                                      null,
                                      "Thalm Listening Post",
                                      3,
                                      new ArrayList<>(Arrays.asList(
                                              Conditions.FREE_PORT,
                                              Conditions.OUTPOST,
                                              Conditions.POPULATION_3
                                      )),
                                      new ArrayList<>(Arrays.asList(
                                              Industries.POPULATION,
                                              Industries.MILITARYBASE,
                                              Industries.HEAVYBATTERIES,
                                              Industries.SPACEPORT,
                                              Industries.WAYSTATION,
                                              //Industries.PATROLHQ,
                                              Industries.BATTLESTATION_HIGH
                                              //Industries.LIGHTINDUSTRY
                                      )),
                                      new ArrayList<>(Arrays.asList(
                                              Submarkets.SUBMARKET_STORAGE,
                                              Submarkets.GENERIC_MILITARY,
                                              Submarkets.SUBMARKET_BLACK,
                                              Submarkets.SUBMARKET_OPEN
                                      )),
                                      0.3f
        );

        brdy_addMarketplace.addMarketplace(Factions.PIRATES, planet_Limbo,
                                      null,
                                      "Limbo",
                                      4,
                                      new ArrayList<>(Arrays.asList(
                                              Conditions.ICE,
                                              Conditions.COLD,
                                              Conditions.EXTREME_WEATHER,
                                              Conditions.VOLATILES_PLENTIFUL,
                                              Conditions.FRONTIER,
                                              Conditions.FREE_PORT,
                                              Conditions.OUTPOST,
                                              Conditions.POPULATION_4
                                      )),
                                      new ArrayList<>(Arrays.asList(
                                              Industries.POPULATION,
                                              //Industries.MILITARYBASE,
                                              Industries.PATROLHQ, // Added
                                              Industries.GROUNDDEFENSES,
                                              Industries.SPACEPORT,
                                              Industries.MINING,
                                              Industries.REFINING
                                      )),
                                      new ArrayList<>(Arrays.asList(
                                              Submarkets.SUBMARKET_STORAGE,
                                              Submarkets.SUBMARKET_BLACK,
                                              Submarkets.SUBMARKET_OPEN
                                      )),
                                      0.3f
        );

        system.autogenerateHyperspaceJumpPoints(true, true);

        float radiusAfter = StarSystemGenerator.addOrbitingEntities(
                system,
                star_Rama,
                StarAge.OLD,
                2,
                4,
                22800,
                3,
                true
        );
        HyperspaceTerrainPlugin plugin = (HyperspaceTerrainPlugin) Misc.getHyperspaceTerrain().getPlugin();
        NebulaEditor editor = new NebulaEditor(plugin);
        float minRadius = plugin.getTileSize() * 2f;

        float radius = system.getMaxRadiusInHyperspace();
        editor.clearArc(system.getLocation().x, system.getLocation().y, 0, radius + minRadius,
                0, 360f);
        editor.clearArc(system.getLocation().x, system.getLocation().y, 0, radius + minRadius,
                0, 360f, 0.25f);
    }
}
