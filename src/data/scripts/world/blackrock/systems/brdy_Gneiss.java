package data.scripts.world.blackrock.systems;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.JumpPointAPI;
import com.fs.starfarer.api.campaign.OrbitAPI;
import com.fs.starfarer.api.campaign.PlanetAPI;
import com.fs.starfarer.api.campaign.SectorAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.impl.campaign.ids.Items;
import com.fs.starfarer.api.impl.campaign.ids.Conditions;
import com.fs.starfarer.api.impl.campaign.ids.Industries;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.Submarkets;
import com.fs.starfarer.api.impl.campaign.procgen.NebulaEditor;
import com.fs.starfarer.api.impl.campaign.procgen.StarAge;
import com.fs.starfarer.api.impl.campaign.procgen.StarSystemGenerator;
import com.fs.starfarer.api.impl.campaign.terrain.HyperspaceTerrainPlugin;
import com.fs.starfarer.api.util.Misc;
import data.campaign.brdy_Conditions;
import data.campaign.ids.brdy_Industries;
import data.scripts.world.blackrock.brdy_addMarketplace;
import data.campaign.ids.brdy_Factions;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

@SuppressWarnings("unused")
public class brdy_Gneiss {
    private static final String FACTION_BLACKROCK = brdy_Factions.BRDY;

    public void generate(SectorAPI sector) {

        StarSystemAPI system = sector.createStarSystem("Gneiss");
        system.getLocation().set(18000, -18600);
        system.setBackgroundTextureFilename("graphics/BR/backgrounds/brdy_gneiss_background.jpg");

        PlanetAPI star_Gneiss = system.initStar("star_Gneiss", "brdy_star_gneiss", 450f, 500f);

        SectorEntityToken gneiss_nebula = Misc.addNebulaFromPNG("data/campaign/terrain/brdy_gneiss_nebula.png",
                0, 0, system, "terrain",
                "brdy_nebula_gneiss", 4, 4, StarAge.AVERAGE);

        PlanetAPI planet_Blackrock = system.addPlanet("planet_Blackrock", star_Gneiss, "Blackrock",
                "brdy_planet_blackrock", 300, 140, 3400, 215);
        planet_Blackrock.getSpec().setGlowTexture(Global.getSettings().getSpriteName(
                "planet_glows", "brdy_blackrock_glow"));
        planet_Blackrock.getSpec().setGlowColor(new Color(255, 255, 255, 255));
        planet_Blackrock.getSpec().setUseReverseLightForGlow(true);
        planet_Blackrock.applySpecChanges();
        planet_Blackrock.setInteractionImage("illustrations", "brdy_blackrock_sky_city");
        planet_Blackrock.setCustomDescriptionId("brdy_planet_blackrock_description");

        SectorEntityToken stellarMirror_Blackrock = system.addCustomEntity("brdy_stellarmirror",
                "Blackrock Stellar Mirror", "brdy_stellarmirror", FACTION_BLACKROCK);
        stellarMirror_Blackrock.setFacing(120f);
        stellarMirror_Blackrock.setCircularOrbitWithSpin(planet_Blackrock, 120, 1200, 215,
                1f / 2150f, 1f / 2150f); // Interposed between planet_Blackrock and star_Gneiss

        PlanetAPI planet_Lodestone = system.addPlanet("planet_Lodestone", planet_Blackrock, "Lodestone",
                "brdy_planet_lodestone", 30, 50, 500, 25); // 0.0025 AU
        planet_Lodestone.setCustomDescriptionId("brdy_planet_lodestone_description");
        planet_Lodestone.setInteractionImage("illustrations", "space_bar");
        planet_Lodestone.getSpec().setRotation(5f); // 5 degrees/second = 7.2 days/revolution
        planet_Lodestone.getSpec().setGlowTexture(Global.getSettings().getSpriteName("hab_glows", "sindria"));
        planet_Lodestone.getSpec().setGlowColor(new Color(255, 255, 255, 255));
        planet_Lodestone.getSpec().setUseReverseLightForGlow(true);
        planet_Lodestone.applySpecChanges();

        SectorEntityToken station_Augustmoon = system.addCustomEntity("brdy_augustmoon_station",
                "Port Augustmoon", "brdy_augustmoon_station", FACTION_BLACKROCK);
        station_Augustmoon.setCircularOrbitPointingDown(planet_Lodestone, 270, 75, -7.2f); // Locked to planet_Lodestone
        station_Augustmoon.setInteractionImage("illustrations", "city_from_above");

        SectorEntityToken relay_Gneiss = system.addCustomEntity("gneiss_relay", "Gneiss Relay",
                "comm_relay", FACTION_BLACKROCK);
        relay_Gneiss.setCircularOrbit(star_Gneiss, 220, 3650, 215);
        
		SectorEntityToken stableloc = system.addCustomEntity(null,null, "stable_location", Factions.NEUTRAL);
        stableloc.setCircularOrbitPointingDown(star_Gneiss, 40, 3650, 215f);
                
		SectorEntityToken stableloc2 = system.addCustomEntity(null,null, "stable_location", Factions.NEUTRAL);
        stableloc2.setCircularOrbitPointingDown(star_Gneiss, 310, 3650, 215f);

        PlanetAPI planet_Creir = system.addPlanet("planet_Creir", star_Gneiss, "Creir", "toxic",
                100, 130, 6500, 360);
		Misc.initConditionMarket(planet_Creir);
        planet_Creir.getMarket().addCondition(Conditions.TOXIC_ATMOSPHERE);
        planet_Creir.getMarket().addCondition(Conditions.HOT);
        

        PlanetAPI planet_Lydia = system.addPlanet("planet_Lydia", planet_Creir, "Lydia",
                "brdy_planet_lydia", 40, 60, 850, 45); // 0.004 AU
        planet_Lydia.setInteractionImage("illustrations", "vacuum_colony");
        planet_Lydia.setCustomDescriptionId("brdy_planet_lydia_description");

        system.addAsteroidBelt(star_Gneiss, 70, 5600, 128, 440, 470);

        PlanetAPI planet_Verge = system.addPlanet("nanoplanet_verge", star_Gneiss, "Verge",
                "brdy_planet_verge", 230, 340, 9500, 800); //
        planet_Verge.getSpec().setGlowTexture(Global.getSettings().getSpriteName("hab_glows", "banded"));
        planet_Verge.getSpec().setGlowColor(new Color(54, 119, 84, 84));
        planet_Verge.getSpec().setUseReverseLightForGlow(true);
        planet_Verge.applySpecChanges();
        
		Misc.initConditionMarket(planet_Verge);
        planet_Verge.getMarket().addCondition(brdy_Conditions.VERGE_CON);
        planet_Verge.getMarket().addCondition(Conditions.DENSE_ATMOSPHERE);
        planet_Verge.getMarket().addCondition(Conditions.HIGH_GRAVITY);

        system.addRingBand(planet_Verge, "misc", "rings_dust0", 256f, 1,
                Color.white, 256f, 360, 7.9f);
        system.addRingBand(planet_Verge, "misc", "rings_dust0", 256f, 1,
                Color.white, 256f, 370, 9.95f);
        system.addRingBand(planet_Verge, "misc", "rings_dust0", 256f, 1,
                Color.white, 256f, 380, 11.45f);

        system.addAsteroidBelt(planet_Verge, 70, 900, 128, 10, 16);

        SectorEntityToken station_Vigil = system.addCustomEntity("brdy_vigil_station", "Vigil Station",
                "brdy_vigil_station", FACTION_BLACKROCK);
        station_Vigil.setCircularOrbitPointingDown(system.getEntityById("nanoplanet_verge"),
                90, 540, 11);
        station_Vigil.setInteractionImage("illustrations", "brdy_vigil_station");
        station_Vigil.setCustomDescriptionId("brdy_vigil_station_description");

        PlanetAPI brdy_Preclusion = system.addPlanet("brdy_Preclusion", star_Gneiss, "Preclusion",
                "cryovolcanic", 260, 30, 12200, 480);
        brdy_Preclusion.setInteractionImage("illustrations", "abandoned_station");
        brdy_Preclusion.setCustomDescriptionId("brdy_preclusion_description");

        JumpPointAPI jumpPoint_Lodestone = Global.getFactory().createJumpPoint(
                "jumpPoint_Lodestone_Passage", "Lodestone Passage");
        OrbitAPI orbit = Global.getFactory().createCircularOrbit(planet_Blackrock, 90, 550, 25);
        jumpPoint_Lodestone.setOrbit(orbit);
        jumpPoint_Lodestone.setRelatedPlanet(planet_Lodestone);
        jumpPoint_Lodestone.setStandardWormholeToHyperspaceVisual();
        system.addEntity(jumpPoint_Lodestone);

        MarketAPI market_Lodestone = brdy_addMarketplace.addMarketplace(FACTION_BLACKROCK, planet_Blackrock,
                                      new ArrayList<>(Arrays.asList(station_Augustmoon, planet_Lodestone)),
                                      "Blackrock",
                                      6,
                                      new ArrayList<>(Arrays.asList(
                                              "brdy_con_barren",
                                              Conditions.ORE_ABUNDANT,
                                              Conditions.RARE_ORE_RICH,
                                              Conditions.HOT,
                                              Conditions.ORGANICS_TRACE,
                                              Conditions.POPULATION_6,
                                              Conditions.REGIONAL_CAPITAL
                                      )),
                                      new ArrayList<>(Arrays.asList(
                                              Industries.POPULATION,
                                              Industries.LIGHTINDUSTRY,
                                              Industries.MEGAPORT,
                                              brdy_Industries.BRDYDEFHQ,
                                              Industries.HEAVYBATTERIES,
                                              Industries.HIGHCOMMAND,
                                              Industries.STARFORTRESS_MID,
                                              Industries.MINING,
                                              Industries.REFINING
                                      )),
                                      new ArrayList<>(Arrays.asList(
                                              Submarkets.SUBMARKET_STORAGE,
                                              Submarkets.SUBMARKET_BLACK,
                                              Submarkets.GENERIC_MILITARY,
                                              Submarkets.SUBMARKET_OPEN
                                      )),
                                      0.3f
        );
        market_Lodestone.addIndustry(Industries.ORBITALWORKS, new ArrayList<>(Collections.singletonList(Items.PRISTINE_NANOFORGE)));


        brdy_addMarketplace.addMarketplace(FACTION_BLACKROCK, station_Vigil,
                                      new ArrayList<>(Collections.singletonList(station_Vigil)),
                                      "Vigil Station",
                                      4,
                                      new ArrayList<>(Arrays.asList(
                                              Conditions.POPULATION_4,
                                              Conditions.STEALTH_MINEFIELDS,
                                              Conditions.DISSIDENT, Conditions.VICE_DEMAND
                                      )),
                                      new ArrayList<>(Arrays.asList(
                                              Industries.POPULATION,
                                              Industries.PATROLHQ,
                                              //Industries.MILITARYBASE,
                                              Industries.GROUNDDEFENSES,
                                              Industries.SPACEPORT,
                                              Industries.WAYSTATION,
                                              Industries.BATTLESTATION_MID,
                                              Industries.LIGHTINDUSTRY
                                      )),
                                      new ArrayList<>(Arrays.asList(
                                              Submarkets.SUBMARKET_STORAGE,
                                              Submarkets.SUBMARKET_BLACK,
                                              Submarkets.SUBMARKET_OPEN
                                      )),
                                      0.3f
        );

        brdy_addMarketplace.addMarketplace(Factions.INDEPENDENT, planet_Lydia,
                                      null,
                                      "Lydia",
                                      4,
                                      new ArrayList<>(Arrays.asList(
                                              Conditions.NO_ATMOSPHERE,
                                              Conditions.FREE_PORT,
                                              Conditions.FRONTIER,
                                              Conditions.POPULATION_4
                                      )),
                                      new ArrayList<>(Arrays.asList(
                                              Industries.SPACEPORT,
                                              Industries.POPULATION,
                                              Industries.WAYSTATION,
                                              Industries.PATROLHQ,
                                              Industries.FUELPROD,
                                              Industries.LIGHTINDUSTRY
                                      )),
                                      new ArrayList<>(Arrays.asList(
                                              Submarkets.SUBMARKET_STORAGE,
                                              Submarkets.SUBMARKET_BLACK,
                                              Submarkets.SUBMARKET_OPEN
                                      )),
                                      0.3f
        );

        brdy_addMarketplace.addMarketplace(Factions.PIRATES, brdy_Preclusion,
                                      null,
                                      "Preclusion",
                                      4,
                                      new ArrayList<>(Arrays.asList(
                                              Conditions.ICE,
                                              Conditions.FREE_PORT,
                                              Conditions.POPULATION_4
                                      )),
                                      new ArrayList<>(Arrays.asList(
                                              Industries.SPACEPORT,
                                              Industries.POPULATION,
                                              Industries.WAYSTATION,
                                              Industries.PATROLHQ,
                                              Industries.FUELPROD,
                                              Industries.LIGHTINDUSTRY
                                      )),
                                      new ArrayList<>(Arrays.asList(
                                              Submarkets.SUBMARKET_STORAGE,
                                              Submarkets.SUBMARKET_BLACK,
                                              Submarkets.SUBMARKET_OPEN
                                      )),
                                      0.3f
        );

        float radiusAfter = StarSystemGenerator.addOrbitingEntities(
                system,
                star_Gneiss,
                StarAge.AVERAGE,
                2,
                4,
                14200,
                3,
                true
        );

        system.autogenerateHyperspaceJumpPoints(true, true); //begone evil clouds
        HyperspaceTerrainPlugin plugin = (HyperspaceTerrainPlugin) Misc.getHyperspaceTerrain().getPlugin();
        NebulaEditor editor = new NebulaEditor(plugin);
        float minRadius = plugin.getTileSize() * 2f;

        float radius = system.getMaxRadiusInHyperspace();
        editor.clearArc(system.getLocation().x, system.getLocation().y, 0, radius + minRadius,
                0, 360f);
        editor.clearArc(system.getLocation().x, system.getLocation().y, 0, radius + minRadius,
                0, 360f, 0.25f);
    }
}
