package data.scripts.plugins;

import com.fs.starfarer.api.combat.BaseEveryFrameCombatPlugin;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipEngineControllerAPI.ShipEngineAPI;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.input.InputEventAPI;
import org.dark.shaders.light.LightShader;
import org.dark.shaders.light.StandardLight;
import org.lwjgl.util.vector.Vector2f;

import java.awt.*;
import java.util.List;
import java.util.*;

@SuppressWarnings("unused")
public class brdy_LightInjector extends BaseEveryFrameCombatPlugin {

    private static final Color ARCJET_COLOR = new Color(131, 228, 119);
    private static final Color BURSTJETS_COLOR = new Color(140, 250, 200);
    private static final Color DESDINOVAJETS_COLOR = new Color(140, 242, 195);
    private static final Color MORPHEUSJETS_COLOR = new Color(49, 255, 210);
    private static final Vector2f ZERO = new Vector2f();

    private static final Map<CombatEngineAPI, LocalData> dataMap = new WeakHashMap<>();

    private CombatEngineAPI engine;

    @Override
    public void advance(float amount, List<InputEventAPI> events) {
        if (engine == null) {
            return;
        }

        if (engine.isPaused()) {
            return;
        }

        final LocalData localData = dataMap.get(engine);
        final Map<ShipAPI, StandardLight> lights = localData.lights;

        List<ShipAPI> ships = engine.getShips();
        for (ShipAPI ship : ships) {
            if (ship.isHulk()) {
                continue;
            }

            ShipSystemAPI system = ship.getSystem();
            if (system != null) {
                String id = system.getId();
                switch (id) {
                    case "brdy_arcjetburner":
                        if (system.isActive()) {
                            Vector2f location = null;
                            if (ship.getEngineController() == null) {
                                break;
                            }
                            List<ShipEngineAPI> engines = ship.getEngineController().getShipEngines();
                            int num = 0;
                            for (ShipEngineAPI eng : engines) {
                                if (eng.isActive() && !eng.isDisabled()) {
                                    num++;
                                    if (location == null) {
                                        location = new Vector2f(eng.getLocation());
                                    } else {
                                        Vector2f.add(location, eng.getLocation(), location);
                                    }
                                }
                            }
                            if (location == null) {
                                break;
                            }

                            location.scale(1f / num);

                            if (lights.containsKey(ship)) {
                                StandardLight light = lights.get(ship);

                                light.setLocation(location);

                                if (system.isActive() && !system.isOn()) {
                                    if (!light.isFadingOut()) {
                                        light.fadeOut(1.25f);
                                    }
                                }
                            } else {
                                StandardLight light = new StandardLight(location, ZERO, ZERO, null);
                                float intensity = (float) Math.sqrt(ship.getCollisionRadius()) / 10f;
                                float size = intensity * 100f;

                                light.setIntensity(intensity);
                                light.setSize(size);
                                light.setColor(ARCJET_COLOR);
                                light.fadeIn(1.43f);

                                lights.put(ship, light);
                                LightShader.addLight(light);
                            }
                        }
                        break;
                    case "brdy_burstjets":
                        if (system.isActive()) {
                            Vector2f location = null;
                            if (ship.getEngineController() == null) {
                                break;
                            }
                            List<ShipEngineAPI> engines = ship.getEngineController().getShipEngines();
                            int num = 0;
                            for (ShipEngineAPI eng : engines) {
                                if (eng.isActive() && !eng.isDisabled()) {
                                    num++;
                                    if (location == null) {
                                        location = new Vector2f(eng.getLocation());
                                    } else {
                                        Vector2f.add(location, eng.getLocation(), location);
                                    }
                                }
                            }
                            if (location == null) {
                                break;
                            }

                            location.scale(1f / num);

                            if (lights.containsKey(ship)) {
                                StandardLight light = lights.get(ship);

                                light.setLocation(location);

                                if ((system.isActive() && !system.isOn()) || system.isChargedown()) {
                                    if (!light.isFadingOut()) {
                                        light.fadeOut(1.5f);
                                    }
                                }
                            } else {
                                StandardLight light = new StandardLight(location, ZERO, ZERO, null);
                                float intensity = (float) Math.sqrt(ship.getCollisionRadius()) / 40f;
                                float size = intensity * 600f;

                                light.setIntensity(intensity);
                                light.setSize(size);
                                light.setColor(BURSTJETS_COLOR);
                                light.fadeIn(0.05f);

                                lights.put(ship, light);
                                LightShader.addLight(light);
                            }
                        }
                        break;
                    case "brdy_arcjetthrusters":
                        if (system.isActive()) {
                            Vector2f location = null;
                            if (ship.getEngineController() == null) {
                                break;
                            }
                            List<ShipEngineAPI> engines = ship.getEngineController().getShipEngines();
                            int num = 0;
                            for (ShipEngineAPI eng : engines) {
                                if (eng.isActive() && !eng.isDisabled()) {
                                    num++;
                                    if (location == null) {
                                        location = new Vector2f(eng.getLocation());
                                    } else {
                                        Vector2f.add(location, eng.getLocation(), location);
                                    }
                                }
                            }
                            if (location == null) {
                                break;
                            }

                            location.scale(1f / num);

                            if (lights.containsKey(ship)) {
                                StandardLight light = lights.get(ship);

                                light.setLocation(location);

                                if ((system.isActive() && !system.isOn()) || system.isChargedown()) {
                                    if (!light.isFadingOut()) {
                                        light.fadeOut(1.1f);
                                    }
                                }
                            } else {
                                StandardLight light = new StandardLight(location, ZERO, ZERO, null);
                                float intensity = (float) Math.sqrt(ship.getCollisionRadius()) / 40f;
                                float size = intensity * 600f;

                                light.setIntensity(intensity);
                                light.setSize(size);
                                light.setColor(DESDINOVAJETS_COLOR);
                                light.fadeIn(0.05f);

                                lights.put(ship, light);
                                LightShader.addLight(light);
                            }
                        }
                        break;
                    case "brdy_slipjets":
                        if (system.isActive()) {
                            Vector2f location = null;
                            if (ship.getEngineController() == null) {
                                break;
                            }
                            List<ShipEngineAPI> engines = ship.getEngineController().getShipEngines();
                            int num = 0;
                            for (ShipEngineAPI eng : engines) {
                                if (eng.isActive() && !eng.isDisabled()) {
                                    num++;
                                    if (location == null) {
                                        location = new Vector2f(eng.getLocation());
                                    } else {
                                        Vector2f.add(location, eng.getLocation(), location);
                                    }
                                }
                            }
                            if (location == null) {
                                break;
                            }

                            location.scale(1f / num);

                            if (lights.containsKey(ship)) {
                                StandardLight light = lights.get(ship);

                                light.setLocation(location);

                                if ((system.isActive() && !system.isOn()) || system.isChargedown()) {
                                    if (!light.isFadingOut()) {
                                        light.fadeOut(1.1f);
                                    }
                                }
                            } else {
                                StandardLight light = new StandardLight(location, ZERO, ZERO, null);
                                float intensity = (float) Math.sqrt(ship.getCollisionRadius()) / 40f;
                                float size = intensity * 600f;

                                light.setIntensity(intensity);
                                light.setSize(size);
                                light.setColor(MORPHEUSJETS_COLOR);
                                light.fadeIn(0.05f);

                                lights.put(ship, light);
                                LightShader.addLight(light);
                            }
                        }
                        break;
                    case "brdy_brakefieldemitter":
                        if (system.isActive()) {
                            Vector2f location = ship.getLocation();

                            if (lights.containsKey(ship)) {
                                StandardLight light = lights.get(ship);

                                light.setLocation(location);

                                if (system.isActive() && !system.isOn()) {
                                    if (!light.isFadingOut()) {
                                        light.fadeOut(1f);
                                    }
                                }
                            } else {
                                StandardLight light = new StandardLight(location, ZERO, ZERO, null);

                                light.setIntensity(0.35f);
                                light.setSize(1500f);
                                light.setColor(0.45f, 0.8f, 0.65f);
                                light.fadeIn(0.5f);

                                lights.put(ship, light);
                                LightShader.addLight(light);
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        Iterator<Map.Entry<ShipAPI, StandardLight>> iter = lights.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry<ShipAPI, StandardLight> entry = iter.next();
            ShipAPI ship = entry.getKey();

            if ((ship.getSystem() != null && !ship.getSystem().isActive()) || !ship.isAlive()) {
                StandardLight light = entry.getValue();

                light.unattach();
                light.fadeOut(0);
                iter.remove();
            }
        }
    }

    @Override
    public void init(CombatEngineAPI engine) {
        this.engine = engine;
        dataMap.put(engine, new LocalData());
    }

    private static final class LocalData {

        final Map<ShipAPI, StandardLight> lights = new LinkedHashMap<>(100);
    }
}
