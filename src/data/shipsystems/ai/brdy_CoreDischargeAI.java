package data.shipsystems.ai;

import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.util.IntervalUtil;
import data.shipsystems.scripts.brdy_CoreDischargeStats;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lwjgl.util.vector.Vector2f;

import java.util.Iterator;
import java.util.List;

// Dynastos System
public class brdy_CoreDischargeAI implements ShipSystemAIScript {
    private CombatEngineAPI engine;
    private ShipAPI ship;
    private ShipwideAIFlags flags;

    private final IntervalUtil tracker = new IntervalUtil(0.5f, 1f);

    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        if (engine == null) {return;}
        if (engine.isPaused()) {return;}

        tracker.advance(amount);

        if (tracker.intervalElapsed()) {
            if (ship.getFluxTracker().isOverloadedOrVenting() || !AIUtils.canUseSystemThisFrame(ship)) {
                return;
            }

            boolean shouldUseSystem = false;
            float empRadius = brdy_CoreDischargeStats.SYSTEM_RANGE;
            float trueRange = ship.getCollisionRadius() + ship.getMutableStats().getSystemRangeBonus().computeEffective(empRadius);

            List<ShipAPI> nearbyEnemies = AIUtils.getNearbyEnemies(ship, trueRange);
            List<MissileAPI> nearbyMissiles = AIUtils.getNearbyEnemyMissiles(ship, trueRange);

            // Filter out fizzling missiles and Flares
            Iterator<MissileAPI> missileIter = nearbyMissiles.iterator();
            while (missileIter.hasNext()) {
                MissileAPI nearbyMissile = missileIter.next();
                if (nearbyMissile.isFizzling() || nearbyMissile.isFlare()) {
                    missileIter.remove();
                }
            }

            int minMissiles = 9;
            int minEnemies = 4;

            float dangerMultiplier = Math.round((0.5f/ship.getHullLevel()) + (2f*ship.getFluxLevel()));
            float dangerLimit = 2.7f;

            if (ship.getShipTarget() != null && MathUtils.getDistance(ship, ship.getShipTarget()) <= trueRange-150) {
                if (dangerMultiplier <= 1.85f
                        || ship.getShipTarget().getHullLevel() <= 0.25f
                        || ship.getShipTarget().getHardFluxLevel() >= 0.75f)
                {
                    flags.setFlag(ShipwideAIFlags.AIFlags.DO_NOT_BACK_OFF, 2f);
                    shouldUseSystem = true;
                }
            }

            if (!shouldUseSystem && (nearbyMissiles.size() >= minMissiles/dangerMultiplier || nearbyEnemies.size() >= minEnemies/dangerMultiplier)) {
                shouldUseSystem = true;
            }
            if (!shouldUseSystem && (dangerMultiplier >= dangerLimit && (nearbyMissiles.size() > 0 || nearbyEnemies.size() > 0))) {
                shouldUseSystem = true;
            }

            if (ship.getSystem().isActive() ^ shouldUseSystem) {
                ship.useSystem();
            }
        }
    }

    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this.ship = ship;
        this.engine = engine;
        this.flags = flags;
    }
}
