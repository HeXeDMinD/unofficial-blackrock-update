package data.shipsystems.ai;

import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.util.IntervalUtil;
import data.scripts.util.brdy_Utils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lwjgl.util.vector.Vector2f;

// Dragonfly System
public class brdy_BrakefieldBarrierAI implements ShipSystemAIScript {
    private CombatEngineAPI engine;
    private ShipAPI ship;
    private ShipSystemAPI system;
    private ShipwideAIFlags flags;

    private final IntervalUtil tracker = new IntervalUtil(0.35f, 0.6f);

    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        if (engine == null) {return;}
        if (engine.isPaused()) {return;}

        tracker.advance(amount);

        if (tracker.intervalElapsed()) {
            if (system.isActive() && brdy_Utils.estimateIncomingBeamDamage(ship, 2f) < 100) {
                if (ship.getShield().isOn()) {
                    ship.giveCommand(ShipCommand.TOGGLE_SHIELD_OR_PHASE_CLOAK, new Vector2f(), 0);
                }
                flags.setFlag(ShipwideAIFlags.AIFlags.DO_NOT_USE_SHIELDS, 0.1f);
            }

            if (ship.getFluxTracker().isOverloadedOrVenting() || !AIUtils.canUseSystemThisFrame(ship)) {
                return;
            }


            boolean shouldUseSystem = false;

            float dangerMultiplier = Math.round((0.5f/ship.getHullLevel()) + (2f*ship.getFluxLevel()));
            int minimum_damage = 350;
            float damage_reduction = 35 * dangerMultiplier;

            float estimated_damage = brdy_Utils.estimateIncomingProjectileDamage(ship, 1f, false) + brdy_Utils.estimateIncomingMissileDamage(ship);
            if (estimated_damage > minimum_damage - damage_reduction) {
                if ((ship.getHullLevel() >= 0.25f && ship.getHardFluxLevel() < 0.33f)
                        || (ship.getShipTarget() != null && (ship.getShipTarget().getHardFluxLevel() > 0.75f
                        || ship.getShipTarget().getHullLevel() <= 0.25f)))
                {
                    float estimated_damage_including_beams = brdy_Utils.estimateAllIncomingDamage(ship, 1f, false);
                    if (estimated_damage_including_beams < 500) {
                        flags.setFlag(ShipwideAIFlags.AIFlags.DO_NOT_BACK_OFF, 4.25f);
                    }
                }
                shouldUseSystem = true;
            }

            if (system.isActive() ^ shouldUseSystem) {
                ship.useSystem();
            }
        }
    }

    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this.ship = ship;
        this.engine = engine;
        this.system = system;
        this.flags = flags;
    }
}
