package data.shipsystems.ai;

import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.util.IntervalUtil;
import data.scripts.util.brdy_Utils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

import java.util.Iterator;
import java.util.List;

// Kurmaraja System
public class brdy_BrakefieldEmitterAI implements ShipSystemAIScript {
    private CombatEngineAPI engine;
    private ShipAPI ship;
    private ShipSystemAPI system;

    private final IntervalUtil tracker = new IntervalUtil(0.35f, 0.6f);

    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        if (engine == null) {return;}
        if (engine.isPaused()) {return;}

        tracker.advance(amount);

        if (tracker.intervalElapsed()) {
            if (ship.getFluxTracker().isOverloadedOrVenting() || !AIUtils.canUseSystemThisFrame(ship)) {
                return;
            }

            if (brdy_Utils.estimateIncomingBeamDamage(ship, 3f) > 300) {
                return;
            }

            Vector2f shipLoc = ship.getLocation();
            boolean shouldUseSystem = false;
            float repulseRadius = 1250f;//brdy_BrakefieldEmitterStats.FIELD_RANGE
            float trueRange = ship.getCollisionRadius() + ship.getMutableStats().getSystemRangeBonus().computeEffective(repulseRadius);

            List<ShipAPI> nearbyAllies = AIUtils.getNearbyAllies(ship, trueRange);
            List<ShipAPI> nearbyEnemies = AIUtils.getNearbyEnemies(ship, trueRange);
            List<MissileAPI> nearbyMissiles = AIUtils.getNearbyEnemyMissiles(ship, trueRange);
            List<DamagingProjectileAPI> nearbyBullets = CombatUtils.getProjectilesWithinRange(shipLoc, trueRange);

            // Filter out fighters
            Iterator<ShipAPI> enemyIter = nearbyEnemies.iterator();
            while (enemyIter.hasNext()) {
                ShipAPI nearbyEnemy = enemyIter.next();
                if (nearbyEnemy.getHullSize() == ShipAPI.HullSize.FIGHTER) {
                    enemyIter.remove();
                }
            }

            // Filter out fizzling missiles and Flares
            Iterator<MissileAPI> missileIter = nearbyMissiles.iterator();
            while (missileIter.hasNext()) {
                MissileAPI nearbyMissile = missileIter.next();
                if (nearbyMissile.isFizzling() || nearbyMissile.isFlare()) {
                    missileIter.remove();
                }
            }

            // Filter to just enemy bullets
            Iterator<DamagingProjectileAPI> bulletIter = nearbyBullets.iterator();
            while (bulletIter.hasNext()) {
                DamagingProjectileAPI nearbyBullet = bulletIter.next();
                if (nearbyBullet.getOwner() == 100 || nearbyBullet.getOwner() == 0) {
                    bulletIter.remove();
                }
            }

            float averageAlly_Flux = 0f;
            float averageAlly_Hull = 0f;

            // Filter out fighters and find average hull and flux levels of nearby allies
            Iterator<ShipAPI> allyIter = nearbyAllies.iterator();
            while (allyIter.hasNext()) {
                ShipAPI nearbyAlly = allyIter.next();
                if (nearbyAlly.getHullSize() == ShipAPI.HullSize.FIGHTER) {
                    allyIter.remove();
                }
                averageAlly_Flux = averageAlly_Flux + nearbyAlly.getFluxLevel();
                averageAlly_Hull = averageAlly_Hull + nearbyAlly.getHullLevel();
            }

            int minMissiles = 15;
            int minEnemies = 9;
            int minBullets = 45;

            averageAlly_Flux = averageAlly_Flux/nearbyAllies.size();
            averageAlly_Hull = averageAlly_Hull/nearbyAllies.size();

            float dangerMultiplier = Math.round((0.5f/ship.getHullLevel()) + (2f*ship.getFluxLevel()));
            float dangerLimit = 2.7f;
            float dangerMultiplierAllies = Math.round((0.5f/averageAlly_Hull) + (2f*averageAlly_Flux));
            float dangerLimitAllies = 2.2f;

            if (nearbyMissiles.size() >= minMissiles/dangerMultiplier
                    || nearbyEnemies.size() >= minEnemies/dangerMultiplier
                    || nearbyBullets.size() >= minBullets/dangerMultiplier)
            {
                shouldUseSystem = true;
            }
            if (!shouldUseSystem && (nearbyMissiles.size() >= minMissiles / dangerMultiplierAllies
                    || nearbyEnemies.size() >= minEnemies / dangerMultiplierAllies
                    || nearbyBullets.size() >= minBullets / dangerMultiplierAllies))
            {
                shouldUseSystem = true;
            }
            if (!shouldUseSystem && ((dangerMultiplier >= dangerLimit || dangerMultiplierAllies >= dangerLimitAllies)
                    && (nearbyMissiles.size() > 0 || nearbyEnemies.size() > 0 || nearbyBullets.size() > 0)))
            {
                shouldUseSystem = true;
            }

            if (system.isActive() ^ shouldUseSystem) {
                ship.useSystem();
            }
        }
    }

    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this.ship = ship;
        this.engine = engine;
        this.system = system;
    }
}
