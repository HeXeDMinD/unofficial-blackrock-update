package data.shipsystems.scripts;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import com.fs.starfarer.api.loading.WeaponSlotAPI;
import java.awt.Color;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lwjgl.util.vector.Vector2f;
// Asura System
public class brdy_ReactorOverdriveStats extends BaseShipSystemScript {
    public static final float ENERGYFIRERATE_BONUS = 0.3f;
    public static final float ENERGYFLUXCOST_REDUCTION = 0.3f;
    public static final float FLUXDISSIPATION_MULTIPLIER = 3f;
    public static final String FLUXEJECTOR_WEAPONID = "brdy_fluxejector";
    private static final Color SMOKE_COLOR = new Color(101, 168, 117, 117);
    private static final String SOUND_ID = "brdy_system_ReactorOverdrive_Eject";
    private boolean armed = false;

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        if (state == State.ACTIVE) {
            stats.getEnergyRoFMult().modifyMult(id, 1f + ENERGYFIRERATE_BONUS * effectLevel);
            stats.getEnergyWeaponFluxCostMod().modifyMult(id, 1f - ENERGYFLUXCOST_REDUCTION * effectLevel);
            stats.getMaxSpeed().modifyFlat(id, 50f * effectLevel);
            stats.getAcceleration().modifyFlat(id, 50f * effectLevel);
            stats.getDeceleration().modifyFlat(id, 20f * effectLevel);
            stats.getMaxTurnRate().modifyMult(id, 1f + 0.25f * effectLevel);

            armed = true;
        } else if (state == State.OUT) {
            stats.getEnergyRoFMult().unmodify(id);
            stats.getEnergyWeaponFluxCostMod().unmodify(id);
            stats.getFluxDissipation().modifyMult(id, 1f + FLUXDISSIPATION_MULTIPLIER * effectLevel);
            stats.getMaxSpeed().unmodify(id);
            stats.getMaxTurnRate().unmodify(id);
            stats.getAcceleration().unmodify(id);
            stats.getDeceleration().unmodify(id);

            ShipAPI ship = (ShipAPI) stats.getEntity();
            if (ship != null) {
                if (armed) {
                    armed = false;
                    for (WeaponSlotAPI slot : ship.getHullSpec().getAllWeaponSlotsCopy()) {
                        if (slot.isSystemSlot()) {
                            float angle = slot.getAngle() + ship.getFacing();
                            if (angle >= 360f) {
                                angle -= 360f;
                            }
                            Vector2f slotLoc = slot.computePosition(ship);
                            Global.getCombatEngine().spawnProjectile(ship, null, FLUXEJECTOR_WEAPONID, slotLoc, angle, ship.getVelocity());
                            for (int i = 0; i < 8; i++) {
                                Vector2f loc = MathUtils.getRandomPointInCircle(slotLoc, 8f);
                                float size = MathUtils.getRandomNumberInRange(10f, 19f);
                                Global.getCombatEngine().addSmokeParticle(loc, ship.getVelocity(), size, 1f, 0.9f, SMOKE_COLOR);
                            }
                            for (int i = 0; i < 6; i++) {
                                Vector2f vel = new Vector2f(MathUtils.getRandomNumberInRange(0f, 30f), 0f);
                                VectorUtils.rotate(vel, angle, vel);
                                Vector2f.add(vel, ship.getVelocity(), vel);
                                float size = MathUtils.getRandomNumberInRange(10f, 19f);
                                Global.getCombatEngine().addSmokeParticle(slotLoc, vel, size, 1f, 1.1f, SMOKE_COLOR);
                            }
                        }
                    }
                    Global.getSoundPlayer().playSound(SOUND_ID, 1f, 1f, ship.getLocation(), ship.getVelocity());
                }
            }
        }
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        stats.getEnergyRoFMult().unmodify(id);
        stats.getFluxDissipation().unmodify(id);
        stats.getMaxSpeed().unmodify(id);
        stats.getMaxTurnRate().unmodify(id);
        stats.getAcceleration().unmodify(id);
        stats.getDeceleration().unmodify(id);

        armed = false;
    }

    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {
        if (index == 0) {
            return new StatusData("Increased energy weapon output and engine power", false);
        }
        return null;
    }
}
