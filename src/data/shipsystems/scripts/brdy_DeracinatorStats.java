package data.shipsystems.scripts;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;
import data.scripts.util.brdy_Utils;
import org.dark.shaders.distortion.DistortionShader;
import org.dark.shaders.distortion.WaveDistortion;
import org.dark.shaders.light.LightShader;
import org.dark.shaders.light.StandardLight;
import org.lazywizard.lazylib.FastTrig;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

import java.awt.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
// Karkinos System 1600 range
public class brdy_DeracinatorStats extends BaseShipSystemScript
{
    private static final Map<ShipAPI.HullSize, Float> DAMAGE_MOD = new HashMap<>(5);
    static {
        DAMAGE_MOD.put(ShipAPI.HullSize.FIGHTER, 0.7f);
        DAMAGE_MOD.put(ShipAPI.HullSize.FRIGATE, 0.8f);
        DAMAGE_MOD.put(ShipAPI.HullSize.DESTROYER, 0.5f);
        DAMAGE_MOD.put(ShipAPI.HullSize.CRUISER, 0.20f);
        DAMAGE_MOD.put(ShipAPI.HullSize.CAPITAL_SHIP, 0.15f);
    }

    private static final float DISTORTION_BLAST_RADIUS = 1200.0f;
    private static final float DISTORTION_CHARGE_RADIUS = 100.0f;

    private static final Color EXPLOSION_COLOR;
    private static final float EXPLOSION_DAMAGE_AMOUNT = 1350.0f;
    private static final DamageType EXPLOSION_DAMAGE_TYPE;
    private static final float EXPLOSION_DAMAGE_VS_ALLIES_MODIFIER = 0.11f;
    private static final float EXPLOSION_EMP_DAMAGE_AMOUNT = 2000.0f;
    private static final float EXPLOSION_EMP_VS_ALLIES_MODIFIER = 0.05f;
    private static final float EXPLOSION_FORCE_VS_ALLIES_MODIFIER = 0.3f;
    private static final float EXPLOSION_PUSH_RADIUS = 800.0f;
    private static final float EXPLOSION_VISUAL_RADIUS = 1250.0f;
    private static final Color FLARE_COLOR;

    private static final Map<ShipAPI.HullSize, Float> FORCE_MOD = new HashMap<>(5);
    static {
        FORCE_MOD.put(ShipAPI.HullSize.FIGHTER, 255.0f);
        FORCE_MOD.put(ShipAPI.HullSize.FRIGATE, 180.0f);
        FORCE_MOD.put(ShipAPI.HullSize.DESTROYER, 115.0f);
        FORCE_MOD.put(ShipAPI.HullSize.CRUISER, 60.0f);
        FORCE_MOD.put(ShipAPI.HullSize.CAPITAL_SHIP, 35.0f);
    }
    private static final float FORCE_VS_ASTEROID = 290.0f;

    private static final Color PARTICLE_COLOR;
    private static final int MAX_PARTICLES_PER_FRAME = 30;
    private static final float PARTICLE_OPACITY = 0.85f;
    private static final float PARTICLE_RADIUS = 300.0f;
    private static final float PARTICLE_SIZE = 6.0f;
    private static final Vector2f ZERO;
    private boolean isActive;
    private StandardLight light;
    private WaveDistortion wave;

    private static final String SOUND_CHARGEUP = "brdy_system_Deracinator_Charge";
    private static final String SOUND_EXPLOSION = "brdy_system_Deracinator_Explosion";

    public brdy_DeracinatorStats() {
        this.isActive = false;
    }

    static {
        EXPLOSION_COLOR = new Color(55, 160, 88);
        EXPLOSION_DAMAGE_TYPE = DamageType.ENERGY;
        FLARE_COLOR = new Color(55, 242, 221);
        PARTICLE_COLOR = new Color(155, 240, 200);
        ZERO = new Vector2f();
    }

    public void apply(final MutableShipStatsAPI stats, final String id, final ShipSystemStatsScript.State state, final float effectLevel) {
        if (!(stats.getEntity() instanceof ShipAPI)) {
            return;
        }

        final ShipAPI ship = (ShipAPI)stats.getEntity();
        float ADJUSTEDEXPLOSION_RANGE = ship.getMutableStats().getSystemRangeBonus().computeEffective(EXPLOSION_PUSH_RADIUS);

        if (state == ShipSystemStatsScript.State.IN) {
            final Vector2f vector2f;
            final Vector2f loc = vector2f = new Vector2f(ship.getLocation());
            vector2f.x -= (float)(70.0 * FastTrig.cos(ship.getFacing() * 3.141592653589793 / 180.0));
            loc.y -= (float)(70.0 * FastTrig.sin(ship.getFacing() * 3.141592653589793 / 180.0));
            if (!this.isActive) {
                this.isActive = true;
                Global.getSoundPlayer().playSound(SOUND_CHARGEUP, 1.0f, 1.0f, ship.getLocation(), ship.getVelocity());
                (this.light = new StandardLight(loc, ZERO, ZERO, null)).setIntensity(1.25f);
                this.light.setSize(EXPLOSION_VISUAL_RADIUS);
                this.light.setColor(PARTICLE_COLOR);
                this.light.fadeIn(1.95f);
                this.light.setLifetime(0.1f);
                this.light.setAutoFadeOutTime(0.17f);
                LightShader.addLight(this.light);
                (this.wave = new WaveDistortion(loc, ZERO)).setSize(DISTORTION_CHARGE_RADIUS);
                this.wave.setIntensity(25.0f);
                this.wave.fadeInSize(1.95f);
                this.wave.fadeInIntensity(1.95f);
                this.wave.setLifetime(0.0f);
                this.wave.setAutoFadeSizeTime(-0.5f);
                this.wave.setAutoFadeIntensityTime(0.17f);
                DistortionShader.addDistortion(this.wave);
            }
            else {
                this.light.setLocation(loc);
                this.wave.setLocation(loc);
            }
            for (int numParticlesThisFrame = Math.round(effectLevel * MAX_PARTICLES_PER_FRAME), x = 0; x < numParticlesThisFrame; ++x) {
                final Vector2f particlePos = MathUtils.getRandomPointOnCircumference(ship.getLocation(), PARTICLE_RADIUS);
                final Vector2f particleVel = Vector2f.sub(ship.getLocation(), particlePos, null);
                Global.getCombatEngine().addSmokeParticle(particlePos, particleVel, PARTICLE_SIZE, PARTICLE_OPACITY, 1.0f, PARTICLE_COLOR);
            }
        }
        else if (state == ShipSystemStatsScript.State.OUT && this.isActive) {
            final CombatEngineAPI engine = Global.getCombatEngine();
            engine.spawnExplosion(ship.getLocation(), ship.getVelocity(), EXPLOSION_COLOR, EXPLOSION_VISUAL_RADIUS, 0.2f);
            engine.spawnExplosion(ship.getLocation(), ship.getVelocity(), EXPLOSION_COLOR, EXPLOSION_VISUAL_RADIUS/2, 0.2f);
            final Vector2f vector2f3;
            final Vector2f loc2 = vector2f3 = new Vector2f(ship.getLocation());
            vector2f3.x -= (float)(70.0 * FastTrig.cos(ship.getFacing() * 3.141592653589793 / 180.0));
            loc2.y -= (float)(70.0 * FastTrig.sin(ship.getFacing() * 3.141592653589793 / 180.0));
            (this.light = new StandardLight()).setLocation(loc2);
            this.light.setIntensity(2.0f);
            this.light.setSize(2500.0f);
            this.light.setColor(EXPLOSION_COLOR);
            this.light.fadeOut(1.25f);
            LightShader.addLight(this.light);
            (this.wave = new WaveDistortion()).setLocation(loc2);
            this.wave.setSize(DISTORTION_BLAST_RADIUS);
            this.wave.setIntensity(90.0f);
            this.wave.fadeInSize(1.2f);
            this.wave.fadeOutIntensity(0.9f);
            this.wave.setSize(300.0f);
            DistortionShader.addDistortion(this.wave);
            Global.getSoundPlayer().playSound(SOUND_EXPLOSION, 1.0f, 1.0f, ship.getLocation(), ship.getVelocity());

            brdy_Utils.createFlare(ship, new Vector2f(loc2), engine, 0.5f, 0.05f,
                    -15.0f + (float)Math.random() * 30.0f, 9.25f, 6.0f,
                    FLARE_COLOR, PARTICLE_COLOR);
            brdy_Utils.createFlare(ship, new Vector2f(loc2), engine, 0.51f, 0.049f,
                    -15.0f + (float)Math.random() * 60.0f, 8.95f, 6.0f,
                    PARTICLE_COLOR, FLARE_COLOR);
            brdy_Utils.createFlare(ship, new Vector2f(loc2), engine, 0.52f, 0.048f,
                    -15.0f + (float)Math.random() * 30.0f, 7.55f, 6.0f,
                    FLARE_COLOR, PARTICLE_COLOR);
            brdy_Utils.createFlare(ship, new Vector2f(loc2), engine, 0.51f, 0.047f,
                    -15.0f + (float)Math.random() * 90.0f, 10.95f, 6.0f,
                    PARTICLE_COLOR, FLARE_COLOR);
            brdy_Utils.createFlare(ship, new Vector2f(loc2), engine, 0.5f, 0.046f,
                    -15.0f + (float)Math.random() * 120.0f, 8.55f, 6.0f,
                    FLARE_COLOR, PARTICLE_COLOR);

            final List<CombatEntityAPI> entities = CombatUtils.getEntitiesWithinRange(ship.getLocation(), ADJUSTEDEXPLOSION_RANGE);
            for (final CombatEntityAPI tmp : entities) {
                if (tmp == ship) {
                    continue;
                }
                final float mod = 1.0f - MathUtils.getDistance(ship, tmp) / ADJUSTEDEXPLOSION_RANGE;
                float force = FORCE_VS_ASTEROID * mod;
                float damage = EXPLOSION_DAMAGE_AMOUNT * mod;
                float emp = EXPLOSION_EMP_DAMAGE_AMOUNT * mod;
                if (tmp instanceof ShipAPI) {
                    final ShipAPI victim = (ShipAPI)tmp;
                    damage /= DAMAGE_MOD.get(victim.getHullSize());
                    force = FORCE_MOD.get(victim.getHullSize()) * mod;

                    if (victim.getOwner() == ship.getOwner()) {
                        damage *= EXPLOSION_DAMAGE_VS_ALLIES_MODIFIER;
                        emp *= EXPLOSION_EMP_VS_ALLIES_MODIFIER;
                        force *= EXPLOSION_FORCE_VS_ALLIES_MODIFIER;
                    }

                    if (victim.getShield() != null && victim.getShield().isOn() && victim.getShield().isWithinArc(ship.getLocation())) {
                        victim.getFluxTracker().increaseFlux(damage * 2.0f, true);
                    }
                    else {
                        for (int x2 = 0; x2 < 5; ++x2) {
                            engine.spawnEmpArc(ship, MathUtils.getRandomPointInCircle(victim.getLocation(),
                                    victim.getCollisionRadius()), victim, victim, EXPLOSION_DAMAGE_TYPE,
                                    damage / 10.0f, emp / 5.0f, 800.0f,
                                    null, 2.0f, EXPLOSION_COLOR, EXPLOSION_COLOR);
                        }
                    }
                }
                final Vector2f dir = VectorUtils.getDirectionalVector(ship.getLocation(), tmp.getLocation());
                dir.scale(force);
                Vector2f.add(tmp.getVelocity(), dir, tmp.getVelocity());
            }
            this.isActive = false;
        }
    }

    public ShipSystemStatsScript.StatusData getStatusData(final int index, final ShipSystemStatsScript.State state, final float effectLevel) {
        if (state == ShipSystemStatsScript.State.IN && index == 0) {
            return new ShipSystemStatsScript.StatusData("charging scalar deracinator", false);
        }
        return null;
    }

    public void unapply(final MutableShipStatsAPI stats, final String id) {
    }
}