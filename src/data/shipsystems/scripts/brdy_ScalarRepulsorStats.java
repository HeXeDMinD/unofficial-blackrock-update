package data.shipsystems.scripts;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import data.scripts.util.brdy_Utils;
import org.dark.shaders.distortion.DistortionShader;
import org.dark.shaders.distortion.RippleDistortion;
import org.dark.shaders.distortion.WaveDistortion;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

import java.awt.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class brdy_ScalarRepulsorStats extends BaseShipSystemScript {
    // Distortion effect constants
    private WaveDistortion wave = null;
    private static final Vector2f ZERO = new Vector2f();
    private static final float DISTORTION_AMOUNT = 25f;
    private static final float DISTORTION_SIZE = 300f;
    // Pulling effect constants
    private static final float ANGLE_FORCE_MULTIPLIER = 1f;
    private static final float VELOCITY_FORCE_MULTIPLIER = 2000f;

    // Absorbsion effect constants
    public static float absorbedPower = 0;
    private static final float POWER_PER_FRIENDLY_DAMAGE_ABSORBED = 0.0004f;
    private static final float POWER_PER_HOSTILE_DAMAGE_ABSORBED = 0.0006f;
    private static final Map<DamageType, Float> DAMAGE_TYPE_POWER_ABSORBTION_MULTIPLIERS = new HashMap<>(5);
    static {
        DAMAGE_TYPE_POWER_ABSORBTION_MULTIPLIERS.put(DamageType.ENERGY, 1.05f);
        DAMAGE_TYPE_POWER_ABSORBTION_MULTIPLIERS.put(DamageType.FRAGMENTATION, 0.4f);
        DAMAGE_TYPE_POWER_ABSORBTION_MULTIPLIERS.put(DamageType.HIGH_EXPLOSIVE, 1.0f);
        DAMAGE_TYPE_POWER_ABSORBTION_MULTIPLIERS.put(DamageType.KINETIC, 0.90f);
        DAMAGE_TYPE_POWER_ABSORBTION_MULTIPLIERS.put(DamageType.OTHER, 0.5f);
    }
    private static final float SPARK_BRIGHTNESS = 1.8f;
    private static final Color SPARK_COLOR = new Color(59, 242, 152);
    private static final float SPARK_DURATION = 0.4f;
    private static final float SPARK_RADIUS = 10f;
    private static final float TEXT_AMOUNT_MULTIPLIER = 1000.0f;
    private static final Color TEXT_COLOR = new Color(60, 255, 245);
    private static final String SOUND_ABSORB = "brdy_system_ScalarCloak_Absorb";

    // Explosion effect constants
    private static final Color EXPLOSION_COLOR = new Color(160, 255, 114, 50);
    private static final float EXPLOSION_PULSES_PER_POWER = 5.5f; // 5.5f
    private static final float EXPLOSION_RADIUS = 700f;
    private static final float EXPLOSION_VISUAL_RADIUS = 450f;
    private static final float MAX_EXPLOSION_RADIUS = 3000.0f;
    private static final String SOUND_EXPLOSION = "brdy_system_ScalarRepulsor_Explode";

    // Explosion scaling constants
    private static final float MIN_POWER_MULTIPLIER = 0.5f;
    private static final float MAX_POWER_MULTIPLIER = 15.0f;
    public static final float MAX_RANGE_MULTIPLIER = 10f;



    private CombatEngineAPI engine;
    private ShipAPI ship;

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        if (!(stats.getEntity() instanceof ShipAPI)) {
            return;
        }

        stats.getAcceleration().modifyPercent(id, 100f * effectLevel);
        stats.getTurnAcceleration().modifyPercent(id, 200f * effectLevel);
        stats.getMaxSpeed().modifyPercent(id, 50f * effectLevel);
        stats.getMaxTurnRate().modifyPercent(id, 100f * effectLevel);

        ship = (ShipAPI) stats.getEntity();
        engine = Global.getCombatEngine();

        if (wave == null) {
            wave = new WaveDistortion(ship.getLocation(), ZERO);

            wave.setIntensity(DISTORTION_AMOUNT);
            wave.setSize(DISTORTION_SIZE); //wave.setSize(ship.getCollisionRadius() * MAX_RANGE_MULTIPLIER / 2f);
            wave.setArc(ship.getFacing() - 65f, ship.getFacing() + 65f);
            wave.setArcAttenuationWidth(10f);
            wave.flip(true);
            wave.fadeInIntensity(0.1f);

            DistortionShader.addDistortion(wave);
        } else {
            wave.setLocation(ship.getLocation());
            wave.setArc(ship.getFacing() - 65f, ship.getFacing() + 65f);
        }

        float ADJUSTEDSYSTEM_RANGE = ship.getMutableStats().getSystemRangeBonus().computeEffective(MAX_RANGE_MULTIPLIER);
        List<CombatEntityAPI> entities = CombatUtils.getEntitiesWithinRange(ship.getLocation(),
                ship.getCollisionRadius() * ADJUSTEDSYSTEM_RANGE);
        for (CombatEntityAPI entity : entities) {
            if (!(entity instanceof DamagingProjectileAPI)) {
                continue;
            }

            DamagingProjectileAPI proj = (DamagingProjectileAPI) entity;

            float angleDiff = Math.abs(MathUtils.getShortestRotation(ship.getFacing(), VectorUtils.getAngle(
                    ship.getLocation(), proj.getLocation())));

            if (state != State.OUT && MathUtils.getDistance(ship, proj) <= ship.getCollisionRadius() && angleDiff <= 65f) {
                absorbProjectile(proj, effectLevel);
                continue;
            }

            if (state != State.OUT) {
                pullProjectile(proj, state, effectLevel);
            }
        }

        if (state == State.OUT) {
            if (wave != null) {
                wave.fadeOutIntensity(0.3f);
                wave = null;
            }
            ship.setPhased(false);
        } else {
            ship.setPhased(true);
        }

        if (state == State.OUT && absorbedPower > 0) {
            releaseExplosion();
            absorbedPower = 0;
        }
    }

    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {
        if (index == 0) {
            return new StatusData("increased speed and maneuverability", false);
        } else {
            return null;
        }
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        ship = (ShipAPI) stats.getEntity();
        ship.setPhased(false);
    }

    private void pullProjectile(DamagingProjectileAPI proj, State state, float effectLevel) {
        if (ship == null) {
            return;
        }

        float fromToAngle = VectorUtils.getAngle(ship.getLocation(), proj.getLocation());
        float angleDif = MathUtils.getShortestRotation(fromToAngle, MathUtils.clampAngle(proj.getFacing() + 180));
        float amount = Global.getCombatEngine().getElapsedInLastFrame();
        float distance = MathUtils.getDistance(ship.getLocation(), proj.getLocation());
        float force = (ship.getCollisionRadius() / distance) * effectLevel * ANGLE_FORCE_MULTIPLIER;
        float dAngle = angleDif * amount * force;

        if (proj instanceof MissileAPI && proj.getOwner() != ship.getOwner()) {
            ((MissileAPI) proj).flameOut();
        }

        fromToAngle *= Math.PI / 180;
        Vector2f speedUp = new Vector2f(
                (float) Math.cos(fromToAngle) * amount,
                (float) Math.sin(fromToAngle) * amount);
        speedUp.scale(VELOCITY_FORCE_MULTIPLIER);

        if (state != State.OUT) {
            dAngle = -dAngle;
            speedUp.scale(-1);
        }

        Vector2f.add(proj.getVelocity(), speedUp, proj.getVelocity());
        VectorUtils.rotate(proj.getVelocity(), dAngle, proj.getVelocity());
        proj.setFacing(MathUtils.clampAngle(proj.getFacing() + dAngle * (float) (180 / Math.PI)));
    }

    private void absorbProjectile(DamagingProjectileAPI proj, float effectLevel) {
        if (ship == null || engine == null) { return; }
        float powerAbsorbed = proj.getDamageAmount();
        powerAbsorbed *= (proj.getOwner() == ship.getOwner()) ? POWER_PER_FRIENDLY_DAMAGE_ABSORBED :
                POWER_PER_HOSTILE_DAMAGE_ABSORBED;
        powerAbsorbed *= DAMAGE_TYPE_POWER_ABSORBTION_MULTIPLIERS.get(proj.getDamageType());

        engine.addFloatingDamageText(ship.getLocation(), (powerAbsorbed*effectLevel) * TEXT_AMOUNT_MULTIPLIER,
                TEXT_COLOR, ship, proj);

        absorbedPower += powerAbsorbed*effectLevel;

        float sparkAngle = VectorUtils.getAngle(proj.getLocation(), ship.getLocation());
        sparkAngle *= Math.PI / 180f;
        Vector2f sparkVect = new Vector2f((float) Math.cos(sparkAngle), (float) Math.sin(sparkAngle));
        float distance = MathUtils.getDistance(proj, ship);
        float visualEffect = (float) Math.sqrt(powerAbsorbed * 1000);

        sparkVect.scale(3 * distance / SPARK_DURATION);

        Global.getSoundPlayer().playSound(SOUND_ABSORB, 1, visualEffect, proj.getLocation(), sparkVect);

        engine.addHitParticle(proj.getLocation(), sparkVect, SPARK_RADIUS * visualEffect + SPARK_RADIUS,
                SPARK_BRIGHTNESS, SPARK_DURATION, SPARK_COLOR);
        engine.removeEntity(proj);
    }

    private void releaseExplosion() {
        if (ship == null || engine == null) {
            return;
        }

        float power = absorbedPower;
        power = Math.max(power, MIN_POWER_MULTIPLIER);
        power = Math.min(power, MAX_POWER_MULTIPLIER);

        engine.spawnExplosion(ship.getLocation(), ship.getVelocity(), EXPLOSION_COLOR, EXPLOSION_VISUAL_RADIUS *
                (float) Math.sqrt(power), 0.21f *
                (float) Math.sqrt(power));
        engine.spawnExplosion(ship.getLocation(), ship.getVelocity(), EXPLOSION_COLOR, EXPLOSION_VISUAL_RADIUS *
                (float) Math.sqrt(power) / 2f, 0.19f *
                (float) Math.sqrt(power));

        Global.getSoundPlayer().playSound(SOUND_EXPLOSION, 1.0f / (float) Math.pow(power, 0.25), power / 2f,
                ship.getLocation(), ship.getVelocity());

        float ADJUSTEDEXPLOSION_RANGE = ship.getMutableStats().getSystemRangeBonus().computeEffective(MAX_EXPLOSION_RADIUS);

        float explosionRadius = Math.min(ADJUSTEDEXPLOSION_RANGE, EXPLOSION_RADIUS * power);

        RippleDistortion ripple = new RippleDistortion(ship.getLocation(), ZERO);

        ripple.setIntensity(DISTORTION_AMOUNT * 5f * (float) Math.sqrt(power));
        ripple.setSize(explosionRadius * 1.5f);
        ripple.setArc(ship.getFacing() + 180f - 65f, ship.getFacing() + 180f + 65f);
        ripple.setArcAttenuationWidth(10f);
        ripple.fadeInSize(0.35f);
        ripple.setFrameRate(RippleDistortion.FRAMES / 0.35f);
        ripple.fadeOutIntensity(0.35f);

        DistortionShader.addDistortion(ripple);

        WeaponAPI weapon = null;
        for (WeaponAPI wep : ship.getAllWeapons()) {
            if (wep.getId().contentEquals("brdy_arclightlfo")) {
                weapon = wep;
                break;
            }
        }

        List<DamagingProjectileAPI> projectiles = CombatUtils.getProjectilesWithinRange(ship.getLocation(),
                explosionRadius);
        int size = projectiles.size();
        for (int i = 0; i < size; i++) {
            DamagingProjectileAPI proj = projectiles.get(i);

            float fromToAngle = VectorUtils.getAngle(ship.getLocation(), proj.getLocation());
            float angleDif = MathUtils.getShortestRotation(fromToAngle, ship.getFacing());
            float amount = Global.getCombatEngine().getElapsedInLastFrame();
            float distance = MathUtils.getDistance(ship.getLocation(), proj.getLocation());
            float force = (ship.getCollisionRadius() / distance) * ANGLE_FORCE_MULTIPLIER;
            if (Math.abs(angleDif) >= 70f) {
                continue;
            } else if (Math.abs(angleDif) >= 60f) {
                force *= (70f - Math.abs(angleDif)) / 10f;
            }

            if (proj instanceof MissileAPI && proj.getOwner() != ship.getOwner()) {
                ((MissileAPI) proj).flameOut();
            }

            float dAngle = angleDif * amount * force;
            fromToAngle *= Math.PI / 180;
            Vector2f speedUp = new Vector2f((float) Math.cos(fromToAngle) * amount, (float) Math.sin(fromToAngle) *
                    amount);
            speedUp.scale(VELOCITY_FORCE_MULTIPLIER * power);

            Vector2f.add(proj.getVelocity(), speedUp, proj.getVelocity());
            VectorUtils.rotate(proj.getVelocity(), dAngle, proj.getVelocity());
            proj.setFacing(MathUtils.clampAngle(proj.getFacing() + dAngle * (float) (180 / Math.PI)));
        }

        for (int i = 0; i <= power * EXPLOSION_PULSES_PER_POWER; i++) {
            Vector2f origin = MathUtils.getPointOnCircumference(ship.getLocation(),
                    MathUtils.getRandomNumberInRange(5f * power, 30f * power),
                    ship.getFacing() + MathUtils.getRandomNumberInRange(-65f, 65f));
            Vector2f vel = MathUtils.getRandomPointInCircle(ship.getVelocity(), 150f + power * 150f);
            engine.spawnProjectile(ship, weapon, "brdy_scalaronrepulsor", origin, ship.getFacing() +
                    MathUtils.getRandomNumberInRange(-65f, 65f), vel);
        }

        List<CombatEntityAPI> entities = CombatUtils.getEntitiesWithinRange(ship.getLocation(), explosionRadius);
        size = entities.size();
        for (int i = 0; i < size; i++) {
            CombatEntityAPI tmp = entities.get(i);
            if (tmp == ship) {
                continue;
            }
            if (brdy_Utils.isRoot(ship)) {
                continue;
            }

            float angleDiff = Math.abs(MathUtils.getShortestRotation(ship.getFacing(), VectorUtils.getAngle(
                    ship.getLocation(), tmp.getLocation())));

            float mod = 1f - (MathUtils.getDistance(ship, tmp) / explosionRadius);
            if (angleDiff >= 70f) {
                continue;
            } else if (angleDiff >= 60f) {
                mod *= (70f - angleDiff) / 10f;
            }

            mod *= power;
            CombatUtils.applyForce(tmp, VectorUtils.getAngle(ship.getLocation(), tmp.getLocation()),
                    VELOCITY_FORCE_MULTIPLIER * (float) Math.sqrt(mod) / 2f);
        }
    }
}
