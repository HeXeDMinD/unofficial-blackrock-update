package data.shipsystems.scripts;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import org.dark.shaders.distortion.DistortionShader;
import org.dark.shaders.distortion.WaveDistortion;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

import java.awt.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class brdy_ScalarRejectorStats extends BaseShipSystemScript {
    // Distortion effect constants
    private WaveDistortion wave = null;
    private static final float DISTORTION_AMOUNT = 15f;
    private static final float DISTORTION_SIZE = 300f;
    private static final Vector2f ZERO = new Vector2f();

    // Pulling effect constants
    private static final float ANGLE_FORCE_MULTIPLIER = 0.5f;
    private static final float VELOCITY_FORCE_MULTIPLIER = 1000f;

    // Absorbsion effect constants
    public static float absorbedPower = 0;
    private static final float POWER_PER_FRIENDLY_DAMAGE_ABSORBED = 0.00025f;
    private static final float POWER_PER_HOSTILE_DAMAGE_ABSORBED = 0.0005f;
    private static final Map<DamageType, Float> DAMAGE_TYPE_POWER_ABSORBTION_MULTIPLIERS = new HashMap<>(5);
    static {
        DAMAGE_TYPE_POWER_ABSORBTION_MULTIPLIERS.put(DamageType.ENERGY, 1.05f);
        DAMAGE_TYPE_POWER_ABSORBTION_MULTIPLIERS.put(DamageType.FRAGMENTATION, 0.4f);
        DAMAGE_TYPE_POWER_ABSORBTION_MULTIPLIERS.put(DamageType.HIGH_EXPLOSIVE, 1.0f);
        DAMAGE_TYPE_POWER_ABSORBTION_MULTIPLIERS.put(DamageType.KINETIC, 1.0f);
        DAMAGE_TYPE_POWER_ABSORBTION_MULTIPLIERS.put(DamageType.OTHER, 0.5f);
    }
    private static final float SPARK_BRIGHTNESS = 1.8f;
    private static final Color SPARK_COLOR = new Color(59, 242, 152);
    private static final float SPARK_DURATION = 0.3f;
    private static final float SPARK_RADIUS = 9f;
    private static final float TEXT_AMOUNT_MULTIPLIER = 1000.0f;
    private static final Color TEXT_COLOR = new Color(60, 255, 245);
    private static final String SOUND_ABSORB = "brdy_system_ScalarCloak_Absorb";

    // Explosion effect constants
    private static final float EXPLOSION_DAMAGE_AMOUNT = 1000f;
    private static final float EXPLOSION_EMP_DAMAGE_AMOUNT = 750f;
    private static final DamageType EXPLOSION_DAMAGE_TYPE = DamageType.ENERGY;
    private static final Map<ShipAPI.HullSize, Float> DAMAGE_MOD = new HashMap<>(5);
    static {
        DAMAGE_MOD.put(ShipAPI.HullSize.FIGHTER, 0.95f);
        DAMAGE_MOD.put(ShipAPI.HullSize.FRIGATE, 0.9f);
        DAMAGE_MOD.put(ShipAPI.HullSize.DESTROYER, 0.45f);
        DAMAGE_MOD.put(ShipAPI.HullSize.CRUISER, 0.20f);
        DAMAGE_MOD.put(ShipAPI.HullSize.CAPITAL_SHIP, 0.15f);
    }
    private static final float EXPLOSION_DAMAGE_VS_ALLIES_MODIFIER = .11f;
    private static final float EXPLOSION_EMP_VS_ALLIES_MODIFIER = .05f;
    public static final float EXPLOSION_RADIUS = 650f;
    public static final float MAX_EXPLOSION_RADIUS = 1020.0f;
    private static final Color EXPLOSION_COLOR = new Color(160, 255, 114);
    private static final float EXPLOSION_VISUAL_RADIUS = 420f;
    private static final String SOUND_EXPLOSION = "brdy_system_ScalarRejector_Explode";

    // Explosion scaling constants
    private static final float MIN_POWER_MULTIPLIER = 0.3f;
    private static final float MAX_POWER_MULTIPLIER = 10.0f;
    public static final float MAX_RANGE_MULTIPLIER = 10f;

    private CombatEngineAPI engine;
    private ShipAPI ship;

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        if (!(stats.getEntity() instanceof ShipAPI)) {
            return;
        }

        stats.getAcceleration().modifyPercent(id, 200f * effectLevel);
        stats.getTurnAcceleration().modifyPercent(id, 200f * effectLevel);
        stats.getMaxSpeed().modifyPercent(id, 200f * effectLevel);
        stats.getMaxTurnRate().modifyPercent(id, 200f * effectLevel);

        ship = (ShipAPI) stats.getEntity();
        engine = Global.getCombatEngine();

        if (wave == null) {
            wave = new WaveDistortion(ship.getLocation(), ZERO);

            wave.setIntensity(DISTORTION_AMOUNT);
            wave.setSize(DISTORTION_SIZE);
            wave.flip(true);
            wave.fadeInIntensity(0.1f);

            DistortionShader.addDistortion(wave);
        } else {
            wave.setLocation(ship.getLocation());
        }

        float adjustedRange = ship.getMutableStats().getSystemRangeBonus().computeEffective(MAX_RANGE_MULTIPLIER);
        List<CombatEntityAPI> entities = CombatUtils.getEntitiesWithinRange(ship.getLocation(),
                ship.getCollisionRadius() * adjustedRange);
        for (CombatEntityAPI entity : entities) {
            if (!(entity instanceof DamagingProjectileAPI)) {
                continue;
            }

            DamagingProjectileAPI proj = (DamagingProjectileAPI) entity;

            if (state != State.OUT && MathUtils.getDistance(ship, proj) <= ship.getCollisionRadius()) {
                absorbProjectile(proj, effectLevel);
                continue;
            }

            pullProjectile(proj, state, effectLevel);
        }

        if (state == State.OUT) {
            if (wave != null) {
                wave.fadeOutIntensity(0.5f);
                wave = null;
            }
            ship.setPhased(false);
        } else {
            ship.setPhased(true);
        }

        if (state == State.OUT && absorbedPower > 0) {
            releaseBurst();
            absorbedPower = 0;
        }
    }

    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {
        if (index == 0) {
            return new StatusData("increased speed and maneuverability", false);
        } else {
            return null;
        }
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        ship = (ShipAPI) stats.getEntity();
        ship.setPhased(false);
    }

    private void pullProjectile(DamagingProjectileAPI proj, State state, float effectLevel){
        if (ship == null) { return; }

        float fromToAngle = VectorUtils.getAngle(ship.getLocation(), proj.getLocation());
        float angleDif = MathUtils.getShortestRotation(fromToAngle, MathUtils.clampAngle(proj.getFacing() + 180));
        float amount = Global.getCombatEngine().getElapsedInLastFrame();
        float distance = MathUtils.getDistance(ship.getLocation(), proj.getLocation());
        float force = (ship.getCollisionRadius() / distance) * effectLevel * ANGLE_FORCE_MULTIPLIER;
        float dAngle = angleDif * amount * force;

        if (proj instanceof MissileAPI && proj.getOwner() != ship.getOwner()) {
            ((MissileAPI) proj).flameOut();
        }

        fromToAngle *= Math.PI / 180;
        Vector2f speedUp = new Vector2f(
                (float) Math.cos(fromToAngle) * amount,
                (float) Math.sin(fromToAngle) * amount);
        speedUp.scale(VELOCITY_FORCE_MULTIPLIER);

        if (state != State.OUT) {
            dAngle = -dAngle;
            speedUp.scale(-1);
        }

        Vector2f.add(proj.getVelocity(), speedUp, proj.getVelocity());
        VectorUtils.rotate(proj.getVelocity(), dAngle, proj.getVelocity());
        proj.setFacing(MathUtils.clampAngle(proj.getFacing() + dAngle * (float) (180 / Math.PI)));
    }

    private void absorbProjectile(DamagingProjectileAPI proj, float effectLevel) {
        if (ship == null || engine == null) { return; }
        float powerAbsorbed = proj.getDamageAmount();
        powerAbsorbed *= (proj.getOwner() == ship.getOwner()) ? POWER_PER_FRIENDLY_DAMAGE_ABSORBED :
                POWER_PER_HOSTILE_DAMAGE_ABSORBED;
        powerAbsorbed *= DAMAGE_TYPE_POWER_ABSORBTION_MULTIPLIERS.get(proj.getDamageType());

        engine.addFloatingDamageText(ship.getLocation(), (powerAbsorbed*effectLevel) * TEXT_AMOUNT_MULTIPLIER,
                TEXT_COLOR, ship, proj);

        absorbedPower += powerAbsorbed*effectLevel;

        float sparkAngle = VectorUtils.getAngle(proj.getLocation(), ship.getLocation());
        sparkAngle *= Math.PI / 180f;
        Vector2f sparkVect = new Vector2f((float) Math.cos(sparkAngle), (float) Math.sin(sparkAngle));
        float distance = MathUtils.getDistance(proj, ship);
        float visualEffect = (float) Math.sqrt(powerAbsorbed * 1000);

        sparkVect.scale(3 * distance / SPARK_DURATION);

        Global.getSoundPlayer().playSound(SOUND_ABSORB, 1, visualEffect, proj.getLocation(), sparkVect);

        engine.addHitParticle(proj.getLocation(), sparkVect, SPARK_RADIUS * visualEffect + SPARK_RADIUS,
                SPARK_BRIGHTNESS, SPARK_DURATION, SPARK_COLOR);
        engine.removeEntity(proj);
    }

    private void releaseBurst() {
        if (ship == null || engine == null) {
            return;
        }

        float power = absorbedPower;
        power = Math.max(power, MIN_POWER_MULTIPLIER);
        power = Math.min(power, MAX_POWER_MULTIPLIER);
        power = (float) Math.sqrt(power);

        engine.spawnExplosion(ship.getLocation(), ship.getVelocity(), EXPLOSION_COLOR,
                EXPLOSION_VISUAL_RADIUS * power, 0.21f * power);
        engine.spawnExplosion(ship.getLocation(), ship.getVelocity(), EXPLOSION_COLOR,
                EXPLOSION_VISUAL_RADIUS * power / 2f, 0.19f * power);

        Global.getSoundPlayer().playSound(SOUND_EXPLOSION, 1f, power, ship.getLocation(), ship.getVelocity());

        float ADJUSTEDEXPLOSION_RANGE = ship.getMutableStats().getSystemRangeBonus().computeEffective(MAX_EXPLOSION_RADIUS);
        float explosionRadius = Math.min(ADJUSTEDEXPLOSION_RANGE, EXPLOSION_RADIUS * power);

        List<CombatEntityAPI> entities = CombatUtils.getEntitiesWithinRange(ship.getLocation(), explosionRadius);
        for (CombatEntityAPI tmp : entities) {
            if (tmp == ship) { continue; }

            float mod = 1f - (MathUtils.getDistance(ship, tmp) / explosionRadius);
            mod *= power;
            float damage = EXPLOSION_DAMAGE_AMOUNT * mod;
            float emp = EXPLOSION_EMP_DAMAGE_AMOUNT * mod;

            if (tmp instanceof ShipAPI) {
                ShipAPI victim = (ShipAPI) tmp;
                damage /= DAMAGE_MOD.get(victim.getHullSize());
                if (victim.getOwner() == ship.getOwner()) {
                    damage *= EXPLOSION_DAMAGE_VS_ALLIES_MODIFIER;
                    emp *= EXPLOSION_EMP_VS_ALLIES_MODIFIER;
                }
                else if (victim.isAlive() && !victim.isHulk() && !victim.isPhased() && victim.getHullSize() != ShipAPI.HullSize.FIGHTER){

                    //Refund cooldown and reduce flux based on enemies him
                    float refundedCooldown =  ship.getPhaseCloak().getCooldownRemaining() - ship.getPhaseCloak().getCooldown()*0.1f;
                    ship.getPhaseCloak().setCooldownRemaining(refundedCooldown);
                    float fluxRefund = 1 - (0.1f + power/100);
                    ship.getFluxTracker().setCurrFlux(ship.getCurrFlux()*fluxRefund);
                }
                for (int x = 0; x < 4; x++) {
                    engine.spawnEmpArc(ship, ship.getLocation(), victim, victim, EXPLOSION_DAMAGE_TYPE,
                            damage/10, emp/5, explosionRadius*3,
                            null, 20*power, EXPLOSION_COLOR, EXPLOSION_COLOR);

                    engine.spawnEmpArc(ship, MathUtils.getRandomPointInCircle(victim.getLocation(),
                                    victim.getCollisionRadius()), victim, victim, EXPLOSION_DAMAGE_TYPE,
                            damage/4, emp/4, explosionRadius,
                            null, 10f*power, EXPLOSION_COLOR, EXPLOSION_COLOR);
                }
            }
        }
    }
}
