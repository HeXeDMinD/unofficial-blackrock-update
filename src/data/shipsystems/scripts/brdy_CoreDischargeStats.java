package data.shipsystems.scripts;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import org.dark.shaders.light.LightShader;
import org.dark.shaders.light.StandardLight;
import org.lazywizard.lazylib.FastTrig;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

import java.awt.*;
import java.util.List;
// Dynastos System
public class brdy_CoreDischargeStats extends BaseShipSystemScript {

    private static final Color COLOR1 = new Color(119, 240, 167);
    private static final Color COLOR2 = new Color(119, 250, 167);
    private static final Vector2f ZERO = new Vector2f();

    public boolean activated = true;

    public static final float SYSTEM_RANGE = 575f;
    private static final DamageType DAMAGE_TYPE = DamageType.ENERGY;
    private static final float DAMAGE_AMOUNT = 125f;
    private static final float EMP_AMOUNT = 300.0f;
    private static final Color PARTICLE_COLOR = new Color(155, 240, 200);
    private static final float PARTICLE_BRIGHTNESS = 0.25f;
    private static final float PARTICLE_RADIUS = 5f;
    private static final float PARTICLE_SIZE = 20f;
    private static final int PARTICLES_PER_FRAME = 12; // Based on charge level
    private static final float LT_VISUAL_RADIUS = 120f;

    private boolean isActive = false;
    private StandardLight light;

    private static final String SOUND_ACTIVATE = "brdy_system_CoreDischarge_Activate";
    private static final String SOUND_DISCHARGE = "brdy_system_CoreDischarge_Discharge";
    private static final String SOUND_IMPACT = "brdy_system_CoreDischarge_Impact";

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        ShipAPI ship = (ShipAPI) stats.getEntity();

        if (state == State.IN) {
            if (activated) {
                Global.getSoundPlayer().playSound(SOUND_ACTIVATE, 1f, 0.95f, ship.getLocation(), ship.getVelocity());
                activated = false;
            }
            Vector2f loc = new Vector2f(ship.getLocation());
            loc.x -= 1f * FastTrig.cos(ship.getFacing() * Math.PI / 180f);
            loc.y -= 1f * FastTrig.sin(ship.getFacing() * Math.PI / 180f);

            // Everything in this block is only done once per chargeup
            if (!isActive) {
                isActive = true;

                light = new StandardLight(loc, ZERO, ZERO, null);
                light.setIntensity(0.6f);
                light.setSize(LT_VISUAL_RADIUS);
                light.setColor(PARTICLE_COLOR);
                light.fadeIn(2.15f);
                light.setLifetime(0.1f);
                light.setAutoFadeOutTime(0.17f);
                LightShader.addLight(light);
            } else {
                light.setLocation(loc);
            }

            // Exact amount per second doesn't matter since it's purely decorative
            Vector2f particlePos, particleVel;
            int numParticlesThisFrame = Math.round(effectLevel * PARTICLES_PER_FRAME);
            for (int x = 0; x < numParticlesThisFrame; x++) {
                particlePos = MathUtils.getRandomPointInCircle(ship.getLocation(), PARTICLE_RADIUS);
                particleVel = Vector2f.sub(ship.getLocation(), particlePos, null);
                Global.getCombatEngine().addHitParticle(particlePos, particleVel, PARTICLE_SIZE, PARTICLE_BRIGHTNESS, 0.25f,
                        PARTICLE_COLOR);
            }
        } // Cooldown, explode once system is finished

        if (state == State.OUT) {
            if (!activated) {
                //float shipRadius = ship.getCollisionRadius();
                float ADJUSTED_RANGE = ship.getMutableStats().getSystemRangeBonus().computeEffective(SYSTEM_RANGE);
                boolean didAnything = false;
                List<ShipAPI> targets = CombatUtils.getShipsWithinRange(ship.getLocation(), ADJUSTED_RANGE);
                for (ShipAPI target : targets) {
                    if (!target.isAlive() || target == ship
                            || target.getVariant().getHullMods().contains("vastbulk") || (target.getOwner() == ship.getOwner())) {
                        continue;
                    }

                    didAnything = true; //the big zap
                    for (int i = 0; i < 8; i++) {
                        Global.getCombatEngine().spawnEmpArc(ship, ship.getLocation(), ship, target,
                                DAMAGE_TYPE, DAMAGE_AMOUNT*effectLevel, EMP_AMOUNT*effectLevel, 100000f, SOUND_IMPACT,
                                20f, COLOR2, COLOR1);
                    }
                }

                if (didAnything) {
                    Global.getSoundPlayer().playSound(SOUND_DISCHARGE, 1f, 2f, ship.getLocation(), ship.getVelocity());
                    StandardLight light2 = new StandardLight(ship.getLocation(), ZERO, ZERO, null);
                    light2.setIntensity(1f);
                    light2.setSize(400f);
                    light2.setColor(COLOR1);
                    light2.fadeOut(1.5f);
                    LightShader.addLight(light2);

                }
            }
            activated = true;
            isActive = false;  //whatever, it works          
        }
    }
}
