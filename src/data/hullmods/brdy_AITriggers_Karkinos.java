package data.hullmods;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import data.scripts.util.brdy_Utils;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lwjgl.util.vector.Vector2f;

import java.util.List;
// Unused
public class brdy_AITriggers_Karkinos extends BaseHullMod {
    private boolean flameout;

    public void advanceInCombat(ShipAPI ship, float amount) {
        if (Global.getCombatEngine().isPaused() || !ship.isAlive()) {
            return;
        }

        if (ship.getShipAI() == null || ship.getSystem().isCoolingDown()) {
            return;
        }
        float estimated_damage = brdy_Utils.estimateAllIncomingDamage(ship,2f, false);

        if (ship.getAIFlags().hasFlag(ShipwideAIFlags.AIFlags.BACKING_OFF) && ship.getFluxLevel() <= 0.6f && estimated_damage < 100) {
            ship.blockCommandForOneFrame(ShipCommand.USE_SYSTEM);
            brdy_Utils.drawText(ship, "BLOCKED", 3f, 3f);
        }

        if (ship.getShipTarget() == null) {
            return;
        }
        if (ship.getShipTarget().isFighter()) {
            ship.blockCommandForOneFrame(ShipCommand.USE_SYSTEM);
            brdy_Utils.drawText(ship, "BLOCKED FIGHTER", 3f, 3f);
        }

        List<ShipEngineControllerAPI.ShipEngineAPI> engines = ship.getShipTarget().getEngineController().getShipEngines();
        if (!engines.isEmpty()) {
            int fraction = 0;
            for (ShipEngineControllerAPI.ShipEngineAPI e : engines) {
                if (e.isSystemActivated() || e.isDisabled()) {
                    fraction++;
                }
            }
            flameout = fraction == engines.size();
        }

        float shipDanger = 0.5f / ship.getHullLevel() + 2f * ship.getFluxLevel();
        float enemyVulnerability = 0.5f / ship.getShipTarget().getHullLevel() + 2f * ship.getShipTarget().getFluxLevel();
        if (flameout) {
            enemyVulnerability = enemyVulnerability + 2f;

        }

        if (ship.getAIFlags().hasFlag(ShipwideAIFlags.AIFlags.PURSUING)
                || ship.getAIFlags().hasFlag(ShipwideAIFlags.AIFlags.HARASS_MOVE_IN)
                || ship.getAIFlags().hasFlag(ShipwideAIFlags.AIFlags.MANEUVER_TARGET)){
            float distance_from_target = MathUtils.getDistance(ship, ship.getShipTarget());
            if (distance_from_target < 1600 && distance_from_target > 800) {
                if (enemyVulnerability > 1.75f || shipDanger <= 2.25 || enemyVulnerability > shipDanger) {
                    Vector2f targetLoc = ship.getShipTarget().getLocation();
                    float correctAngle = VectorUtils.getAngle(ship.getLocation(), targetLoc);
                    float aimAngle = MathUtils.getShortestRotation(ship.getFacing(), correctAngle);

                    if (aimAngle < 0) {
                        ship.giveCommand(ShipCommand.TURN_RIGHT, new Vector2f(), 0);
                    } else {
                        ship.giveCommand(ShipCommand.TURN_LEFT, new Vector2f(), 0);
                    }
                    if (Math.abs(aimAngle) < 15) {
                        ship.getAIFlags().setFlag(ShipwideAIFlags.AIFlags.DO_NOT_BACK_OFF, 2f);
                        ship.getAIFlags().setFlag(ShipwideAIFlags.AIFlags.SYSTEM_TARGET_COORDS, 1.25f, targetLoc);
                        ship.useSystem();
                        brdy_Utils.drawText(ship, "IM GOING IN", 3f, 3f);
                    }
                }
            }
        }
    }
}
