package data.hullmods;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import data.scripts.util.brdy_Utils;
import data.shipsystems.scripts.brdy_ScalarRejectorStats;
import org.lazywizard.lazylib.MathUtils;
import org.lwjgl.util.vector.Vector2f;

import java.util.List;

public class brdy_AITriggers_Imaginos extends BaseHullMod {
    public void advanceInCombat(ShipAPI ship, float amount) {
        if (Global.getCombatEngine().isPaused() || !ship.isAlive()) { return; }

        if (ship.getShipAI() == null){ return; }

        if (ship.getPhaseCloak().isActive() && ship.getFluxLevel() > 0.95f && Math.random() > 0.85f){
            ship.giveCommand(ShipCommand.VENT_FLUX, null, 0);
        }

        if (ship.getPhaseCloak().isCoolingDown()){ return; }

        float estimated_damage = brdy_Utils.estimateAllIncomingDamage(ship,1f, false);

        if (estimated_damage < 100) {
            ship.blockCommandForOneFrame(ShipCommand.TOGGLE_SHIELD_OR_PHASE_CLOAK);
        }

        if (ship.getFluxLevel() <= 0.55f && estimated_damage > 500) {
            ship.giveCommand(ShipCommand.TOGGLE_SHIELD_OR_PHASE_CLOAK, new Vector2f(), 0);
        }


        if (ship.getShipTarget() == null) { return; }

        // Check if target is flamed out
        boolean flamedout = false;
        if (ship.getShipTarget() != null){
            List<ShipEngineControllerAPI.ShipEngineAPI> engines = ship.getShipTarget().getEngineController().getShipEngines();
            if (!engines.isEmpty()){
                int fraction = 0;
                for(ShipEngineControllerAPI.ShipEngineAPI e : engines){
                    if (e.isSystemActivated() || e.isDisabled()){
                        fraction++;
                    }
                }
                flamedout = fraction == engines.size();
            }
        }

        float armorVulnerability = 1f / brdy_Utils.getArmorLevel(ship);

        float enemyVulnerability = 0.5f / ship.getShipTarget().getHullLevel() + 2f * ship.getShipTarget().getFluxLevel();
        if (flamedout){
            enemyVulnerability = enemyVulnerability+2f;
        }

        if (brdy_ScalarRejectorStats.absorbedPower > 0){
            float ADJUSTEDEXPLOSION_RANGE = ship.getMutableStats().getSystemRangeBonus().computeEffective(brdy_ScalarRejectorStats.MAX_EXPLOSION_RADIUS);
            float explosionRadius = Math.min(ADJUSTEDEXPLOSION_RANGE,
                    brdy_ScalarRejectorStats.EXPLOSION_RADIUS * brdy_ScalarRejectorStats.absorbedPower);

            if (MathUtils.getDistance(ship, ship.getShipTarget()) <= explosionRadius+500){
                if (enemyVulnerability > 1.75f || armorVulnerability <= 2.25 || enemyVulnerability > armorVulnerability){
                    if (MathUtils.getDistance(ship, ship.getShipTarget()) > explosionRadius+100) {
                        if (MathUtils.getDistance(ship, ship.getShipTarget()) > ship.getCollisionRadius()*4) {
                            ship.getAIFlags().setFlag(ShipwideAIFlags.AIFlags.DO_NOT_BACK_OFF, 0.25f);
                            ship.getAIFlags().setFlag(ShipwideAIFlags.AIFlags.PURSUING, 0.25f);
                            ship.getAIFlags().setFlag(ShipwideAIFlags.AIFlags.PHASE_ATTACK_RUN, 0.25f);
                        }
                    }
                }
            }
        }
    }
}
