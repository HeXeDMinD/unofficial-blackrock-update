package data.hullmods;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShieldAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.loading.HullModSpecAPI;
import com.fs.starfarer.api.ui.Alignment;
import com.fs.starfarer.api.ui.LabelAPI;
import com.fs.starfarer.api.ui.TooltipMakerAPI;
import com.fs.starfarer.api.util.Misc;
import data.scripts.util.MagicIncompatibleHullmods;
import org.lwjgl.input.Keyboard;

import java.awt.*;
import java.util.HashSet;
import java.util.Set;

public class brdy_FocusedShields extends BaseHullMod {

    final int SHIELDSPEED_BONUS = 150; //Applied to unfold and turn rates
    final float DAMAGEREDUCTION_BONUS = 15f;
    final int SHIELDUPKEEP_REDUCTION = 50;
    final float FORCEDSHIELD_ARC = 90f;

    final int SHIELDSPEED_BONUS_SMOD = 25; //Applied to unfold and turn rates
    final float DAMAGEREDUCTION_BONUS_SMOD = 5f;
    final int SHIELDUPKEEP_REDUCTION_SMOD = 25;
    final float FORCEDSHIELD_ARC_SMOD = 15f;

    private static final Color SHIELD_RING_COLOR = new Color(120, 255, 222, 255);
    private static final Color SHIELD_INNER_COLOR = new Color(70, 230, 196, 60);

    private static final String HULLMODID = "brdy_focusedshields";

    private static final Set<String> BLOCKED_HULLMODS = new HashSet<>(7);
    static {
        BLOCKED_HULLMODS.add("frontshield");
        BLOCKED_HULLMODS.add("frontemitter");
        BLOCKED_HULLMODS.add("adaptiveshields");
        BLOCKED_HULLMODS.add("advancedshieldemitter");
        BLOCKED_HULLMODS.add("hardenedshieldemitter");
        BLOCKED_HULLMODS.add("stabilizedshieldemitter");
        BLOCKED_HULLMODS.add("extendedshieldemitter");
        BLOCKED_HULLMODS.add("shield_shunt");
    }

    @Override
    public void advanceInCombat(ShipAPI ship, float amount) {
        if (ship.getShield() != null) {
            ship.getShield().setInnerColor(SHIELD_INNER_COLOR);
            ship.getShield().setRingColor(SHIELD_RING_COLOR);
        }
    }

    @Override
    public void applyEffectsAfterShipCreation(ShipAPI ship, String id) {
        for (String tmp : BLOCKED_HULLMODS) {
            if (ship.getVariant().getHullMods().contains(tmp)) {
                MagicIncompatibleHullmods.removeHullmodWithWarning(ship.getVariant(), tmp, HULLMODID);
            }
        }

        float FORCEDSHIELD_ARC = this.FORCEDSHIELD_ARC;
        ShieldAPI shield = ship.getShield();
        if (ship.getVariant().getSMods().contains(HULLMODID)) {
            FORCEDSHIELD_ARC = FORCEDSHIELD_ARC + FORCEDSHIELD_ARC_SMOD;
        }
        if (shield != null) {
            shield.setArc(FORCEDSHIELD_ARC);
        }
    }

    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        float SHIELDSPEED_BONUS = this.SHIELDSPEED_BONUS;
        float DAMAGEREDUCTION_BONUS = this.DAMAGEREDUCTION_BONUS;
        float SHIELDUPKEEP_REDUCTION = this.SHIELDUPKEEP_REDUCTION;

        if (stats.getVariant().getSMods().contains(HULLMODID)) {
            SHIELDSPEED_BONUS = SHIELDSPEED_BONUS + SHIELDSPEED_BONUS_SMOD;
            DAMAGEREDUCTION_BONUS = DAMAGEREDUCTION_BONUS + DAMAGEREDUCTION_BONUS_SMOD;
            SHIELDUPKEEP_REDUCTION = SHIELDUPKEEP_REDUCTION + SHIELDUPKEEP_REDUCTION_SMOD;
        }
        stats.getShieldTurnRateMult().modifyPercent(id, SHIELDSPEED_BONUS);
        stats.getShieldUnfoldRateMult().modifyPercent(id, SHIELDSPEED_BONUS);
        stats.getShieldDamageTakenMult().modifyMult(id, 1f - DAMAGEREDUCTION_BONUS * 0.01f);
        stats.getShieldUpkeepMult().modifyMult(id, 1f - SHIELDUPKEEP_REDUCTION * 0.01f);
    }

    @Override
    public String getDescriptionParam(int index, HullSize hullSize) {
        if (index == 0) {
            return "" + SHIELDSPEED_BONUS + "%";
        }
        if (index == 1) {
            return "" + (int) DAMAGEREDUCTION_BONUS + "%";
        }
        if (index == 2) {
            return "" + SHIELDUPKEEP_REDUCTION + "%";
        }
        if (index == 3) {
            return "" + (int) FORCEDSHIELD_ARC;
        }
        return null;
    }

    @Override
    public void addPostDescriptionSection(TooltipMakerAPI tooltip, HullSize hullSize, ShipAPI ship, float width, boolean isForModSpec) {
        if (!Keyboard.isKeyDown(Keyboard.getKeyIndex("F1")) &&
                (ship == null || !ship.getVariant().getSMods().contains(HULLMODID))){
            tooltip.addPara("Hold F1 to show S-mod effect info", Misc.getGrayColor(), 10);
            return;
        }
        Color colour_text = Misc.getTextColor();
        Color colour_highlight = Misc.getStoryOptionColor();

        if (ship == null || !ship.getVariant().getSMods().contains(HULLMODID)) {
            tooltip.addSectionHeading("Effect if S-modded", Alignment.MID, 10f);
            colour_text = Misc.getGrayColor();
        }

        HullModSpecAPI hullmod = Global.getSettings().getHullModSpec(HULLMODID);
        LabelAPI label = tooltip.addPara(hullmod.getDescriptionFormat(), 10f, colour_text, colour_highlight,
                (SHIELDSPEED_BONUS + SHIELDSPEED_BONUS_SMOD) + "%",
                (DAMAGEREDUCTION_BONUS + DAMAGEREDUCTION_BONUS_SMOD) + "%",
                (SHIELDUPKEEP_REDUCTION + SHIELDUPKEEP_REDUCTION_SMOD) + "%",
                "" + (FORCEDSHIELD_ARC + FORCEDSHIELD_ARC_SMOD)
        );

        label.setHighlight(
                (SHIELDSPEED_BONUS + SHIELDSPEED_BONUS_SMOD) + "%",
                (DAMAGEREDUCTION_BONUS + DAMAGEREDUCTION_BONUS_SMOD) + "%",
                (SHIELDUPKEEP_REDUCTION + SHIELDUPKEEP_REDUCTION_SMOD) + "%",
                "" + (FORCEDSHIELD_ARC + FORCEDSHIELD_ARC_SMOD)
        );

        label.setHighlightColors(colour_highlight);
    }

    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        boolean applicable = true;
        for (String tmp : BLOCKED_HULLMODS) {
            if (ship.getVariant().getHullMods().contains(tmp)) {
                applicable = false;
            }
        }
        if (!ship.getHullSpec().getHullId().startsWith("brdy_") &&
                !ship.getHullSpec().getHullId().startsWith("brdyx_")){
            applicable = false;
        }
        return applicable;
    }

    @Override
    public String getUnapplicableReason(ShipAPI ship) {
        if (!(ship.getHullSpec().getHullId().startsWith("brdy_") ||
                ship.getHullSpec().getHullId().startsWith("brdyx_"))) {
            return "Must be installed on a Blackrock ship";
        }
        for (String tmp : BLOCKED_HULLMODS) {
            if (ship.getVariant().getHullMods().contains(tmp)) {
                return "Incompatible with " + Global.getSettings().getHullModSpec(tmp).getDisplayName();
            }
        }
        return null;
    }
}
