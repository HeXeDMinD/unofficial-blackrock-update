package data.hullmods;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.loading.HullModSpecAPI;
import com.fs.starfarer.api.ui.Alignment;
import com.fs.starfarer.api.ui.LabelAPI;
import com.fs.starfarer.api.ui.TooltipMakerAPI;
import com.fs.starfarer.api.util.Misc;
import data.scripts.util.MagicIncompatibleHullmods;
import org.lwjgl.input.Keyboard;

import java.awt.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class brdy_StrikeSuite extends BaseHullMod {

    final int DAMAGE_BONUS = 10;
    final int WEAPON_FLUX_REDUCTION = 10;
    final int PROJ_SPEEDMULT = 25;

    final int DAMAGE_BONUS_SMOD = 5;
    final int WEAPON_FLUX_REDUCTION_SMOD = 5;
    final int PROJ_SPEEDMULT_SMOD = 10;

    private static final String HULLMODID = "brdy_strikesuite";

    public static final Map<HullSize, Float> WEAPONRANGE_BONUS = new HashMap<>(4);
    static {
        WEAPONRANGE_BONUS.put(HullSize.FRIGATE, 5f);
        WEAPONRANGE_BONUS.put(HullSize.DESTROYER, 10f);
        WEAPONRANGE_BONUS.put(HullSize.CRUISER, 20f);
        WEAPONRANGE_BONUS.put(HullSize.CAPITAL_SHIP, 35f);
    }

    public static final Map<HullSize, Float> WEAPONRANGE_BONUS_SMOD = new HashMap<>(4);
    static {
        WEAPONRANGE_BONUS_SMOD.put(HullSize.FRIGATE, 5f);
        WEAPONRANGE_BONUS_SMOD.put(HullSize.DESTROYER, 5f);
        WEAPONRANGE_BONUS_SMOD.put(HullSize.CRUISER, 5f);
        WEAPONRANGE_BONUS_SMOD.put(HullSize.CAPITAL_SHIP, 5f);
    }

    private static final Set<String> BLOCKED_HULLMODS = new HashSet<>(3);
    static {
        BLOCKED_HULLMODS.add("dedicated_targeting_core");
        BLOCKED_HULLMODS.add("targetingunit");
        BLOCKED_HULLMODS.add("safetyoverrides");
        BLOCKED_HULLMODS.add("ballistic_rangefinder");
    }

    @Override
    public void applyEffectsAfterShipCreation(ShipAPI ship, String id) {
        for (String tmp : BLOCKED_HULLMODS) {
            if (ship.getVariant().getHullMods().contains(tmp)) {
                MagicIncompatibleHullmods.removeHullmodWithWarning(ship.getVariant(), tmp, HULLMODID);
            }
        }
    }

    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        float DAMAGE_BONUS = this.DAMAGE_BONUS;
        float WEAPON_FLUX_REDUCTION = this.WEAPON_FLUX_REDUCTION;
        float PROJ_SPEEDMULT = this.PROJ_SPEEDMULT;
        float WEAPONRANGE_BONUS = brdy_StrikeSuite.WEAPONRANGE_BONUS.get(hullSize);

        if (stats.getVariant().getSMods().contains(HULLMODID)) {
            DAMAGE_BONUS = DAMAGE_BONUS + DAMAGE_BONUS_SMOD;
            WEAPON_FLUX_REDUCTION = WEAPON_FLUX_REDUCTION + WEAPON_FLUX_REDUCTION_SMOD;
            PROJ_SPEEDMULT = PROJ_SPEEDMULT + PROJ_SPEEDMULT_SMOD;
            WEAPONRANGE_BONUS = brdy_StrikeSuite.WEAPONRANGE_BONUS.get(hullSize) +
                    brdy_StrikeSuite.WEAPONRANGE_BONUS_SMOD.get(hullSize);
        }

    stats.getEnergyWeaponDamageMult().modifyPercent(id, DAMAGE_BONUS);
    stats.getBallisticWeaponDamageMult().modifyPercent(id, DAMAGE_BONUS);
    stats.getMissileWeaponDamageMult().modifyPercent(id, DAMAGE_BONUS);
    stats.getBallisticWeaponFluxCostMod().modifyMult(id, 1f - WEAPON_FLUX_REDUCTION * 0.01f);
    stats.getEnergyWeaponFluxCostMod().modifyMult(id, 1f - WEAPON_FLUX_REDUCTION * 0.01f);
    stats.getMissileWeaponFluxCostMod().modifyMult(id, 1f - WEAPON_FLUX_REDUCTION * 0.01f);
    stats.getProjectileSpeedMult().modifyPercent(id, PROJ_SPEEDMULT);
	stats.getBallisticWeaponRangeBonus().modifyPercent(id, WEAPONRANGE_BONUS);
	stats.getEnergyWeaponRangeBonus().modifyPercent(id, WEAPONRANGE_BONUS);
    }

    @Override
    public String getDescriptionParam(int index, HullSize hullSize) {
        if (index == 0) {
            return "" + DAMAGE_BONUS + "%";
        }
        if (index == 1) {
            return "" + WEAPON_FLUX_REDUCTION + "%";
        }
        if (index == 2) {
            return "" + PROJ_SPEEDMULT + "%";
        }        
         if (index == 3) {
            return "" + WEAPONRANGE_BONUS.get(HullSize.FRIGATE).intValue() + "%";
        }
        if (index == 4) {
            return "" + WEAPONRANGE_BONUS.get(HullSize.DESTROYER).intValue() + "%";
        }
        if (index == 5) {
            return "" + WEAPONRANGE_BONUS.get(HullSize.CRUISER).intValue() + "%";
        }
        if (index == 6) {
            return "" + WEAPONRANGE_BONUS.get(HullSize.CAPITAL_SHIP).intValue() + "%";
        }       
        return null;
    }

    @Override
    public void addPostDescriptionSection(TooltipMakerAPI tooltip, HullSize hullSize, ShipAPI ship, float width, boolean isForModSpec) {
        if (!Keyboard.isKeyDown(Keyboard.getKeyIndex("F1")) &&
                (ship == null || !ship.getVariant().getSMods().contains(HULLMODID))){
            tooltip.addPara("Hold F1 to show S-mod effect info", Misc.getGrayColor(), 10);
            return;
        }
        Color colour_text = Misc.getTextColor();
        Color colour_highlight = Misc.getStoryOptionColor();

        if (ship == null || !ship.getVariant().getSMods().contains(HULLMODID)) {
            tooltip.addSectionHeading("Effect if S-modded", Alignment.MID, 10f);
            colour_text = Misc.getGrayColor();
        }


        HullModSpecAPI hullmod = Global.getSettings().getHullModSpec(HULLMODID);
        LabelAPI label = tooltip.addPara(hullmod.getDescriptionFormat(), 10f, colour_text, colour_highlight,
                (DAMAGE_BONUS + DAMAGE_BONUS_SMOD) + "%",
                (WEAPON_FLUX_REDUCTION + WEAPON_FLUX_REDUCTION_SMOD) + "%",
                (PROJ_SPEEDMULT + PROJ_SPEEDMULT_SMOD) + "%",

                (WEAPONRANGE_BONUS.get(HullSize.FRIGATE).intValue() +
                        WEAPONRANGE_BONUS_SMOD.get(HullSize.FRIGATE).intValue()) + "%",
                (WEAPONRANGE_BONUS.get(HullSize.DESTROYER).intValue() +
                        WEAPONRANGE_BONUS_SMOD.get(HullSize.DESTROYER).intValue()) + "%",
                (WEAPONRANGE_BONUS.get(HullSize.CRUISER).intValue() +
                        WEAPONRANGE_BONUS_SMOD.get(HullSize.CRUISER).intValue()) + "%",
                (WEAPONRANGE_BONUS.get(HullSize.CAPITAL_SHIP).intValue() +
                        WEAPONRANGE_BONUS_SMOD.get(HullSize.CAPITAL_SHIP).intValue()) + "%"
        );

        label.setHighlight(
                (DAMAGE_BONUS + DAMAGE_BONUS_SMOD) + "%",
                (WEAPON_FLUX_REDUCTION + WEAPON_FLUX_REDUCTION_SMOD) + "%",
                (PROJ_SPEEDMULT + PROJ_SPEEDMULT_SMOD) + "%",

                (WEAPONRANGE_BONUS.get(HullSize.FRIGATE).intValue() +
                        WEAPONRANGE_BONUS_SMOD.get(HullSize.FRIGATE).intValue()) + "%",
                (WEAPONRANGE_BONUS.get(HullSize.DESTROYER).intValue() +
                        WEAPONRANGE_BONUS_SMOD.get(HullSize.DESTROYER).intValue()) + "%",
                (WEAPONRANGE_BONUS.get(HullSize.CRUISER).intValue() +
                        WEAPONRANGE_BONUS_SMOD.get(HullSize.CRUISER).intValue()) + "%",
                (WEAPONRANGE_BONUS.get(HullSize.CAPITAL_SHIP).intValue() +
                        WEAPONRANGE_BONUS_SMOD.get(HullSize.CAPITAL_SHIP).intValue()) + "%"
                );

        label.setHighlightColors(colour_highlight);
    }

    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        boolean applicable = true;
        for (String tmp : BLOCKED_HULLMODS) {
            if (ship.getVariant().getHullMods().contains(tmp)) {
                applicable = false;
            }
        }
        if (!ship.getHullSpec().getHullId().startsWith("brdy_") &&
                !ship.getHullSpec().getHullId().startsWith("brdyx_")){
            applicable = false;
        }
        return applicable;
    }

    @Override
    public String getUnapplicableReason(ShipAPI ship) {
        if (!(ship.getHullSpec().getHullId().startsWith("brdy_") ||
                ship.getHullSpec().getHullId().startsWith("brdyx_"))) {
            return "Must be installed on a Blackrock ship";
        }
        for (String tmp : BLOCKED_HULLMODS) {
            if (ship.getVariant().getHullMods().contains(tmp)) {
                return "Incompatible with " + Global.getSettings().getHullModSpec(tmp).getDisplayName();
            }
        }
        return null;
    }
}
