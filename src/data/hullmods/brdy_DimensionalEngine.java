package data.hullmods;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.combat.ShipHullSpecAPI.ShipTypeHints;
import com.fs.starfarer.api.combat.ShipwideAIFlags.AIFlags;
import com.fs.starfarer.api.combat.WeaponAPI.AIHints;
import com.fs.starfarer.api.combat.WeaponAPI.DerivedWeaponStatsAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import data.scripts.util.brdy_Utils;
import org.lazywizard.lazylib.CollectionUtils;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

import java.awt.*;
import java.util.List;
import java.util.*;

public class brdy_DimensionalEngine extends BaseHullMod {

    private static final float DAMAGE_FACTOR = 0.015f;
    private static final float DAMAGE_POWER = 1.4f;
    private static final float EMP_FACTOR = 0.67f;
    private static final float PROFILE_INCREASE = 100f;

    private static final Set<String> VENT_AI_FOR_THESE_SHIPS = new HashSet<>(2);

    private static final Map<HullSize, Float> FLUX_ACTIVEVENT_BONUS = new HashMap<>(5);
    private static final Map<HullSize, Float> RESERVED_FLUX_DEFAULTS = new HashMap<>(6);
    private static final Map<String, Float> RESERVED_FLUX_EXCEPTIONS = new HashMap<>(1);
    private static final String HULLMOD_ID = "brdy_dimensionalengine";

    static {
        VENT_AI_FOR_THESE_SHIPS.add("brdyx_imaginos");
        VENT_AI_FOR_THESE_SHIPS.add("brdyx_morpheus");
    }

    static {
        RESERVED_FLUX_EXCEPTIONS.put("brdyx_imaginos", 0.7f);
        RESERVED_FLUX_EXCEPTIONS.put("brdyx_morpheus", 0.7f);
    }

    static {
        RESERVED_FLUX_DEFAULTS.put(HullSize.FIGHTER, 0.4f);
        RESERVED_FLUX_DEFAULTS.put(HullSize.FRIGATE, 0.5f);
        RESERVED_FLUX_DEFAULTS.put(HullSize.DESTROYER, 0.6f);
        RESERVED_FLUX_DEFAULTS.put(HullSize.DEFAULT, 0.6f);
        RESERVED_FLUX_DEFAULTS.put(HullSize.CRUISER, 0.7f);
        RESERVED_FLUX_DEFAULTS.put(HullSize.CAPITAL_SHIP, 0.8f);
    }

    static {
        FLUX_ACTIVEVENT_BONUS.put(HullSize.FIGHTER, 0f);
        FLUX_ACTIVEVENT_BONUS.put(HullSize.FRIGATE, 80f);
        FLUX_ACTIVEVENT_BONUS.put(HullSize.DESTROYER, 60f);
        FLUX_ACTIVEVENT_BONUS.put(HullSize.CRUISER, 50f);
        FLUX_ACTIVEVENT_BONUS.put(HullSize.CAPITAL_SHIP, 40f);
    }

    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        stats.getVentRateMult().modifyPercent(id, FLUX_ACTIVEVENT_BONUS.get(hullSize));
        stats.getSensorProfile().modifyPercent(id, PROFILE_INCREASE);
    }

    @Override
    public String getDescriptionParam(int index, HullSize hullSize) {
        if (index == 0) {
            return "" + (FLUX_ACTIVEVENT_BONUS.get(HullSize.FRIGATE)).intValue();
        }
        if (index == 1) {
            return "" + (FLUX_ACTIVEVENT_BONUS.get(HullSize.DESTROYER)).intValue();
        }
        if (index == 2) {
            return "" + (FLUX_ACTIVEVENT_BONUS.get(HullSize.CRUISER)).intValue();
        }
        if (index == 3) {
            return "" + (FLUX_ACTIVEVENT_BONUS.get(HullSize.CAPITAL_SHIP)).intValue();
        }
        if (index == 4) {
            return "" + PROFILE_INCREASE + "%";
        }
        return null;
    }

    private final Color CONTRAIL_COLOR = new Color(125, 250, 170, 44);
    private final Color ENGINE_COLOR = new Color(107, 235, 255, 235);

    @Override
    public void advanceInCombat(ShipAPI ship, float amount) {
        CombatEngineAPI engine = Global.getCombatEngine();

        if (Global.getCombatEngine().isPaused()) {
            return;
        }

        //////////////////////////////////////////
        //  SAFETY OVERRIDES COLOR ADJUSTEMENT  //
        //////////////////////////////////////////
        if (ship.getVariant().getHullMods().contains("safetyoverrides")) {
            ship.getEngineController().fadeToOtherColor( this, ENGINE_COLOR, CONTRAIL_COLOR, 1, 0.8f);
        }
        
        if (!VENT_AI_FOR_THESE_SHIPS.contains(ship.getHullSpec().getBaseHullId())) {
            return;
        }

        //////////////////////////////////////////
        //              VENT AI BELOW           //
        //////////////////////////////////////////

        ShipwideAIFlags flags = ship.getAIFlags();

        if (ship.isShuttlePod() || ship.isDrone() || ship.getShipAI() == null || flags == null) {
            return;
        }

        if (ship.getHullSpec().getHullId().contentEquals("ssp_excelsior")) {
            flags.setFlag(AIFlags.DO_NOT_VENT);
            flags.removeFlag(AIFlags.DO_NOT_USE_FLUX);
            flags.removeFlag(AIFlags.HAS_INCOMING_DAMAGE);
            flags.removeFlag(AIFlags.KEEP_SHIELDS_ON);
            return;
        }

        if (flags.hasFlag(AIFlags.DO_NOT_VENT)) {
            return;
        }

        if (ship.isFighter() && !ship.getHullSpec().getHullId().contentEquals("ssp_lightning")) {
            return;
        }

        if (Math.random() > 0.97) {
            FluxTrackerAPI shipFT = ship.getFluxTracker();
            MutableShipStatsAPI shipMS = ship.getMutableStats();
            if (shipFT.isOverloadedOrVenting()) {
                return;
            }

            float range = (float) Math.sqrt(ship.getCollisionRadius()) * 200f;

            List<DamagingProjectileAPI> projectiles = engine.getProjectiles();
            List<DamagingProjectileAPI> nearbyThreats = new ArrayList<>(projectiles.size() / 4);
            for (DamagingProjectileAPI tmp : projectiles) {
                if (MathUtils.isWithinRange(tmp.getLocation(), ship.getLocation(), range)) {
                    nearbyThreats.add(tmp);
                }
            }
            nearbyThreats = CollectionUtils.filter(nearbyThreats, new brdy_Utils.FilterMisses(ship));
            List<MissileAPI> nearbyMissiles = AIUtils.getNearbyEnemyMissiles(ship, range / 2f);
            for (MissileAPI missile : nearbyMissiles) {
                if (missile.getMissileAI() != null && !missile.isFizzling() && !missile.isFlare()
                        && !missile.getMissileAI().getClass().getSimpleName().contentEquals("RocketAI")) {
                    nearbyThreats.add(missile);
                }
            }

            FleetMemberAPI member = CombatUtils.getFleetMember(ship);
            float shipStrength = 1f;
            if (member != null) {
                shipStrength = 0.1f + member.getFleetPointCost();
            }

            float armorlevel = brdy_Utils.getArmorLevel(ship);
            float maxVentTime = shipFT.getMaxFlux() / (shipMS.getFluxDissipation().getModifiedValue() * 2f
                    * shipMS.getVentRateMult().getModifiedValue());
            float decisionLevel = (5f * (float) Math.sqrt(ship.getHitpoints() / 100f) + 0.5f * (float) Math.sqrt(
                    ship.getHitpoints() / 100f)
                    * (float) Math.sqrt(armorlevel
                            * ship.getArmorGrid().getArmorRating() / 10f)
                    / (maxVentTime / 8f));
            float threatLevel = 0f;
            float opportunityLevel = 0f;
            int threatsSize = nearbyThreats.size();
            for (int j = 0; j < threatsSize; j++) {
                DamagingProjectileAPI threat = nearbyThreats.get(j);
                float damage = threat.getDamageAmount() + threat.getEmpAmount() * EMP_FACTOR;
                damage /= (float) Math.sqrt(Math.max((ship.getHitpoints() / 1000000f) * armorlevel
                        * ship.getArmorGrid().getArmorRating(), 0.1f));
                if (threat.getDamageType() == DamageType.HIGH_EXPLOSIVE) {
                    damage = (float) Math.pow(damage * (1f + armorlevel * 0.25f) * DAMAGE_FACTOR * 1.25f, DAMAGE_POWER);
                } else if (threat.getDamageType() == DamageType.KINETIC) {
                    damage = (float) Math.pow(damage * (1f - armorlevel * 0.25f) * DAMAGE_FACTOR * 1.25f, DAMAGE_POWER);
                } else if (threat.getDamageType() == DamageType.ENERGY) {
                    damage = (float) Math.pow(damage * DAMAGE_FACTOR * 1.25f, DAMAGE_POWER);
                } else if (threat.getDamageType() == DamageType.FRAGMENTATION) {
                    damage = (float) Math.pow(damage * (1f - armorlevel * 0.5f) * DAMAGE_FACTOR * 1.25f, DAMAGE_POWER);
                }

                //Global.getCombatEngine().addFloatingText(threat.getLocation(), "" + damage, 15f, Color.white, ship, 0f, 0f);
                threatLevel += damage;
            }

            List<BeamAPI> nearbyBeams = engine.getBeams();
            threatsSize = nearbyBeams.size();
            for (int j = 0; j < threatsSize; j++) {
                BeamAPI beam = nearbyBeams.get(j);
                if (beam.getDamageTarget() == ship) {
                    float damage;
                    if (beam.getWeapon().isBurstBeam()) {
                        damage = beam.getWeapon().getDerivedStats().getBurstDamage()
                                / beam.getWeapon().getDerivedStats().getBurstFireDuration()
                                + beam.getWeapon().getDerivedStats().getEmpPerSecond() * EMP_FACTOR;
                    } else {
                        damage = beam.getWeapon().getDerivedStats().getDps()
                                + beam.getWeapon().getDerivedStats().getEmpPerSecond() * EMP_FACTOR;
                    }
                    damage /= (float) Math.sqrt(Math.max((ship.getHitpoints() / 1000000f) * armorlevel
                            * ship.getArmorGrid().getArmorRating(), 0.1f));
                    if (beam.getWeapon().getDamageType() == DamageType.HIGH_EXPLOSIVE) {
                        threatLevel
                                += Math.pow(damage * (1f + armorlevel * 0.25f) * DAMAGE_FACTOR * 0.75f, DAMAGE_POWER);
                    } else if (beam.getWeapon().getDamageType() == DamageType.KINETIC) {
                        threatLevel
                                += Math.pow(damage * (1f - armorlevel * 0.25f) * DAMAGE_FACTOR * 0.75f, DAMAGE_POWER);
                    } else if (beam.getWeapon().getDamageType() == DamageType.ENERGY) {
                        threatLevel += Math.pow(damage * DAMAGE_FACTOR * 0.75f, DAMAGE_POWER);
                    } else if (beam.getWeapon().getDamageType() == DamageType.FRAGMENTATION) {
                        threatLevel += Math.pow(damage * (1f - armorlevel * 0.5f) * DAMAGE_FACTOR * 0.75f, DAMAGE_POWER);
                    }
                }
            }

            List<ShipAPI> nearbyEnemies = AIUtils.getEnemiesOnMap(ship);
            threatsSize = nearbyEnemies.size();
            for (int j = 0; j < threatsSize; j++) {
                ShipAPI enemy = nearbyEnemies.get(j);
                float falloff = 1f;
                float distance = MathUtils.getDistance(ship, enemy);
                if (distance >= range) {
                    continue;
                }

                if (distance >= range / 2f) {
                    falloff = (1f - distance / range) * 2f;
                }
                FluxTrackerAPI enemyFT = enemy.getFluxTracker();
                MutableShipStatsAPI enemyMS = enemy.getMutableStats();
                float fluxDifference = ((enemyFT.getMaxFlux() - enemyFT.getCurrFlux()) - (shipFT.getMaxFlux()
                        - shipFT.getCurrFlux()))
                        / (shipFT.getMaxFlux() + 1f);
                if ((enemyFT.isOverloadedOrVenting() || fluxDifference <= -0.5f) && (member == null
                        || !member.isCivilian())) {
                    FleetMemberAPI enemyMember = CombatUtils.getFleetMember(enemy);
                    if (enemyMember != null) {
                        if (ship.getShipTarget() == enemy) {
                            opportunityLevel += 100f * Math.max(-fluxDifference, 0.5f) * (float) Math.sqrt(
                                    enemyMember.getFleetPointCost()) / shipStrength;
                        } else {
                            opportunityLevel += 30f * Math.max(-fluxDifference, 0.5f) * (float) Math.sqrt(
                                    enemyMember.getFleetPointCost()) / shipStrength;
                        }
                    }
                }

                float speedFactor = (float) Math.sqrt(enemy.getMutableStats().getMaxSpeed().getModifiedValue()
                        / (ship.getMutableStats().getMaxSpeed().getModifiedValue() + 20f));

                if (distance <= range / 2f) {
                    FleetMemberAPI enemyMember = CombatUtils.getFleetMember(enemy);
                    if (enemyMember != null) {
                        float fall = (range / 2f - distance) / (range / 2f);
                        if (ship.getShipTarget() == enemy) {
                            threatLevel += speedFactor * 100f * fall
                                    * (float) Math.sqrt(enemyMember.getFleetPointCost()) / shipStrength;
                        } else {
                            threatLevel += speedFactor * 30f * fall * (float) Math.sqrt(
                                    (enemyMember.getFleetPointCost())) / shipStrength;
                        }
                    }
                }

                float shipTTV = shipFT.getCurrFlux() / (shipMS.getFluxDissipation().getModifiedValue() * 2f
                        * shipMS.getVentRateMult().getModifiedValue());
                float enemyTTV = enemyFT.getCurrFlux() / (enemyMS.getFluxDissipation().getModifiedValue() * 2f
                        * enemyMS.getVentRateMult().getModifiedValue());
                if (enemyFT.isOverloaded() && enemyFT.getOverloadTimeRemaining() > shipTTV + 2.5f && distance >= range
                        / 2f) {
                    continue;
                }
                if (enemyFT.isVenting() && enemyTTV > shipTTV + 2.5f && distance >= range / 2f) {
                    continue;
                }
                List<WeaponAPI> weapons = enemy.getAllWeapons();

                for (WeaponAPI weapon : weapons) {
                    float rangeSlip = enemyMS.getMaxSpeed().getModifiedValue() * Math.max(enemyTTV, Math.max(weapon.getCooldownRemaining(), shipTTV));
                    float weaponDist = MathUtils.getDistance(ship, weapon.getLocation());
                    float weaponRange = weapon.getRange() + rangeSlip;
                    float availableFlux = Math.min(enemyFT.getMaxFlux() - enemyFT.getCurrFlux() + Math.max(
                                    (enemyMS.getFluxDissipation().getModifiedValue()
                                            + enemyMS.getVentRateMult().getModifiedValue()) * enemyTTV,
                                    enemyMS.getFluxDissipation().getModifiedValue() * Math.max(weapon.getCooldownRemaining(),
                                            shipTTV)),
                            enemyFT.getMaxFlux());
                    if (!weapon.isPermanentlyDisabled()
                            && ((!weapon.isFiring() && weapon.getCooldownRemaining() <= 0f)
                            || weapon.getCooldownRemaining()
                            <= shipTTV)
                            && (weapon.getAmmo() > 0 || !weapon.usesAmmo()) && weapon.getFluxCostToFire() <= availableFlux
                            && ((brdy_Utils.getTimeToAim(weapon, ship.getLocation()) <= shipTTV && weaponRange >= weaponDist)
                            || ((weapon.getSpec().getAIHints().contains(AIHints.DO_NOT_AIM)
                            || weapon.getSpec().getAIHints().contains(
                            AIHints.HEATSEEKER)) && weaponRange >= weaponDist))) {
                        float damage;
                        DerivedWeaponStatsAPI stats = weapon.getDerivedStats();
                        if (weapon.isBurstBeam()) {
                            damage = stats.getBurstDamage() + stats.getEmpPerSecond() * EMP_FACTOR
                                    * stats.getBurstFireDuration();
                        } else if (stats.getSustainedDps() < stats.getDps() && weapon.usesAmmo()) {
                            damage = Math.max((stats.getDamagePerShot() + stats.getEmpPerShot() * EMP_FACTOR),
                                    (stats.getDps() + stats.getEmpPerSecond()
                                            * 0.25f) * weapon.getAmmo()
                                            / weapon.getMaxAmmo()
                                            + stats.getSustainedDps() *
                                            (1f - (float)weapon.getAmmo() / (float)weapon.getMaxAmmo()));
                        } else {
                            damage = Math.max((stats.getDamagePerShot() + stats.getEmpPerShot() * EMP_FACTOR),
                                    stats.getDps() + stats.getEmpPerSecond()
                                            * EMP_FACTOR);
                        }

                        if (Math.abs(weapon.distanceFromArc(ship.getLocation())) >= 30f
                                && (weapon.getSpec().getAIHints().contains(AIHints.DO_NOT_AIM)
                                || weapon.getSpec().getAIHints().contains(AIHints.HEATSEEKER))) {
                            damage /= Math.abs(weapon.distanceFromArc(ship.getLocation())) / 30f;
                        }
                        if (weapon.getDamageType() == DamageType.HIGH_EXPLOSIVE) {
                            damage = (float) Math.sqrt(falloff) * (float) Math.pow(damage * (1f + armorlevel * 0.25f)
                                    * DAMAGE_FACTOR, DAMAGE_POWER);
                        } else if (weapon.getDamageType() == DamageType.KINETIC) {
                            damage = (float) Math.sqrt(falloff) * (float) Math.pow(damage * (1f - armorlevel * 0.25f)
                                    * DAMAGE_FACTOR, DAMAGE_POWER);
                        } else if (weapon.getDamageType() == DamageType.ENERGY) {
                            damage = (float) Math.sqrt(falloff) * (float) Math.pow(damage * DAMAGE_FACTOR, DAMAGE_POWER);
                        } else if (weapon.getDamageType() == DamageType.FRAGMENTATION) {
                            damage = (float) Math.sqrt(falloff) * (float) Math.pow(damage * (1f - armorlevel * 0.5f)
                                    * DAMAGE_FACTOR, DAMAGE_POWER);
                        }

                        //Global.getCombatEngine().addFloatingText(weapon.getLocation(), "" + damage, 15f, Color.white, enemy, 0f, 0f);
                        threatLevel += speedFactor * damage;
                    }
                }
            }

            float allyLevel = 0f;
            List<ShipAPI> nearbyAllies = AIUtils.getNearbyAllies(ship, range / 2f);
            threatsSize = nearbyAllies.size();
            for (int j = 0; j < threatsSize; j++) {
                ShipAPI ally = nearbyAllies.get(j);
                if (ally == ship || ally.isDrone() || ally.isFighter() || ally.getHullSpec().getHints().contains(
                        ShipTypeHints.CIVILIAN)) {
                    continue;
                }
                FleetMemberAPI allyMember = CombatUtils.getFleetMember(ally);
                if (allyMember != null) {
                    allyLevel += allyMember.getFleetPointCost();
                } else {
                    if (ally.getHullSize() == HullSize.FRIGATE) {
                        allyLevel += 4f;
                    } else if (ally.getHullSize() == HullSize.DESTROYER) {
                        allyLevel += 8f;
                    } else if (ally.getHullSize() == HullSize.CRUISER) {
                        allyLevel += 14f;
                    } else if (ally.getHullSize() == HullSize.CAPITAL_SHIP) {
                        allyLevel += 28f;
                    }
                }
            }

            float reserved = RESERVED_FLUX_DEFAULTS.get(ship.getHullSize());
            Float specificReserved = RESERVED_FLUX_EXCEPTIONS.get(ship.getHullSpec().getBaseHullId());
            if (specificReserved != null) {
                reserved = specificReserved;
            }

            decisionLevel *= (shipFT.getCurrFlux() + 0.5f * shipFT.getHardFlux() - reserved * shipFT.getMaxFlux())
                    / shipFT.getMaxFlux();
            if (shipFT.getFluxLevel() <= 0.5f) {
                decisionLevel *= shipFT.getFluxLevel() * 2f;
            }

            if (ship.getHullSpec().getHullId().startsWith("tem_")) {
                if (shipFT.getFluxLevel() >= 0.75f) {
                    decisionLevel *= 1.5f;
                } else {
                    decisionLevel *= shipFT.getFluxLevel() / 0.75f;
                }
            } else if (ship.getHullSpec().getHullId().startsWith("ms_") || ship.getHullSpec().getHullId().startsWith(
                    "msp_")) {
                decisionLevel *= 0.75f;
            } else if (ship.getHullSpec().getHullId().startsWith("exigency_")) {
                decisionLevel *= 0.5f;
            } else {
                decisionLevel *= 0.85f;
            }

            threatLevel = (float) Math.pow(threatLevel, 0.75) / (float) Math.sqrt(shipStrength);
            threatLevel = Math.max(threatLevel - (float) Math.sqrt(allyLevel / 3f) * 6f, 0f);

            if (threatLevel > shipStrength) {
                decisionLevel *= shipStrength / threatLevel;
            }

            decisionLevel -= threatLevel;
            decisionLevel -= opportunityLevel;

            if (brdy_Utils.shipBurst(ship)) {
                decisionLevel *= 0.0f;
            }

            if (flags.hasFlag(AIFlags.BACK_OFF)) {
                decisionLevel *= 1.15f;
            }

            if (flags.hasFlag(AIFlags.IN_ATTACK_RUN)) {
                decisionLevel *= 0.5f;
            }

            if (flags.hasFlag(AIFlags.KEEP_SHIELDS_ON)) {
                decisionLevel *= 0.75f;
            }

            if (flags.hasFlag(AIFlags.RUN_QUICKLY)) {
                decisionLevel *= 1.3f;
            }

            if (flags.hasFlag(AIFlags.PURSUING)) {
                decisionLevel *= 0.5f;
            }

            if (shipFT.getFluxLevel() <= 0.25f) {
                decisionLevel *= shipFT.getFluxLevel() * 4f;
            }

            float threshold = ((0.6f * (float) Math.sqrt(ship.getMaxHitpoints() / 50f) + 0.05f * (float) Math.sqrt(
                    ship.getMaxHitpoints() / 50f)
                    * (float) Math.sqrt(ship.getArmorGrid().getArmorRating() / 5f))
                    * maxVentTime / 8f) * (1.5f - reserved);
            if (decisionLevel >= threshold) {
                //engine.addFloatingText(ship.getLocation(), "Lets Vent2!", 30f, Color.white, ship, 1f, 0.5f);
                ship.giveCommand(ShipCommand.VENT_FLUX, null, 0);
            }
        }
    }

    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        // Allows any ship with a brdy hull id
        return (ship.getHullSpec().getHullId().startsWith("brdy") && ship.getVariant().getHullMods().contains(HULLMOD_ID));
    }
}
