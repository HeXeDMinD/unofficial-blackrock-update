package data.hullmods;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipCommand;
import data.scripts.util.brdy_Utils;
import org.lwjgl.util.vector.Vector2f;

public class brdy_AITriggers_Morpheus extends BaseHullMod {
    public void advanceInCombat(ShipAPI ship, float amount) {
        if (Global.getCombatEngine().isPaused() || !ship.isAlive()) { return; }

        if (ship.getShipAI() == null){ return; }

        if (ship.getPhaseCloak().isActive() && ship.getFluxLevel() > 0.9f && Math.random() > 0.85f){
            ship.giveCommand(ShipCommand.VENT_FLUX, null, 0);
        }

        if (ship.getPhaseCloak().isCoolingDown()){ return; }

        float estimated_damage = brdy_Utils.estimateAllIncomingDamage(ship,1f, false);

        if (estimated_damage < 100) {
            ship.blockCommandForOneFrame(ShipCommand.TOGGLE_SHIELD_OR_PHASE_CLOAK);
        }
        if (ship.getFluxLevel() <= 0.6f && estimated_damage > 500) {
            ship.giveCommand(ShipCommand.TOGGLE_SHIELD_OR_PHASE_CLOAK, new Vector2f(), 0);
        }
    }
}
