package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.impl.campaign.ids.Stats;

public class brdy_GAIA extends BaseHullMod {

	public static final float TURRETHEALTH_BONUS = 80f;
	public static final float TURNRATE_BONUS = 25f; //Applies to turnrate and autofire accuracy
	public static final float RANGE_BONUS = 100f;
	public static final float SMALLBEAM_TAX = 1; //Increases small beam weapons OP cost

	@Override
	public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id)
	{
		stats.getWeaponHealthBonus().modifyPercent(id, TURRETHEALTH_BONUS);
		stats.getWeaponTurnRateBonus().modifyPercent(id, TURNRATE_BONUS);
		stats.getBeamWeaponTurnRateBonus().modifyPercent(id, TURNRATE_BONUS);
		stats.getBallisticWeaponRangeBonus().modifyFlat(id, RANGE_BONUS);
		stats.getEnergyWeaponRangeBonus().modifyFlat(id, RANGE_BONUS);
		stats.getAutofireAimAccuracy().modifyFlat(id, TURNRATE_BONUS * 0.01f);
		stats.getRecoilPerShotMultSmallWeaponsOnly().modifyMult(id, 1f - 0.5f);
		stats.getDynamic().getMod(Stats.SMALL_BEAM_MOD).modifyFlat(id, SMALLBEAM_TAX);
	}

	@Override
	public String getDescriptionParam(int index, HullSize hullSize) {
		if (index == 0) return "" + (int) TURRETHEALTH_BONUS + "%";
		if (index == 1) return "" + (int) TURNRATE_BONUS + "%";
		if (index == 2) return "" + (int) RANGE_BONUS;                
		if (index == 3) return "+" + (int) SMALLBEAM_TAX;
		return null;
	}
	@Override
	public boolean affectsOPCosts() {
		return true;
	}
}
