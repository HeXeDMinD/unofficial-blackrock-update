package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;

public class brdy_CommisionedCrews extends BaseHullMod {

    final float DAMAGEBONUS_FRIGATE_DESTROYER = 5f;
    final float DAMAGEBONUS_FRIGATE_CRUISER = 7.5f;
    final float DAMAGEBONUS_FRIGATE_CAPITAL = 10f;
    final float PEAKTIMEBONUS_FRIGATE = 15f;

    final float DAMAGEBONUS_DESTROYER_CRUISER = 5f;
    final float DAMAGEBONUS_DESTROYER_CAPITAL = 7.5f;
    final float PEAKTIMEBONUS_DESTROYER = 10f;

    final float PEAKTIMEBONUS_LARGER = 7.5f;

    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        if (hullSize == HullSize.FRIGATE) {
            stats.getDamageToDestroyers().modifyPercent(id, DAMAGEBONUS_FRIGATE_DESTROYER);
            stats.getDamageToCruisers().modifyPercent(id, DAMAGEBONUS_FRIGATE_CRUISER);
            stats.getDamageToCapital().modifyPercent(id, DAMAGEBONUS_FRIGATE_CAPITAL);
            stats.getPeakCRDuration().modifyPercent(id, PEAKTIMEBONUS_FRIGATE);
        } else if (hullSize == HullSize.DESTROYER) {
            stats.getDamageToCruisers().modifyPercent(id, DAMAGEBONUS_DESTROYER_CRUISER);
            stats.getDamageToCapital().modifyPercent(id, DAMAGEBONUS_DESTROYER_CAPITAL);
            stats.getPeakCRDuration().modifyPercent(id, PEAKTIMEBONUS_DESTROYER);
        } else {
            stats.getPeakCRDuration().modifyPercent(id, PEAKTIMEBONUS_LARGER);
        }
    }

    @Override
    public String getDescriptionParam(int index, HullSize hullSize) {
        if (index == 0) return "" + DAMAGEBONUS_FRIGATE_DESTROYER + "%";
        if (index == 1) return "" + DAMAGEBONUS_FRIGATE_CRUISER + "%";
        if (index == 2) return "" + DAMAGEBONUS_FRIGATE_CAPITAL + "%";
        if (index == 3) return "" + PEAKTIMEBONUS_FRIGATE + "%";

        if (index == 4) return "" + DAMAGEBONUS_DESTROYER_CRUISER + "%";
        if (index == 5) return "" + DAMAGEBONUS_DESTROYER_CAPITAL + "%";
        if (index == 6) return "" + PEAKTIMEBONUS_DESTROYER + "%";

        if (index == 7) return "" + PEAKTIMEBONUS_LARGER + "%";
        return null;
    }

    @Override
    public void applyEffectsAfterShipCreation(ShipAPI ship, String id) {
        if (ship.getVariant().hasHullMod("CHM_commissioned")) {
            ship.getVariant().removeMod("CHM_commissioned");
        }
    }
}