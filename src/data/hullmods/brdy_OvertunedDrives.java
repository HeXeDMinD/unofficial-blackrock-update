package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;

public class brdy_OvertunedDrives extends BaseHullMod {

    public static final float EXTRAENGINE_DAMAGE = 200f;

    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        //stats.getDynamic().getStat(Stats.HULL_DAMAGE_CR_LOSS).modifyMult(id, HULL_DAMAGE_CR_MULT);
        stats.getEngineDamageTakenMult().modifyPercent(id, EXTRAENGINE_DAMAGE);
    }

    @Override
    public String getDescriptionParam(int index, HullSize hullSize) {
        if (index == 0) {
            return "" + EXTRAENGINE_DAMAGE + "%";
        }
        return null;
    }
}
