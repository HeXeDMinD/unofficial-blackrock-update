package data.hullmods;

import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;

import java.util.HashMap;
import java.util.Map;

public class brdy_FluxCore extends BaseHullMod {

    private static final Map<HullSize, Float> FLUX_ACTIVEVENT_BONUS = new HashMap<>(5);

    static {
        FLUX_ACTIVEVENT_BONUS.put(HullSize.FIGHTER, 0f);
        FLUX_ACTIVEVENT_BONUS.put(HullSize.FRIGATE, 60f);
        FLUX_ACTIVEVENT_BONUS.put(HullSize.DESTROYER, 50f);
        FLUX_ACTIVEVENT_BONUS.put(HullSize.CRUISER, 40f);
        FLUX_ACTIVEVENT_BONUS.put(HullSize.CAPITAL_SHIP, 30f);
    }

    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        stats.getVentRateMult().modifyPercent(id, FLUX_ACTIVEVENT_BONUS.get(hullSize));
    }

    @Override
    public String getDescriptionParam(int index, HullSize hullSize) {
        if (index == 0) {
            return "" + (FLUX_ACTIVEVENT_BONUS.get(HullSize.FRIGATE)).intValue();
        }
        if (index == 1) {
            return "" + (FLUX_ACTIVEVENT_BONUS.get(HullSize.DESTROYER)).intValue();
        }
        if (index == 2) {
            return "" + (FLUX_ACTIVEVENT_BONUS.get(HullSize.CRUISER)).intValue();
        }
        if (index == 3) {
            return "" + (FLUX_ACTIVEVENT_BONUS.get(HullSize.CAPITAL_SHIP)).intValue();
        }
        return null;
    }
}