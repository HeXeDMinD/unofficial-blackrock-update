package data.hullmods;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.loading.HullModSpecAPI;
import com.fs.starfarer.api.ui.Alignment;
import com.fs.starfarer.api.ui.LabelAPI;
import com.fs.starfarer.api.ui.TooltipMakerAPI;
import com.fs.starfarer.api.util.Misc;
import data.scripts.util.MagicIncompatibleHullmods;
import org.lwjgl.input.Keyboard;

import java.awt.*;
import java.util.HashSet;
import java.util.Set;

public class brdy_DriveConversion extends BaseHullMod {

    final int ZEROFLUXSPEED_BONUS = 30;
    final int MAXSPEED_BONUS = 10;
    final int MANEUVERING_MALICE = 10;

    final int ZEROFLUXSPEED_BONUS_SMOD = 10;
    final int MAXSPEED_BONUS_SMOD = 5;
    final int MANEUVERING_MALICE_SMOD = 5;

    private static final int BURN_LEVEL_BONUS = 1;

    private static final String HULLMODID = "brdy_driveconversion";

    private static final Set<String> BLOCKED_HULLMODS = new HashSet<>(3);
    static {
        BLOCKED_HULLMODS.add("augmentedengines");
        BLOCKED_HULLMODS.add("unstable_injector");
        BLOCKED_HULLMODS.add("safetyoverrides");
    }

    @Override
    public void applyEffectsAfterShipCreation(ShipAPI ship, String id) {
        for (String tmp : BLOCKED_HULLMODS) {
            if (ship.getVariant().getHullMods().contains(tmp)) {
                MagicIncompatibleHullmods.removeHullmodWithWarning(ship.getVariant(), tmp, HULLMODID);
            }
        }
    }

    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        float ZEROFLUXSPEED_BONUS = this.ZEROFLUXSPEED_BONUS;
        float MAXSPEED_BONUS = this.MAXSPEED_BONUS;
        float MANEUVERING_MALICE = this.MANEUVERING_MALICE;

        if (stats.getVariant().getSMods().contains(HULLMODID)) {
            ZEROFLUXSPEED_BONUS = ZEROFLUXSPEED_BONUS + ZEROFLUXSPEED_BONUS_SMOD;
            MAXSPEED_BONUS = MAXSPEED_BONUS + MAXSPEED_BONUS_SMOD;
            MANEUVERING_MALICE = MANEUVERING_MALICE - MANEUVERING_MALICE_SMOD;
        }

        stats.getZeroFluxSpeedBoost().modifyFlat(id, ZEROFLUXSPEED_BONUS);
        stats.getMaxSpeed().modifyPercent(id, MAXSPEED_BONUS);
        stats.getMaxTurnRate().modifyPercent(id, -MANEUVERING_MALICE);
        stats.getDeceleration().modifyPercent(id, -MANEUVERING_MALICE);
        stats.getMaxBurnLevel().modifyFlat(id, BURN_LEVEL_BONUS);
    }

    @Override
    public String getDescriptionParam(int index, HullSize hullSize) {
        if (index == 0) {
            return "" + ZEROFLUXSPEED_BONUS;
        }
        if (index == 1) {
            return "" + MAXSPEED_BONUS;
        }
        if (index == 2) {
            return "" + BURN_LEVEL_BONUS;
        }
        if (index == 3) {
            return "" + MANEUVERING_MALICE;
        }
        return null;
    }

    @Override
    public void addPostDescriptionSection(TooltipMakerAPI tooltip, HullSize hullSize, ShipAPI ship, float width, boolean isForModSpec) {
        if (!Keyboard.isKeyDown(Keyboard.getKeyIndex("F1")) &&
                (ship == null || !ship.getVariant().getSMods().contains(HULLMODID))){
            tooltip.addPara("Hold F1 to show S-mod effect info", Misc.getGrayColor(), 10);
            return;
        }
        Color colour_text = Misc.getTextColor();
        Color colour_highlight = Misc.getStoryOptionColor();

        if (ship == null || !ship.getVariant().getSMods().contains(HULLMODID)) {
            tooltip.addSectionHeading("Effect if S-modded", Alignment.MID, 10f);
            colour_text = Misc.getGrayColor();
        }

        HullModSpecAPI hullmod = Global.getSettings().getHullModSpec(HULLMODID);
        LabelAPI label = tooltip.addPara(hullmod.getDescriptionFormat(), 10f, colour_text, colour_highlight,
                "" + (ZEROFLUXSPEED_BONUS + ZEROFLUXSPEED_BONUS_SMOD),
                "" + (MAXSPEED_BONUS + MAXSPEED_BONUS_SMOD),
                "" + BURN_LEVEL_BONUS,
                "" + (MANEUVERING_MALICE - MANEUVERING_MALICE_SMOD)
        );

        label.setHighlight(
                "" + (ZEROFLUXSPEED_BONUS + ZEROFLUXSPEED_BONUS_SMOD),
                "" + (MAXSPEED_BONUS + MAXSPEED_BONUS_SMOD),
                "" + BURN_LEVEL_BONUS,
                "" + (MANEUVERING_MALICE - MANEUVERING_MALICE_SMOD)
        );

        label.setHighlightColors(colour_highlight);
    }

    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        boolean applicable = true;
        for (String tmp : BLOCKED_HULLMODS) {
            if (ship.getVariant().getHullMods().contains(tmp)) {
                applicable = false;
            }
        }
        if (!ship.getHullSpec().getHullId().startsWith("brdy_") &&
                !ship.getHullSpec().getHullId().startsWith("brdyx_")){
            applicable = false;
        }
        return applicable;
    }

    @Override
    public String getUnapplicableReason(ShipAPI ship) {
        if (!(ship.getHullSpec().getHullId().startsWith("brdy_") ||
                ship.getHullSpec().getHullId().startsWith("brdyx_"))) {
            return "Must be installed on a Blackrock ship";
        }
        for (String tmp : BLOCKED_HULLMODS) {
            if (ship.getVariant().getHullMods().contains(tmp)) {
                return "Incompatible with " + Global.getSettings().getHullModSpec(tmp).getDisplayName();
            }
        }
        return null;
    }
}
