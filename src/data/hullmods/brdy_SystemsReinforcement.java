package data.hullmods;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.loading.HullModSpecAPI;
import com.fs.starfarer.api.ui.Alignment;
import com.fs.starfarer.api.ui.LabelAPI;
import com.fs.starfarer.api.ui.TooltipMakerAPI;
import com.fs.starfarer.api.util.Misc;
import data.scripts.util.MagicIncompatibleHullmods;
import org.lwjgl.input.Keyboard;

import java.awt.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class brdy_SystemsReinforcement extends BaseHullMod {

    final int HULLHEALTH_BONUS = 25;
    final int PEAKTIME_BONUS = 20;

    final int HULLHEALTH_BONUS_SMOD = 5;
    final int PEAKTIME_BONUS_SMOD = 5;

    private static final String HULLMODID = "brdy_systemsreinforcement";

    public static final Map<HullSize, Float> ARMOR_BONUS = new HashMap<>(4);
    static {
        ARMOR_BONUS.put(HullSize.FRIGATE, 50f);
        ARMOR_BONUS.put(HullSize.DESTROYER, 100f);
        ARMOR_BONUS.put(HullSize.CRUISER, 150f);
        ARMOR_BONUS.put(HullSize.CAPITAL_SHIP, 200f);
    }

    public static final Map<HullSize, Float> ARMOR_BONUS_SMOD = new HashMap<>(4);
    static {
        ARMOR_BONUS_SMOD.put(HullSize.FRIGATE, 25f);
        ARMOR_BONUS_SMOD.put(HullSize.DESTROYER, 25f);
        ARMOR_BONUS_SMOD.put(HullSize.CRUISER, 25f);
        ARMOR_BONUS_SMOD.put(HullSize.CAPITAL_SHIP, 25f);
    }

    private static final Set<String> BLOCKED_HULLMODS = new HashSet<>(3);
    static {
        BLOCKED_HULLMODS.add("hardened_subsystems");
    }

    @Override
    public void applyEffectsAfterShipCreation(ShipAPI ship, String id) {
        for (String tmp : BLOCKED_HULLMODS) {
            if (ship.getVariant().getHullMods().contains(tmp)) {
                MagicIncompatibleHullmods.removeHullmodWithWarning(ship.getVariant(), tmp, HULLMODID);
            }
        }
    }

    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        float HULLHEALTH_BONUS = this.HULLHEALTH_BONUS;
        float PEAKTIME_BONUS = this.PEAKTIME_BONUS;
        float ARMOR_BONUS = brdy_SystemsReinforcement.ARMOR_BONUS.get(hullSize);

        if (stats.getVariant().getSMods().contains(HULLMODID)) {
            HULLHEALTH_BONUS = HULLHEALTH_BONUS + HULLHEALTH_BONUS_SMOD;
            PEAKTIME_BONUS = PEAKTIME_BONUS + PEAKTIME_BONUS_SMOD;
            ARMOR_BONUS = brdy_SystemsReinforcement.ARMOR_BONUS.get(hullSize) +
                    brdy_SystemsReinforcement.ARMOR_BONUS_SMOD.get(hullSize);
        }

        stats.getHullBonus().modifyPercent(id, HULLHEALTH_BONUS);
        stats.getPeakCRDuration().modifyPercent(id, PEAKTIME_BONUS);
        stats.getArmorBonus().modifyFlat(id, ARMOR_BONUS);
    }

    @Override
    public String getDescriptionParam(int index, HullSize hullSize) {
        if (index == 0) {
            return "" + ARMOR_BONUS.get(HullSize.FRIGATE).intValue();
        }
        if (index == 1) {
            return "" + ARMOR_BONUS.get(HullSize.DESTROYER).intValue();
        }
        if (index == 2) {
            return "" + ARMOR_BONUS.get(HullSize.CRUISER).intValue();
        }
        if (index == 3) {
            return "" + ARMOR_BONUS.get(HullSize.CAPITAL_SHIP).intValue();
        }
        if (index == 4) {
            return "" + HULLHEALTH_BONUS + "%";
        }
        if (index == 5) {
            return "" + PEAKTIME_BONUS + "%";
        }
        return null;
    }

    @Override
    public void addPostDescriptionSection(TooltipMakerAPI tooltip, HullSize hullSize, ShipAPI ship, float width, boolean isForModSpec) {
        if (!Keyboard.isKeyDown(Keyboard.getKeyIndex("F1")) &&
                (ship == null || !ship.getVariant().getSMods().contains(HULLMODID))){
            tooltip.addPara("Hold F1 to show S-mod effect info", Misc.getGrayColor(), 10);
            return;
        }
        Color colour_text = Misc.getTextColor();
        Color colour_highlight = Misc.getStoryOptionColor();

        if (ship == null || !ship.getVariant().getSMods().contains(HULLMODID)) {
            tooltip.addSectionHeading("Effect if S-modded", Alignment.MID, 10f);
            colour_text = Misc.getGrayColor();
        }

        HullModSpecAPI hullmod = Global.getSettings().getHullModSpec(HULLMODID);
        LabelAPI label = tooltip.addPara(hullmod.getDescriptionFormat(), 10f, colour_text, colour_highlight,
                "" + ( ARMOR_BONUS.get(HullSize.FRIGATE).intValue() +
                        ARMOR_BONUS_SMOD.get(HullSize.FRIGATE).intValue()),
                "" + ( ARMOR_BONUS.get(HullSize.DESTROYER).intValue() +
                        ARMOR_BONUS_SMOD.get(HullSize.DESTROYER).intValue()),
                "" + ( ARMOR_BONUS.get(HullSize.CRUISER).intValue() +
                        ARMOR_BONUS_SMOD.get(HullSize.CRUISER).intValue()),
                "" + ( ARMOR_BONUS.get(HullSize.CAPITAL_SHIP).intValue() +
                        ARMOR_BONUS_SMOD.get(HullSize.CAPITAL_SHIP).intValue()),

                (HULLHEALTH_BONUS + HULLHEALTH_BONUS_SMOD) + "%",
                (PEAKTIME_BONUS + PEAKTIME_BONUS_SMOD) + "%"
        );

        label.setHighlight(
                "" + ( ARMOR_BONUS.get(HullSize.FRIGATE).intValue() +
                        ARMOR_BONUS_SMOD.get(HullSize.FRIGATE).intValue()),
                "" + ( ARMOR_BONUS.get(HullSize.DESTROYER).intValue() +
                        ARMOR_BONUS_SMOD.get(HullSize.DESTROYER).intValue()),
                "" + ( ARMOR_BONUS.get(HullSize.CRUISER).intValue() +
                        ARMOR_BONUS_SMOD.get(HullSize.CRUISER).intValue()),
                "" + ( ARMOR_BONUS.get(HullSize.CAPITAL_SHIP).intValue() +
                        ARMOR_BONUS_SMOD.get(HullSize.CAPITAL_SHIP).intValue()),

                (HULLHEALTH_BONUS + HULLHEALTH_BONUS_SMOD) + "%",
                (PEAKTIME_BONUS + PEAKTIME_BONUS_SMOD) + "%"
        );

        label.setHighlightColors(colour_highlight);
    }

    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        boolean applicable = true;
        for (String tmp : BLOCKED_HULLMODS) {
            if (ship.getVariant().getHullMods().contains(tmp)) {
                applicable = false;
            }
        }
        if (!ship.getHullSpec().getHullId().startsWith("brdy_") &&
                !ship.getHullSpec().getHullId().startsWith("brdyx_")){
            applicable = false;
        }
        return applicable;
    }

    @Override
    public String getUnapplicableReason(ShipAPI ship) {
        if (!(ship.getHullSpec().getHullId().startsWith("brdy_") ||
                ship.getHullSpec().getHullId().startsWith("brdyx_"))) {
            return "Must be installed on a Blackrock ship";
        }
        for (String tmp : BLOCKED_HULLMODS) {
            if (ship.getVariant().getHullMods().contains(tmp)) {
                return "Incompatible with " + Global.getSettings().getHullModSpec(tmp).getDisplayName();
            }
        }
        return null;
    }
}
