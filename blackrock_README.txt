To run Blackrock Drive Yards, you *need* to also run the separate library mods LazyLib, GraphicsLib and MagicLib.

LazyLib: http://fractalsoftworks.com/forum/index.php?topic=5444.0
GraphicsLib: http://fractalsoftworks.com/forum/index.php?topic=10982.0
MagicLib: http://fractalsoftworks.com/forum/index.php?topic=13718.0

If you have trouble running the mod due to incompatible integrated graphics cards or general poor performance:
 
Go to the GraphicsLib folder and open the GRAPHICS_OPTIONS.ini file with a text editor, then change the "enableShaders" setting to "false,"
You still need to have the mod enabled for Blackrock Drive Yards to run, but this way it should not have any impact on your system performance.

Lastly, you often need to allocate more memory to the game than baseline, if you use lots of mods at once - otherwise Starsector may crash to desktop.
See http://fractalsoftworks.com/forum/index.php?topic=8726.0 for comprehensive instructions.