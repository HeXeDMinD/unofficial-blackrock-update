
# Added

## Mod support
* Commisioned Crews
* Better Deserved S-Mods
* indEvo Support - Printing and reverse engineering whitelist
* Ruthless Sector Support - Whitelist rep changes
* vayraBounties Support - Whitelist bounties
* Starship Legends Support - Derelict config
* Blacklisted hulls from Prism Freeport
* Persean Chronicles Support - Whitelist faction
* Nexerelin Support
    * Mining ship values
    * Diplomacy traits
    * More custom start options

## Hullmods

* **Blackrock Pack Tactics** - _Commisioned Crews_
  * Increases frigates and destroyers damage to larger ships
  * Increases all ships peak performance time


* **BRDY Strike Suite** - _Better Deserved S-Mods_
  * Damage bonus increased by 5%
  * Weapon range bonus increased by 5%
  * Weapon flux reduction increased by 5%
  * Weapon projectile speed bonus increased by 10%


* **BRDY Focused Shield Emitter** - _Better Deserved S-Mods_
  * Shield unfold and turn bonus increased by 25%
  * Damage reduction bonus increased by 5%
  * Shield upkeep reduced by 25%
  * Maximum shield arc increased by 15 degrees


* **BRDY Systems Reinforcement** - _Better Deserved S-Mods_
  * Hull health bonus increased by 5%
  * Peak performance time bonus increased by 5%
  * Armor bonus increased by 25


* **BRDY Drive Conversion** - _Better Deserved S-Mods_
  * 0-Flux speed bonus increased by 10
  * Max speed bonus increased by 5%
  * Maneuvering penalties reduced by 5%


## Descriptions
* Added Scalar Rejector Description
* Added Scalar Repulsor Description
* Added missing weapon descriptions, mainly fighter weapons

## Blueprints
* Added new blueprints for Fighters and Missiles

# Balancing:

## Ships

* **Imaginos**
  * Cost from 46000 to 92000


* **Morpheus**
  * Cost from 120000 to 160000


* **Scorpion**
  * Armor value from 500 to 750
  * Drone System no longer removes 0-flux boost


* **Convergence**
  * Drone System no longer removes 0-flux boost

## Fighters

* **Vespa**
  * Now uses Fury Torpedo (Fighter Rack) from Fury Torpedo (Single)
  * Changed Micro Argus PD to Focused Argus PD


* **Dipteron**
  * Weapon arc changed from 53.13 to 10


* **Krait**
  * Changed Micro Argus PD to Focused Argus PD


* **Bulwark Drone**
  * Changed Micro Argus PD to Focused Argus PD

## Weapons

* **Shardgun**
  * OP Cost 4 from 6


* **Gridfire MRM/Gridfire MRM Pod**
  * Range reduced from 2500 to 1200
  * Flight time reduced from 10 to 5
  * Submunition accuracy reduced from 1.25 to 2


* **Voidspear MRM/Voidspear MRM Pod**
  * Range increased to 2500 from 1600
  * Missile hitpoints increased from 150 to 250


* **Added Fury Torpedo (Fighter Rack)**
  * 1800 damage from 2500
  * 750 EMP from 1000
  * 650 Range from 1200


* **Added Focused Argus PD (Fighter)**
  * Currently identical to Micro Argus PD

## Colonies

* **Vigil Station**
  * Colony had both Patrol HQ and Military Base, to reduce Blackrock's already strong presence in the system
    * Military Base removed
  * To bring colony in line with industry limit
    * Size reduced from 4 to 3


* **Lydia**
  * To bring colony in line with industry limit
    * Size increased from 3 to 4


* **Preclusion**
  * To bring colony in line with industry limit
    * Size increased from 3 to 4


* **Bharata**
  * To bring colony in line with industry limit
    * Heavy Industry removed
  * To help not overpower pirates
    * Patrol HQ removed


* **Staalo**
  * To help not overpower pirates
    * Patrol HQ removed


* **Thalm Listening Post**
  * To bring colony in line with industry limit
    * Light Industry removed
  * To help not overpower pirates
    * Patrol HQ removed


* **Limbo**
  * To bring colony in line with industry limit
    * Size increased from 3 to 4
    * Military Base removed to bring it in line with industry limit
  * To help pirates raid in system
    * Patrol HQ added

Overall these changes should stop the colonies from decivilizing. 

These changes were originally made by Dal, i just included them in my update.

# Changes

## Ships

### Minor adjustments

* Frequency for most ships
* Rarity for most ships
* Default ship roles


* **Eschaton**
    * Removed "CARRIER, COMBAT" from hints


* **Kurmaraja**
  * Added "CARRIER, COMBAT" to hints

### Custom AI


* **Imaginos**
  * Scalar Rejector


* **Morpheus**
  * Scalar Repulsor


* **Dragonfly**
  * Brakefield Barrier


* **Kurmaraja**
  * Brakefield Emitter


* **Dynastos**
  * Core Discharge

### Ship Systems

* **Dragonfly**
  * Increased Breakfield Barrier's maximum affected mass from 2000 to 2500.

## Fighters

* **Kutos Drone**
  * Changed role from Fighter to Interceptor


* **Neutrocyte Drone**
  * Changed role from Fighter to Interceptor


* **Krait**
  * Changed Micro Argus PD to Focused Argus PD

## Misc
* Renamed and reorganised nearly all files, so this will not be save compatible with other unofficial updates
* Updated trails to new system
* Removed mod prefixes from most items

# Bugfixes

## Misc

* Add "brdy_wep_bp" to Blackrock Faction knownWeapons, they now have access their entire arsenal.
* 0-Series Nevermore tagged brdy-rare not brdy_rare, preventing it from showing up.